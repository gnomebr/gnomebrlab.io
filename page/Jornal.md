[]{#GNOME_Jornal} GNOME Jornal
==============================

A [GNOME Journal](http://www.gnomejournal.org) (GJ) é uma revista online
dedicada a publicação de artigos originais (em inglês) relacionados
(direta ou indiretamente) ao GNOME. A GJ é mantido por um grupo de
voluntários que trabalham na produção, edição e publicação dos artigos.

Existem diversas formas de contribuir com a GNOME Journal:

-   Divulgue as novas edições em seu blog ou em algum site de notícias;
-   Traduza e divulgue artigos da revista;
-   Escreva novos artigos (em inglês) para as futuras edições da
    revista;
-   Escreva novos artigos (em português) para as futuras edições da
    revista (\*).

Para começar a contribuir diretamente com a GJ, você precisa se
inscrever na [lista discussão dos
colaboradores](http://mail.gnome.org/mailman/listinfo/gnome-journal-list).
Para saber as diretrizes gerais para as contribuições, leia a [página de
organização da revista](http://live.gnome.org/GnomeJournal) (em inglês).

\* *Se desejar contribuir com artigos em português, envie uma mensagem
para a [lista geral do GNOME Brasil](Comunidade.html) para obter
ajuda na tradução do seu artigo para inglês*.
