[]{#Fotos_da_Comunidade_GNOME_Brasil} Fotos da Comunidade GNOME Brasil no [FISL](FISL.html) 9.0
===================================================================================================

::: {#igp1 .igp .jqSlimbox .{itemSelector:'.igpThumbNail', .singleMode:true}}
[![Estande GNOME: Fábio Nogueira (de costas), Tiago e
Luciana](/pub/images/GNOMEBR/FotosFisl9/1/thumb_DSC02304.JPG)](/pub/images/GNOMEBR/FotosFisl9/1/DSC02304.JPG "Estande GNOME: Fábio Nogueira (de costas), Tiago e Luciana"){.igpThumbNail
.{origUrl:"/FotosFisl9/DSC02304.JPG"}} [![Tradutores:
Wendell, Leonardo e
Hugo](/pub/images/GNOMEBR/FotosFisl9/1/thumb_DSC02319.JPG)](/pub/images/GNOMEBR/FotosFisl9/1/DSC02319.JPG "Tradutores: Wendell, Leonardo e Hugo"){.igpThumbNail
.{origUrl:"/FotosFisl9/DSC02319.JPG"}} [![Leonardo (no
centro) na sua
palestra](/pub/images/GNOMEBR/FotosFisl9/1/thumb_DSC02320.JPG)](/pub/images/GNOMEBR/FotosFisl9/1/DSC02320.JPG "Leonardo (no centro) na sua palestra"){.igpThumbNail
.{origUrl:"/FotosFisl9/DSC02320.JPG"}} [![Turma reunida para
um café, antes da viagem de
volta](/pub/images/GNOMEBR/FotosFisl9/1/thumb_DSC02364.JPG)](/pub/images/GNOMEBR/FotosFisl9/1/DSC02364.JPG "Turma reunida para um café, antes da viagem de volta"){.igpThumbNail
.{origUrl:"/FotosFisl9/DSC02364.JPG"}} [![Luciana, Wendell e
Izabel no stand
GNOME](/pub/images/GNOMEBR/FotosFisl9/1/thumb_DSCF1058.JPG)](/pub/images/GNOMEBR/FotosFisl9/1/DSCF1058.JPG "Luciana, Wendell e Izabel no stand GNOME"){.igpThumbNail
.{origUrl:"/FotosFisl9/DSCF1058.JPG"}}
[![Wendell](/pub/images/GNOMEBR/FotosFisl9/1/thumb_DSCF1099.JPG)](/pub/images/GNOMEBR/FotosFisl9/1/DSCF1099.JPG "Wendell"){.igpThumbNail
.{origUrl:"/FotosFisl9/DSCF1099.JPG"}} [![Izabel e Wendell
conversando](/pub/images/GNOMEBR/FotosFisl9/1/thumb_DSCF1246.JPG)](/pub/images/GNOMEBR/FotosFisl9/1/DSCF1246.JPG "Izabel e Wendell conversando"){.igpThumbNail
.{origUrl:"/FotosFisl9/DSCF1246.JPG"}} [![Izabel arrumando o
estande](/pub/images/GNOMEBR/FotosFisl9/1/thumb_HPIM4054.JPG)](/pub/images/GNOMEBR/FotosFisl9/1/HPIM4054.JPG "Izabel arrumando o estande"){.igpThumbNail
.{origUrl:"/FotosFisl9/HPIM4054.JPG"}} [![Izabel e Luciana,
aka \"Women in
Pink\"](/pub/images/GNOMEBR/FotosFisl9/1/thumb_HPIM4105.JPG)](/pub/images/GNOMEBR/FotosFisl9/1/HPIM4105.JPG "Izabel e Luciana, aka "Women in Pink""){.igpThumbNail
.{origUrl:"/FotosFisl9/HPIM4105.JPG"}} [![Mesa do
GNOME](/pub/images/GNOMEBR/FotosFisl9/1/thumb_HPIM4109.JPG)](/pub/images/GNOMEBR/FotosFisl9/1/HPIM4109.JPG "Mesa do GNOME"){.igpThumbNail
.{origUrl:"/FotosFisl9/HPIM4109.JPG"}}[]{.foswikiClear}
:::

[]{#Outras_Fotos} Outras Fotos
------------------------------

-   [Mais fotos por Jonh
    Wendell](http://www.flickr.com/photos/jwendell/tags/fisl/)
