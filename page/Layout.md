[]{#Layout_da_TWiki_Web_Gnome_BR} Layout da TWiki Web Gnome BR
==============================================================

::: {style="padding: 0.25em; color: #006; border: 2px dashed #006; background: #aaf; "}
![edittopic](/pub/System/DocumentGraphics/edittopic.gif){width="16"
height="16"} Este tópico existe de forma provisória para ajudar no
desenvolvimento desta TWiki Web.
:::

[]{#A_tarefa} A tarefa
----------------------

Esta tarefa envolve o desenvolvimento do novo layout desta TWiki Web. A
idéia inicial aqui é ter como referência tanto [a proposta de novo
layout](http://gnome.jardigrec.eu/en/) do Projeto Internacional como
também [a última proposta de layout](http://br.gnome.org/) da comunidade
brasileira.

[]{#Id_ias_e_Propostas} Idéias e Propostas
------------------------------------------

-   Iniciei ontem (2007-05-04) o desenho do layout no Inkscape. Usei
    como base a proposta de layout do projeto internacional, mas sem me
    prender muito à risca. Já já publico o modelo para receber sugestões
    e fazer algumas mudanças. Depois disso, a tarefa é criar a página
    estilizada em xhtml e css seguindo os padrões. \--
    [ViniciusDepizzol](/Main/ViniciusDepizzol) - 05 May 2007 - 15:52
-   Vinicius, uma dica: nesse link abaixo tem referências bem legais
    sobre projeto Gráfico para o Twiki
    <http://twiki.softwarelivre.org/bin/view/InkscapeBrasil.ProjetoGrafico>
    \--
    [VicenteAguiar[?](/bin/edit/Main/VicenteAguiar?topicparent=GNOMEBR.Layout "Create this topic")]{.foswikiNewLink} -
    08 May 2007 - 17:02
-   Valeu, Vicente. Essa página vai ser muito útil para criar o template
    compatível com o Twiki. Então\... [aqui
    está](http://img514.imageshack.us/img514/3532/gnomebrrn2.png) o link
    do layout que fiz no inkscape. A página será líqüida, ou seja, terá
    a largura especificada em porcentagens na janela, independente de
    resolução. O conteúdo mostrado na imagem é só um modelo, bem como os
    menus. A floresta vetorial é um oferecimento de [Andreas
    Nilsson](http://www.andreasn.se/blog/). Bom\... acho que é isso. O
    que acharam? Estou esperando por comentários
    ![smile](/pub/System/SmiliesPlugin/smile.gif "smile") \--
    [ViniciusDepizzol](/Main/ViniciusDepizzol) - 11 May 2007 15:29

::: {style="overflow: auto; border: 1px solid black; width: 800px; height: 320px;"}
\
![gnomebrrn2.png](/Layout/gnomebrrn2.png){width="900"
height="1000"}
:::

-   Os detalhes do layout aplicado estão disponíveis no tópico
    [ProjetoGrafico](/ProjetoGrafico). \--
    [LucasRocha](/Main/LucasRocha) - 23 Jun 2007 - 18:14

::: {.commentPlugin .commentPluginPromptBox}
  -- ---
      
  -- ---
:::

-   Fica a sugestão e contribuição, de uma web template
    [WebTemplateWrap](/WebTemplateWrap) com iframe para chamar
    conteúdo externo, dentro desta Web. \--
    [ValessioBrito](/Main/ValessioBrito)
