::: {align="center"}
[]{#I_Encontro_de_Software_Livre_da} I Encontro de Software Livre da Universidade Católica de Pernambuco
========================================================================================================

[![I Encontro de Software Livre da Universidade Católica de
Pernambuco](http://mepbew.blu.livefilestore.com/y1pfd9Dgb0e9tga3eYhteCPy_ZHf8hJVBTxbc5hXNZZ2rqPT5m5-ihVArYUupwwjcDTFx6MzWI3H7A/eslucapbanner2vl4.pngg)](http://www.slack-pe.org/unicap)
:::

A Primeira edição ocorrerá nos dias 1, 2 e 3 de Outubro na UNICAP, das
14h às 22h. A infra-estrutura disponível será: 1 (um) auditório para
palestrantes e 2 (duas) salas com máquinas para realização de
mini-cursos.

O encontro proporcionará atividades como oficinas de multimídia,
concursos com premiações além de contar com a presença das diversas
comunidades e membros da cena pernambucana de Software Livre. Fora isso,
há vários restaurantes, lanchonetes e bares para o pessoal se divertir
durante o evento.

Este ano, a comunidade GNOME Brasil estará sendo representada por [Jorge
Pereira](http://www.jorgepereira.com.br/), que falará sobre o tema
\"Contribuindo com o GNOME\". Esta apresentação abordará os passos para
aqueles que desejam colaborar com o projeto GNOME, seja desenvolvendo,
com traduções entre outras formas.

------------------------------------------------------------------------

[]{#foswikiTOC}

::: {.foswikiToc}
-   [Programação](#Programa_o)
-   [Slide do
    Palestrante](#Slide_do_Palestrante)
-   [Fotos do Evento](#Fotos_do_Evento)
:::

------------------------------------------------------------------------

[]{#Programa_o} Programação
---------------------------

Dia 02/10 - Quinta-Feira - 20:00h

20h-21h

Contribuindo com o GNOME

Jorge Pereira

Mais informações sobre a programação geral do evento, [veja
aqui](http://www.slack-pe.org/unicap), ou no blog do [Jorge
Pereira](http://www.jorgepereira.com.br/).

\

[]{#Slide_do_Palestrante} Slide do Palestrante
----------------------------------------------

-   [ESLUCAP2008-Contribuindo\_com\_o\_gnome-JorgePereira.pdf](ESLUCAP/ESLUCAP2008-Contribuindo_com_o_gnome-JorgePereira.pdf)

\

[]{#Fotos_do_Evento} Fotos do Evento
------------------------------------

![I Encontro de Software Livre da Universidade Católica de
Pernambuco](/ESLUCAP/jpereiran-eslucap.png)
