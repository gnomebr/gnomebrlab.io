%Desenvolvimento do extensõe para Nautilus usando Python

## Resumo em espanhol {#resumo_em_espanhol}

En aplicaciones de carácter general, como el administrador de archivos,
y de acuerdo a la filosofía del escritorio GNOME, sólo se incorporan
como características aquellas funcionalidades que son comunes a la mayor
parte de los usuarios. Se evita poblar de características que sólo
tienen una utilidad a un grupo reducido de usuarios y se ofrece como
alternativa una infraestructura de extensiones, en donde se permite
añadir características a la aplicación sin obligar a todos usuarios,
además que no requiere conocer internamente el código, ni recompilar
toda la aplicación cada vez que se realiza un cambio.

La infraestructura de extensiones no sólo permite personalizar una
aplicación, sino que también sirve como iniciación en el desarrollo de
la aplicación y como un puente para que nuevos colaboradores se integren
a un proyecto.

Este trabajo presenta la infraestructura de extensiones Nautilus, el
administrador de archivos de GNOME, a través del lenguaje Python y de la
API provista por PyGTK. Se mostrará la integración que se puede lograr
entre dos aplicaciones como Nautilus y Mercurial.

## Resumo em portunhol {#resumo_em_portunhol}

Em geral aplicações, como gerenciador de arquivos, e, de acordo com a
filosofia do ambiente de trabalho Gnome, só incorpora aquelas
características que são comuns à maioria dos usuários. É oferecido como
alternativa uma infraestrutura do extensões para incorporar
características útil para um pequeno grupo de pessoas, sem forçar todos
os utilizadores. Que não precisa saber o código internamente, e não
recompilar toda a aplicação de cada vez que fizer uma alteração.

A infraestructura do extensõe não só permite personalizar una
aplicativo, ela serve também como uma iniciação em desenvolvimento da
aplicativo e como uma ponte para integrar novos contribuintes para um
projeto.

Este trabalho apresenta a infraestructura do extensõe do Nautilus, o
gerenciador de arquivos do Gnome, através da língua Python e da API
fornecidos pelo PyGTK. Você verá a integração pode ser alcançada entre
as duas aplicações como o Nautilus e Mercurial.
