[]{#Dickson_S_Guedes} Dickson S. Guedes
=======================================

I am Brazilian and lived in several cities: Sao Paulo/SP, Laguna/SC,
Tubarao/SC, Florianopolis/SC and currently in Curitiba/PR.

User of Ubuntu and
[FreeBSD[?](/bin/edit/GNOMEBR/FreeBSD?topicparent=GNOMEBR.DicksonGuedes "Create this topic")]{.foswikiNewLink},
Biojava contributor, Database Administrator
([PostgreSQL[?](/bin/edit/GNOMEBR/PostgreSQL?topicparent=GNOMEBR.DicksonGuedes "Create this topic")]{.foswikiNewLink}
and Oracle) and now a Gnome contributor\...

[]{#Contacting_me} Contacting me:
---------------------------------

-   Wiki:
    [DicksonGuedes](DicksonGuedes.html)

<!-- -->

-   IRC: Guedes on network irc.freenode.org (\#tradutores)

<!-- -->

-   [GoogleTalk[?](/bin/edit/GNOMEBR/GoogleTalk?topicparent=GNOMEBR.DicksonGuedes "Create this topic")]{.foswikiNewLink}:
    guedizz at gmail dot com

<!-- -->

-   MSN: guedes123 at hotmail dot com

[]{#Email} Email:
-----------------

-   guediz at gmail dot com
