\

[![bannerweb\_468x60.png](ENSL/bannerweb_468x60.png){width="468"
height="60"}](http://wiki.softwarelivre.org/Festival4/WebHome)

***Maior evento de software livre do Nordeste em 2009!***

O III [ENSL](ENSL.html) - Encontro
Nordestino de Software Livre e IV Festival Software Livre da Bahia se
fundiram num só evento que será realizado nos dias 29 e 30 de maio de
2009, no campus da Universidade Estadual da Bahia (UNEB) em Salvador.
Participe! ![smile](/pub/System/SmiliesPlugin/smile.gif "smile")

Site do evento: <http://festival.softwarelivre.org/>

------------------------------------------------------------------------

[]{#foswikiTOC}

::: {.foswikiToc}
-   [Programação](#Programa_o)
-   [Slides dos
    Palestrantes](#Slides_dos_Palestrantes)
-   [Fotos do Evento](#Fotos_do_Evento)
-   [Comentários](#Coment_rios)
:::

[]{#Programa_o} Programação
---------------------------

Dia 29/05 - Sexta

14h-15h

[Uma introdução a programação de GUIs com
GTK+](http://festival.softwarelivre.org/4/pub/programacao/8)

[CarlosJosePereira](/Main/CarlosJosePereira)

16h-17h

[Programação GTK: Uma introdução ao
GLADE](http://festival.softwarelivre.org/4/pub/programacao/7)

[CarlosJosePereira](/Main/CarlosJosePereira)

14h-18h

[Oficina de Edição de Fotografias com
GIMP](http://festival.softwarelivre.org/4/pub/programacao/92)

[AurelioAHeckert](/Main/AurelioAHeckert)

Dia 30/05 - Sábado

14h-15h

[GIMP: o editor (livre) de fotos e
imagens](http://festival.softwarelivre.org/4/pub/programacao/75)

[AurelioAHeckert](/Main/AurelioAHeckert)

14h-15h

[Computador para todos: Acessibilidade no
GNOME](http://festival.softwarelivre.org/4/pub/programacao/62)

[Jonh Wendell](/Main/JonhWendell)

15h-17h

[Mesa-Redonda DS: Como contribuir com projeto o Projeto
GNOME](http://festival.softwarelivre.org/4/pub/programacao/96)

[Jonh Wendell](/Main/JonhWendell)

[]{#Slides_dos_Palestrantes} Slides dos Palestrantes
----------------------------------------------------

-   Para ter acesso aos slides de algumas das palestras apresentadas no
    III [ENSL](ENSL.html), acesse a nossa
    página de [apresentações](Apresentacoes.html).

[]{#Fotos_do_Evento} Fotos do Evento
------------------------------------

-   [Confira as fotos do evento!](IIIENSLFotos.html)

[]{#Coment_rios} Comentários
----------------------------

-   [de Jonh\...](http://www.bani.com.br/lang/pt-br/2009/06/iii-ensl/)
