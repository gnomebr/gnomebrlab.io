%Quem usa GNOME no Brasil?

**Atenção!** Esta página, assim como todas as informações e
formulários relacionados à esta campanha, ainda está em fase de
elaboração. Por favor, não realize nenhum cadastro oficial até que a
campanha seja lançada publicamente.
:::

Atualmente, o GNOME está sendo utilizado amplamente no Brasil em
projetos sociais, empresas públicas e privadas, organizações
não-governamentais, universidades, orgãos públicos, entre outros.
Seguramente, o Brasil é um dos países com maior número de usuários do
GNOME no mundo. No entanto, apesar de termos tantos usuários, sabemos
muito pouco sobre as organizações que adotaram o GNOME no país em
relação ao números de usuários, abordagens de implantação, desafios e
problemas encontrados, adaptações técnicas, etc. A comunidade GNOME
Brasil está promovendo a campanha ***\"O Brasil usa GNOME!\"***.

A proposta desta campanha é mapear o maior número possível de
organizações brasileiras que usam o ambiente gráfico do GNOME. Desta
forma, se você faz parte de alguma organização (de qualquer porte ou
natureza) que usa o desktop GNOME no país, preencha o [formulário de
cadastro](CadastroOrganizacao.html) com as informações requisitadas.

## Realize o Cadastro da Sua Organização {#Cadastro}

Clique [aqui](CadastroOrganizacao.html) para realizar o cadastro.

------------------------------------------------------------------------

## Números do Mapeamento {#Numeros_do_Mapeamento}

[Filtrar por
Estado](/QuemUsasortcol=0;table=1;up=0#sorted_table "Sort by this column")

[Filtrar por Distribuição/Sistema
Operacional](/QuemUsasortcol=1;table=1;up=0#sorted_table "Sort by this column")

[Natureza](/QuemUsasortcol=2;table=1;up=0#sorted_table "Sort by this column")

Escolha\...AcreAlagoasAmapáAmazonasBahiaCearáDistrito FederalEspírito
SantoGoiásMaranhãoMato GrossoMato Grosso do SulMinas
GeraisParáParaíbaParanáPernambucoPiauíRio de JaneiroRio Grande do
NorteRio Grande do SulRondôniaRoraimaSanta CatarinaSergipeSão
PauloTocantins

Escolha\...DebianFedoraFreeBSDKNOPPIXMandrivaNetBSDOpenBSDRed
HatSlackwareSuseUbuntuOutra

Escolha\...Orgão PúblicoEmpresa PrivadaCooperativaAssociação ou
FundaçãoMovimento SocialOutros

-   **Mostrando Estado:** todos
-   **Mostrando Distribuição/Sistema Operacional:** todos
-   **Mostrando Natureza:** todos

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  [Organização](/QuemUsasortcol=0;table=2;up=0#sorted_table "Sort by this column")   [Estado](/QuemUsasortcol=1;table=2;up=0#sorted_table "Sort by this column")   [Natureza](/QuemUsasortcol=2;table=2;up=0#sorted_table "Sort by this column")   [Número de Usuários](/QuemUsasortcol=3;table=2;up=0#sorted_table "Sort by this column")   [Distribuição/Sistema Operacional](/QuemUsasortcol=4;table=2;up=0#sorted_table "Sort by this column")
  -------------------------------------------------------------------------------------------------------------------- --------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------- --------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------
  [Colivre - Cooperativa de Tecnologias Livres](Organizacao0.html)                                                 Bahia                                                                                                           Cooperativa                                                                                                       22                                                                                                                          Debian

  [Organização Teste](Organizacao1.html)                                                                           Mato Grosso do Sul                                                                                              Associação ou Fundação                                                                                            50                                                                                                                          Fedora

  [Telecentros Sao Paulo](Organizacao2.html)                                                                       São Paulo                                                                                                       Orgão Público                                                                                                     1000                                                                                                                        Debian

  [e sadsadasd a](Organizacao3.html)                                                                               Acre                                                                                                            Org%E3o P%FAblico                                                                                                                                                                                                                             Fedora

  [SIERTI](Organizacao4.html)                                                                                      Minas Gerais                                                                                                    Empresa Privada                                                                                                   5                                                                                                                           Ubuntu

  [SIERTI](Organizacao5.html)                                                                                      Minas Gerais                                                                                                    Empresa Privada                                                                                                   5                                                                                                                           Ubuntu

  Totais                                                                                                               Acre: 1\                                                                                                        Associação ou Fundação: 1\                                                                                        1082                                                                                                                        Debian: 2\
                                                                                                                       Bahia: 1\                                                                                                       Cooperativa: 1\                                                                                                                                                                                                                               Fedora: 2\
                                                                                                                       Mato Grosso do Sul: 1\                                                                                          Empresa Privada: 2\                                                                                                                                                                                                                           Ubuntu: 2
                                                                                                                       Minas Gerais: 2\                                                                                                Org%E3o P%FAblico: 1\                                                                                                                                                                                                                         
                                                                                                                       São Paulo: 1                                                                                                    Orgão Público: 1                                                                                                                                                                                                                              
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
