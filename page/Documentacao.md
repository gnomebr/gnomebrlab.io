---------------------------------------------------------------------    --------------------------------------------------------------------
              [![](Documentacao/gnome-tutorial-text.png)\                  [![Suporte Comunitário](Documentacao/gnome-help-community.png)\
               Tutoriais](Tutoriais.html)                                                            Suporte Comunitário](SuporteComunitario.html)

                                                                                                      

      [![Vídeo Tutoriais](Documentacao/gnome-tutorial-video.png)\          [![Apresentações](Documentacao/gnome-presentation.png)\
             Vídeos](Videos.html)                                                                    Apresentações](Apresentacoes.html)

                                                                                                      

   [![Publicações Acadêmicas](Documentacao/ArtigosAcademicos.png)\         [![Dicas e Manhas!](Documentacao/DIcasGNOME.png)\
      Publicações Acadêmicas](PublicacaoAcademica.html)                                              Dicas e Manhas!](DicasManhas.html)
---------------------------------------------------------------------    --------------------------------------------------------------------
