::: {align="center"}
[]{#II_Encontro_de_Software_Livre_de} II Encontro de Software Livre de Pernambuco
=================================================================================

[![II Encontro de Software Livre de
Pernambuco](http://www.softwarelivrepe.com.br/img/topo_logo2.gif)](http://eventosl.recife.pe.gov.br/)
:::

Recife receberá o *[Segundo encontro de Software Livre de
Pernambuco](http://eventosl.recife.pe.gov.br/)* nos dias 23, 24 e 25 de
abril de 2008, na Faculdade Maurício de Nassau e será gratuito. A
organização estima que cerca de 800 pessoas participem do Encontro e
ajudem a difundir o Software Livre (SL) nos âmbitos acadêmicos,
empresariais, governamentais e sociais. O objetivo geral do Encontro de
Software Livre de Pernambuco
([ESLPE](ESLPE.html)) é propagar as
práticas de desenvolvimento colaborativo e de garantia ao acesso à
produção do conhecimento no estado de Pernambuco, além de difundir a
cultura do Software Livre e a sua importância social.

Este ano, a comunidade GNOME Brasil estará sendo representada por [Jorge
Pereira](http://www.jorgepereira.com.br/), que falará sobre o tema
\"Contribuindo com o GNOME\". Esta apresentação abordará os passos para
aqueles que desejam colaborar com o projeto GNOME, seja desenvolvendo,
com traduções entre outras formas.

------------------------------------------------------------------------

[]{#foswikiTOC}

::: {.foswikiToc}
-   [Programação](#Programa_o)
-   [Slide do
    Palestrante](#Slide_do_Palestrante)
-   [Fotos do Evento](#Fotos_do_Evento)
:::

------------------------------------------------------------------------

[]{#Programa_o} Programação
---------------------------

Dia 23/04 - Quarta-Feira - Sala 309

09h-10h

Contribuindo com o GNOME

Jorge Pereira

Mais informações sobre a programação geral do evento, [veja
aqui](http://www.softwarelivrepe.com.br/index.php?secao=cHJvZ3JhbWFjYW8=).

\

[]{#Slide_do_Palestrante} Slide do Palestrante
----------------------------------------------

Para ter acesso aos slides da palestra apresentada no
[ESLPE](ESLPE.html), acesse a nossa página
de [apresentações](Apresentacoes.html).

\

[]{#Fotos_do_Evento} Fotos do Evento
------------------------------------

Fotos das palestras, [veja
aqui](http://www.flickr.com/photos/jpereiran/tags/eslpe2008/)
