[]{#Guia_do_tradutor} Guia do tradutor
======================================

Esse documento explica o que alguém precisa saber antes de começar a
colaborar com a [equipe brasileira de tradução do
GNOME](Traducao.html). Leia-o atentamente, inclusive seguindo os
links fornecidos. A seguir, apresente-se na [lista de discussão da
equipe](Comunidade.html), receba as boas-vindas e só depois abra sua
conta no [servidor de traduções do GNOME](http://l10n.gnome.org).(mais
sobre isso na seção *Fluxo de trabalho*).

[]{#foswikiTOC}

::: {.foswikiToc}
-   [Introdução](#Introdu_o)
-   [Catálogos de
    mensagem](#Cat_logos_de_mensagem)
-   [Fluxo de trabalho](#Fluxo_de_trabalho)
-   [Mantenha-se
    informado](#Mantenha_se_informado)
:::

[]{#Introdu_o} Introdução
-------------------------

A tecnologia de [internacionalização e
localização](http://en.wikipedia.org/wiki/Internationalization_and_localization)
do GNOME é baseada no [GNU
Gettext](http://www.gnu.org/software/gettext/). Se você não estiver
familiarizado com catálogos de mensagem, sugerimos que leia a
[introdução ao
gettext](http://www.gnu.org/software/gettext/manual/gettext.html#Introduction),
bem como [o ponto de vista dos
usuários](http://www.gnu.org/software/gettext/manual/gettext.html#Users)
e o [formato dos catálogos de
mensagem](http://www.gnu.org/software/gettext/manual/gettext.html#PO-Files).
Depois de ler essa introdução, consulte nossa página de
[ferramentas](FerramentasDeTraducao.html) para saber como editar um
catálogo de mensagem.

A tradução e adaptação do GNOME é responsabilidade do [GNOME Translation
Project](http://live.gnome.org/TranslationProject/) (GTP), do qual nossa
equipe faz parte. Os [ciclos de
lançamento](http://live.gnome.org/ReleasePlanning) do GNOME têm fases
bem definidas. Cerca de dois meses antes do lançamento começa o *String
Change Announcement Period*, em que os desenvolvedores são obrigados a
notificar na lista de discussão do GTP quaisquer alterações em mensagens
a serem traduzidas. Cerca de um mês antes do lançamento acontece o
*String Freeze*, em que (quase) nenhuma mensagem para tradução pode ser
acrescentada ou alterada sem autorização do GTP e do *Release Team*.
Essas duas fases são as melhores para traduzir os módulos oficiais do
GNOME, por exemplo aqueles que farão parte do GNOME 2.26. A documentação
não está sujeita a esses congelamentos, e muitos módulos inteiros não
estão sujeitos também, como alguns módulos da plataforma de
desenvolvimento (GTK+, GLib etc.) e qualquer módulo que não faça parte
dos GNOME 2.xx. Para mais informações, leia a página
[ReleasePlanning/Freezes](http://live.gnome.org/ReleasePlanning/Freezes)
do GNOME Live. Para uma lista dos próximos congelamentos, confira a
página [Schedule](http://live.gnome.org/Schedule) do GNOME Live.

O GTP mantém um site de [estatísticas de
tradução](http://l10n.gnome.org/), baseado num software chamado *Damned
Lies*. Se uma mensagem for acrescentada, retirada ou alterada no
código-fonte do software, o catálogo continuará inalerado no repositório
git, mas no site de estatísticas ele será atualizado automaticamente.
Não, ele não será traduzido automaticamente, mas a lista das mensagens
originais (em inglês) estará atualizada, possibilitando que os
tradutores editem-no acrescentando as mensagens traduzidas. Por isso, os
tradutores devem baixar as traduções do Damned Lies, e não do
Subversion. Nas estatísticas de tradução, o nome de arquivo das
traduções de interface seguem o padrão `domínio.ramo.código.po`.
*Domínio* é geralmente o nome do módulo no git, por exemplo `gedit` ou
`seahorse`. Alguns módulos, por outro lado, têm mais de um domínio, como
o GTK+, que tem o domínio `gtk+` e o `gtk+-properties`. Cada domínio
corresponde a um arquivo MO no diretório
`/usr/share/locale/pt_BR/LC_MESSAGES/`. *Ramo* é algo como `HEAD` (ou
seja, *trunk*), `gnome-2-24` ou `glib-2-18`; para mais informações, leia
a seção \"Branches\" no [canto dos mantenedores (no GNOME
Live)](http://live.gnome.org/MaintainersCorner). *Código* é o código da
localidade; no nosso caso, é sempre `pt_BR`. Quando um aplicativo é
executado e não encontra as traduções `pt_BR`, usa por omissão as
traduções `pt`, feitas pela equipe portuguesa de tradução do GNOME.

A tradução da documentação do GNOME também é baseada no GNU Gettext.
Graças ao `gnome-doc-utils`, a documentação original é mantida em
[DocBook](http://www.docbook.org/) e as traduções em catálogos de
mensagem, e durante a compilação do software são gerados documentos
DocBook traduzidos, que podem ser abertos com o Yelp. Os catálogos de
mensagem podem ser baixados do Damned Lies da mesma forma que a tradução
da interface, mas no caso da documentação o nome dos arquivos PO é
`help.ramo.código.po`.

[]{#Cat_logos_de_mensagem} Catálogos de mensagem
------------------------------------------------

Para ler essa seção, sugerimos que você baixe um catálogo de mensagens
incompletamente traduzido. Você não vai traduzi-lo agora, mas vai poder
abri-lo com um [editor de texto ou outra ferramenta de
tradução](FerramentasDeTraducao.html), para acompanhar as
informações a seguir.

Depois de editar o catálogo de mensagens, você precisa abrir o arquivo
com um editor de textos para conferir se o cabeçalho foi atualizado
corretamente pela ferramenta de tradução. Na maioria dos casos, algumas
partes precisarão ser corrigidas. Só deixe de conferir o cabeçalho se já
conhecer muito bem o comportamento da sua ferramenta de tradução.
Observe o modelo a seguir:

    # Brazilian Portuguese translation of Evolution.
    # This file is distributed under the same license as the Evolution package.
    # Copyright (C) 2000-2009 Free Software Foundation, Inc.
    # Fulano da Silva <fdsilva@hohoho.com.br>, 2000.
    # Cicrano Almeida <calmeida@bunny.net.br>, 2001-2007.
    #
    msgid ""
    msgstr ""
    "Project-Id-Version: evolution 1.4.5\n"
    "POT-Creation-Date: 2007-10-09 16:20-0200\n"
    "PO-Revision-Date: 2007-10-09 18:18-0200\n"
    "Last-Translator: Cicrano Almeida <calmeida@bunny.net.br>\n"
    "Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>\n"
    "MIME-Version: 1.0\n"
    "Content-Type: text/plain; charset=UTF-8\n"
    "Content-Transfer-Encoding: 8bit\n"
    "Plural-Forms: nplurals=2; plural=(n > 1);\n"

Repare que a linha atribuindo diretos de cópia à Free Software
Foundation só tem valor legal se os tradutores tiverem assinado um
[termo de
atribuição](http://translationproject.org/html/whydisclaim.html). Esse
termo é necessário para quem queira traduzir o GNU, mas não para os
tradutores do GNOME. Mesmo não tendo valor legal, essa linha é muito
comum, por estar presente no [modelo de cabeçalho do Translation
Project](http://translationproject.org/html/translators.html) (que não é
o GNOME Translation Project!).

De acordo com a [lei brasileira do direito
autoral](http://www.planalto.gov.br/ccivil_03/leis/l9610.htm), tem
direitos autorais quem for autor ou co-autor, não sendo necessário para
isso o registro em órgão próprio. Dessa forma, a informação da autoria
no cabeçalho é conveniente mas a omissão de um nome não lhe exclui
direito autoral devido.

Como descrito no [guia de localização do
GNOME](http://live.gnome.org/TranslationProject/LocalisationGuide),
muitos catálogos de mensagem contêm uma mensagem especial,
*translator-credits* ou algo assim. Essa mensagem deve ser traduzida com
o nome e endereço de e-mail dos tradutores, como no cabeçalho do
catálogo de mensagens mas sem a necessidade de colocar o ano. Nos casos
em que a tradução tenha sido inicialmente derivada de uma tradução para
o português europeu, os tradutores portugueses mencionados no cabeçalho
também devem constar na tradução dessa mensagem.

Grande parte das ferramentas de tradução é capaz de atualizar
corretamente o cabeçalho, especialmente a parte que consiste na tradução
da `msgid ""`, ou seja a parte que não começa com `#`. É importante
conferir se os campos *last-translator* e *language-team* estão
preenchidos corretamente, e se o campo *plural-forms* está presente e
correto. Esse último campo é indispensável em catálogos que tenham
mensagens com forma plural, como no exemplo:

    msgid "One match found"
    msgid_plural "%d matches found"
    msgstr[0] "Uma ocorrência localizada"
    msgstr[1] "%d ocorrências localizadas"

Para mais informações sobre `%s`, `%d`, `%Y`, `${username}` e
semelhantes, leia [essa seção do manual do GNU
Gettext](http://www.gnu.org/software/gettext/manual/gettext.html#Translators-for-other-Languages).
Ela não é muito descritiva, então se você tiver alguma dúvida, pergunte!

Outra coisa a prestar atenção são as *traduções aproximadas* (em inglês:
*fuzzy*). Quando o catálogo de mensagens foi atualizado, algumas
mensagens novas eram semalhantes a outras antigas, e por isso sua
tradução foi aproveitada, mas marcada como aproximada. Essas traduções
estão erradas, e não serão aproveitadas pelo aplicativo quando este for
compilado. Ao traduzir um catálogo de mensagens, você deve conferir as
traduções aproximadas, corrigi-las, e retirar seu status de *fuzzy*.

[]{#Fluxo_de_trabalho} Fluxo de trabalho
----------------------------------------

Nós usamos o [Mentiras Cabeludas (Damned Lies)](http://l10n.gnome.org/)
para gerenciar a designação das traduções. Pedimos a todos que leiam
esse guia e apresentem-se na [lista de discussão](Comunidade.html)
antes de abrir uma conta no Mentiras Cabeludas. Uma vez inscrito no
servidor, você pode *reservar para tradução* um catálogo de mensagens.
Você deve fazer isso **antes** de começar a traduzir o catálogo! Depois
de baixar o catálogo de mensagens e traduzi-lo, você pode *enviar a nova
tradução* anexando o catálogo de mensagem traduzido e colar o diff no
campo \"Comentário\". (Para gerar o diff, digite na linha comando
`diff -u pt_BR.original.po pt_BR.traduzido.po > pt_BR.po.diff`). Daí
para frente, a tradução deverá ser revisada por um *tradutor graduado*
(a não ser que a tradução já tenha sido feita por um) e enviada ao
repositório Subversion por um *committer*.

Durante a tradução, providencie para que você possa consultar o
[Vocabulário Padrão](http://vp.taylon.eti.br/) com freqüência. Quando o
mesmo termo for usado com o mesmo significado em duas partes diferentes
do mesmo aplicativo, ou mesmo em dois aplicativos diferentes, o termo
deverá ser traduzido da mesma forma. Só fuja ao padrão se tiver certeza
do que está fazendo; na dúvida, discuta o caso com a equipe. Caso você
não encontre um termo no Vocabulário Padrão, pode pesquisá-lo no
[Open-Tran](http://www.open-tran.eu). Geralmente vai valer a pena
discutir a tradução do termo na lista de discussão da equipe. Para
acrescentar uma tradução ao Vocabulário Padrão, a discussão deve ser
realizada na [lista de discussão do
LDP-BR](http://bazar2.conectiva.com.br/mailman/listinfo/ldp-br). Aliás,
recomendamos a todos os tradutores de nossa equipe que participem dessa
lista de discussão, para que estejam a par das alterações de
terminologia, e possam colaborar com as mesmas.

Ao traduzir a documentação, preste atenção às citações de elementos da
interface de usuário. A tradução dessas citações na documentação deve
ser exatamente igual à tradução da interface de usuário. Caso a tradução
da interface esteja errada, você tem duas opções: traduzir a
documentação do mesmo jeito, ou traduzir a documentação corretamente e
corrigir a tradução da interface ao mesmo tempo.

Nós também temos um [guia de estilo](GuiaDeEstilo.html), que deve
ser seguido sempre que possível. Leia-o do começo ao fim antes de
começar a traduzir a interface de usuário ou a documentação de um
aplicativo.

Por fim, antes de anexar uma tradução ao Mentiras Cabeludas,
[revise-a](ComoRevisar.html). **Sério.** Se você não for um tradutor
graduado, sua tradução também será revisada por outra pessoa antes de
ser enviada ao repositório. Mas, se sua tradução contiver muitos erros,
revisá-la pode levar mais tempo que retraduzi-la. Por isso, se você não
revisar sua tradução antes de colocá-la no servidor, ela corre o risco
de ser simplesmente descartada. Se sua tradução estiver esperando
revisão há muito tempo, e você não a tiver revisado antes, esse pode ser
o motivo. Uma forma de estimular a aceitação de sua tradução é informar
que você já a revisou.

Como os tradutores iniciantes freqüentemente cometem erros, é importante
você comece devagar, por mais motivado que esteja. Complete uma tradução
pequena (10 mensagens, por exemplo), ou ajude a corrigir um erro simples
(leia a seguir), espere a revisão de seu trabalho, e só depois comece
outra tradução. Aumente progressivamente o tamanho de suas
contribuições, na medida em que você se sinta confiante. Lembre-se,
corrigir traduções pode dar mais trabalho que descartá-las e retraduzir
as mensagens!

[]{#Mantenha_se_informado} Mantenha-se informado
------------------------------------------------

Recomendamos a todos os membros da equipe que assinem a [lista de
discussão do
LDP-BR](http://bazar2.conectiva.com.br/mailman/listinfo/ldp-br), para
participar e ficar a par das discussões de terminologia; e a [lista de
discussão do GTP](http://mail.gnome.org/mailman/listinfo/gnome-i18n),
para ficar a par de mensagens novas durante o *string change
announcement period* ou após o *string freeze*.

A página da [equipe brasileira de tradução do GNOME no Damned
Lies](http://l10n.gnome.org/languages/pt_BR) fornece links convenientes
para [abrir um relatório de erro de
tradução](http://bugzilla.gnome.org/enter_bug.cgi?product=l10n&component=Brazilian%20Portuguese%20%5Bpt_BR%5D),
ou conferir uma [lista dos relatórios de erros de
abertos](http://bugzilla.gnome.org/buglist.cgi?product=l10n&component=Brazilian%20Portuguese%20%5Bpt_BR%5D&bug_status=NEW&bug_status=REOPENED&bug_status=ASSIGNED&bug_status=UNCONFIRMED).
Se você ainda está lendo esse documento, deve estar mesmo interessado em
colaborar com a tradução do GNOME. Se assim for, adicione esses
endereços aos seus marcadores! Você também pode ser notificado de
qualquer alteração em relatórios de erro da nossa tradução. Para isso,
acrescente `l10n-pt-br-maint@gnome.bugs` ao campo *Users to watch*, nas
suas [preferências de e-mail do Bugzilla do
GNOME](http://bugzilla.gnome.org/userprefs.cgi?tab=email).

::: {style="margin-left:-9999px;"}
:::
