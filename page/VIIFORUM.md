%VII Fórum GNOME

[![](http://rn.softwarelivre.org/ensl4/wp-content/uploads/fullbanner.gif "ENSL")](http://www.ensl.org.br/)\
**5 e 6 de novembro de 2010** **Natal - RN**


O Fórum GNOME é o evento anual da comunidade brasileira de usuários,
tradutores e desenvolvedores do GNOME. Na sua sétima edição, o Fórum
GNOME acontecerá junto ao ENSL - Encontro Nordestino de Software Livre -
em Natal, RN nos dias 5 e 6 de novembro de 2010.

------------------------------------------------------------------------

-   [Programação](#Programacao)
-   [Estande](#Estande)
-   [Slides dos Palestrantes](#Slides_dos_Palestrantes)
-   [Fotos do Evento](#Fotos_do_Evento)
-   [Dia/Hora chegada dos Palestrantes (Natal/RN)](#Dia_Hora_chegada)

------------------------------------------------------------------------

## Programação {#Programacao}

  +-----------+-----------------------------------------------------------------+-------------------------------------+
  | Dia/Hora  |  Palestra                                                       | Palestrante                         |
  ============+=================================================================+=====================================+
  |05/11 09h  |   Por trás da Infra-estrutura do GNOME: Trabalho para sysadmins | Alexandro Silva                     |
  |05/11 10h  |   Traduzindo o GNOME                                            | Antonio Fernandes e Flamarion Jorge |
  |           |                                                                 |                                     |
  |05/11 13h  |   Uma nova visão para o GNOME 3.0                               |  Everaldo Canuto                    |
  |05/11 14h  |   Repensando a interface do GNOME                               |  Vinicius Depizzol                  |
  |05/11 15h  |   Escrevendo aplicações para o GNOME em Vala                    |  Lincoln de Sousa                   |
  |05/11 16h  |   GNOME 3, auditório principal                                  |  Frederic Peters                    |
  |05/11 17h  |   Enviando seu primeiro patch ao GNOME                          |  Jonh Wendell                       |
  |05/11 18h  |   Encontro da comunidade GNOME Brasil                           |  Todos                              |
  |           |                                                                 |                                     |
  |06/11 09h  |   Participação na comunidade                                    |  Djavan Fagundes                    |
  |06/11 10h  |   GNOME e as distribuições: Uma visão geral                     |  Marcelo Santana                    |
  |06/11 11h  |   GNOME and the Web                                             |  Frederic Peters                    |
  |06/11 13h  |   Acessibilidade no GNOME                                       |  Carlos Machado                     |
  +-----------+-----------------------------------------------------------------+-------------------------------------+

## Estande {#Estande}

Aguarde

## Slides dos Palestrantes {#Slides_dos_Palestrantes}

Aguarde

## Fotos do Evento {#Fotos_do_Evento}

[Fotos - Hugo Dória](http://picasaweb.google.com/hugodoria/ENSL2010)

## Dia/Hora chegada dos Palestrantes (Natal/RN) {#Dia_Hora_chegada}

  +--------------------------+-----------------------------------+----------------------+-------------------------------+----------------+
  |      Quem                |         Chegada                   |          Voo         |           Volta               |     Voo        |
  +==========================+===================================+======================+===============================+================+
  |Antonio Fernandes         |    04 Nov, 15:20 - GOL            |     \~ G31999 \~     |     07 Nov, 13:20 - GOL       |  \~ G31998 \~  |
  |Jonh Wendell              |    04 Nov, \~10:00 - Carro        |                      |                               |                |
  |Marcelo Santana           |    04 Nov, \~15:00 - Carro        |                      |                               |                |
  |Alexandro Silva           |    04 Nov, 14:00 - GOL            |     \~ G31600 \~     |     07 Nov, 15:00 - GOL       |  \~ G31601 \~  |
  |Djavan Fagundes           |    06 Nov, 09:20 - WEBJET         |     \~6734\~         |     07 Nov, 13:55 - AZUL      |  \~ AD4211 \~  |
  |Flamarion Jorge           |    04 Nov, 22:45 - TAM            |     \~ JJ3342 \~     |     06 Nov, 15:35 - TAM       |  \~ JJ3371 \~  |
  |Everaldo Canuto           |    03 Nov, 22:22 - TAM            |     \~ JJ3600 \~     |     07 Nov, 14:15 - TAM       |  \~ JJ3302 \~  |
  |Vinicius Depizzol         |    05 Nov, 00:09 - GOL            |     \~ G31702 \~     |     08 Nov, 04:10 - GOL       |  \~ G31703 \~  |
  |Lincoln de Sousa          |    05 Nov, 09:20 - WEBJET         |     \~ 6734 \~       |                               |                |
  |Frederic Peters           |    ???                            |                      |                               |                |
  |Carlos Machado            |    ???                            |                      |                               |                |
  +--------------------------+-----------------------------------+----------------------+-------------------------------+----------------+
