[]{#FISL_11_Porto_Alegre_RS_Brasil} [FISL](FISL.html) 11 - Porto Alegre-RS - Brasil
=======================================================================================

[]{#Evento_Comunit_rio_GNOME_Brasil} Evento Comunitário GNOME Brasil
====================================================================

-   Data: 22/07
-   Horário: 10:00 às 13:00
-   Sala: Prédio 9 - [FISL](FISL.html) 10
-   Numero de Sessões aprovadas: 3
-   Coordenadores: Rodrigo Padula, Izabel, Gustavo Noronha

Tema:Encontro Comunitário dos Usuários e Colaboradores GNOME

-   Programação:
-   Discussões sobre o Projeto GNOME
-   Discussões sobre o Projeto GNOME Brasil
-   Gnome Women
-   Novidades do Gnome 3
-   [GnomeShell[?](/bin/edit/GNOMEBR/GnomeShell?topicparent=GNOMEBR.FISL11 "Create this topic")]{.foswikiNewLink}
-   Unconference / Mesa Redonda

[]{#Pessoas_confirmadas_para_o_FISL} Pessoas confirmadas para o [FISL](FISL.html)
=====================================================================================

-   Rodrigo Padula
-   Izabel Valverde
-   Gustavo Noronha

[]{#Grupo_de_Usu_rios_Integrantes} Grupo de Usuários - Integrantes
==================================================================

-   Djavan Fagundes
-   Amanda Magalhães

[]{#Palestras_que_o_Gustavo_Noronha} Palestras que o Gustavo Noronha (kov) pode dar:
====================================================================================

-   [WebKitGTK[?](/bin/edit/GNOMEBR/WebKitGTK?topicparent=GNOMEBR.FISL11 "Create this topic")]{.foswikiNewLink}+
    como extensão da plataforma GNOME
-   GNOME3 - o que vem aí
-   GObject Introspection - o que é e por que você ainda vai amar
