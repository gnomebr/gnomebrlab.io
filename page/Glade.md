[]{#Glade} Glade
================

[![telaGlade01.png](/Glade/telaGlade01.png){width="320"
height="168"}](http://glade.gnome.org/screenshots.html)

O [Glade](http://glade.gnome.org/) é um software para a construção de
interfaces que lhe permite criar e a editar projetos de interfaces de
usuário para aplicações GTK+.

\
\
\
\

[]{#Dicas_e_Manhas} Dicas e Manhas!
-----------------------------------

-   [\"Mantendo a Sanidade com o
    Glade\"](http://www.cin.ufpe.br/~cinlug/wiki/index.php/Mantendo_A_Sanidade_Com_O_Glade)

<!-- -->

-   [\"Continue mantendo sua sanidade com o
    Glade\"](http://starfightercarlao.blogspot.com/2008/09/continue-mantendo-sua-sanidade-com-o.html)

<!-- -->

-   [\"Como preencher uma GtkComboBox criada no
    GLADE\"](http://starfightercarlao.blogspot.com/2008/09/como-preencher-uma-gtkcombobox-criada.html)

<!-- -->

-   [\"Roteiro para criação de uma janela no
    GLADE\"](http://starfightercarlao.blogspot.com/2008/09/roteiro-para-criao-de-uma-janela-no.html)
