%Suporte Comunitário

\
![gnome-help-community.png](SuporteComunitario/gnome-help-community.png){width="133"
height="133"} Como o GNOME é um ambiente *desktop* usado por [diversas
distribuições](http://people.gnome.org/~daniellem/footware.shtml) do
GNU/Linux no mundo, o suporte para usuários no Brasil acaba sendo feito
por grupos de voluntários (brasileiros) que usam essas distribuições.

A partir então da distribuição GNU/Linux que você esteja usando, veja
abaixo onde você pode encontrar suporte comunitário (no Brasil) para
suas dúvidas em relação ao uso do *desktop* GNOME.

------------------------------------------------------------------------

## Escolha sua distribuição\... {#Escolha_sua_distribuicao}
::: {.foswikiToc}
-   [Debian](#Debian)
-   [Ubuntu](#Ubuntu)
-   [Arch Linux](#Arch_Linux)
-   [Slackware](#Slackware)
-   [Fedora](#Fedora)
-   [Foresight Linux](#Foresight_Linux)
:::

------------------------------------------------------------------------

### Debian {#Debian}

O [Debian Brasil](https://wiki.debian.org/Brasil)
(Debian-BR) é um projeto formado por usuários, desenvolvedores,
tradutores e documentadores voluntários, com o objetivo central de
tornar o [Debian](http://www.debian.org/) mais próximo dos usuários de
nosso país.

Se você é um usuário desta distribuição, comece pela seção
[documentação](http://www.debian.org/doc/ddp), onde você encontrará
diversos documentos, escritos ou traduzidos por voluntários do projeto,
principalmente a partir de originais do Projeto de Documentação do
Debian. Contudo, se você tem dúvida específicas sobre o desktop GNOME
dessa distribuição, existe uma [lista de discussão para usuários da
língua portuguesa](http://lists.debian.org/debian-user-portuguese/)
voltada exclusivamente para troca de informações e soluções de dúvidas.
![wink](/pub/System/SmiliesPlugin/wink.gif "wink")

Para assiná-la, mande uma mensagem para o endereço da lista acrescido de
-request com o assunto *subscribe*. Por exemplo: para assinar a
*debian-user-portuguese@lists.debian.org* mande uma mensagem para
*debian-user-portuguese-request@lists.debian.org* com *subscribe* no
assunto. Pouco tempo depois você receberá uma mensagem de confirmação.
Basta responder essa mensagem sem alterar o assunto (o Re: é aceito sem
problemas) para ter sua inscrição confirmada.

------------------------------------------------------------------------

###Ubuntu {#Ubuntu}

Apesar do Ubuntu usar o \[\[https://unity.ubuntu.com/\]\[Unity\] em
algumas versões, o GNOME ainda é um Desktop muito usado no Ubuntu. Por
este motivo, o [Ubuntu Brasil](http://wiki.ubuntu-br.org) apóia e dá
suporte aos usuários do GNOME.

Usa Ubuntu? Surgiu um problema ou dúvida relacionada ao seu desktop
GNOME? Não se preocupe, esteja certo de que em uma das fontes abaixo
você conseguirá uma solução para o seu problema ou uma resposta para a
sua dúvida, a comunidade Ubuntu-BR é muito aberta e colaborativa.

Consulte a página de [Wiki do Ubuntu-BR](http://wiki.ubuntu-br.org) para
maiores informações de como obter suporte, acesso a documentação, Fórum,
[Listas de
discussão](https://lists.ubuntu.com/mailman/listinfo/ubuntu-br),
[IRC](http://wiki.ubuntu-br.org/IRC) e outros.

------------------------------------------------------------------------

### Arch Linux {#Arch_Linux}

Arch Linux é uma distribuição rápida, leve e bastante flexível. Ela é
completamente simples de compreender e modificar, o que a torna uma
ótima distribuição estilo "faça você mesmo". O [Arch Linux
Brasil](http://www.archlinux-br.org/) é uma comunidade formada por
usuários, colaboradores e desenvolvedores da distribuição e foi criada
justamente para centralizar em um só lugar as documentações e reunir o
maior número possível de amantes da mesma.

O Arch, oficialmente, não tem nenhum desktop padrão, sendo necessário
instalar o desejado. Entre os disponíveis está o GNOME. Você pode ver
instruções de instalação do GNOME no Archlinux no [wiki oficial do
projeto (em inglês)](http://wiki.archlinux.org/index.php/GNOME) ou no
[Wiki do Archlinux Brasil (em
português)](http://wiki.archlinux.org/GNOME_(Português)).

O Arch Linux Brasil possui quatro canais de suporte oficial: o
[fórum](http://forum.archlinux-br.org/), a [lista de
discussão](http://groups.google.com/group/archlinux-br), o
[wiki](http://wiki.archlinux-br.org/) e, no IRC, \#archlinux-br na rede
Freenode. Caso você tenha alguma dúvida sobre o desktop GNOME e a sua
distribuição seja o Arch Linux procure um dos nossos canais de suporte
que nós tentaremos solucioná-la.

------------------------------------------------------------------------

### Slackware {#Slackware}

[Slackware Linux](http://www.slackware.com/) é o nome de uma das mais
antigas e conhecidas distribuições do GNU/Linux. A Slackware (ou
simplesmente \"Slack\") tem como objetivo manter-se fiel aos padrões
UNIX, rejeitando também ferramentas de configuração que escondam do
usuário o real funcionamento do sistema. Por isto, pode ser considerada
como a distribuição GNU/Linux mais \"UNIX-like\" existente. Além disso,
a Slackware é composta somente de aplicativos estáveis (e não de versões
beta ou pré-releases).

Vale ressaltar que o Slackware não tem integrado na sua distribuição o
GNOME. Desta forma, você deve baixar o seu Desktop GNU separadamente:

-   [Instalando o Slackware
    14](http://guiadoti.blogspot.com.br/2013/01/instalando-o-slackware-14.html)
-   [dropline GNOME 3.x para Slackware 14](http://droplinegnome.org)

------------------------------------------------------------------------

### Fedora {#Fedora}
------------------

O Fedora representa um conjunto de projetos patrocinados pela Red Hat e
direcionados pelo Projeto Fedora. Estes projetos são desenvolvidos por
uma imensa comunidade internacional de pessoas focadas em prover e
manter as melhores iniciativas através dos padrões livres do software de
fonte aberta. O Fedora, projeto central o desenvolvimento da
Distribuição GNU/Linux Fedora, é um sistema operacional baseado no
Kernel Linux, sempre gratuito para ser usado, modificado e distribuído
por qualquer pessoa.

O Projeto Fedora Brasil nasceu da necessidade crescente em promover o
sistema operacional Fedora em território nacional brasileiro. Através de
ações de base, como a tradução de software e de documentação, a
organização de informações básicas para novos usuários, a organização de
eventos e distribuição de DVDs, entre outras, o Projeto Fedora Brasil
pretende contribuir para a ampliação da base de usuários Fedora no
Brasil. Nosso Projeto está sempre aberto e disposto a criar novas
parcerias locais para ampliar a democratização do conhecimento, reduzir
a divisão digital e contribuir com outros projetos educacionais. Se você
é do setor educacional, governamental ou um entusiasta do software de
código aberto, entre em contato conosco. Teremos o maior prazer em
trocar ideias com você!

Clique [aqui](https://fedoraproject.org/wiki/Join/pt-br) para saber como
participar.

Você pode obter ajuda das seguintes formas:

-   [Lista de discussão para usuários brasileiros do
    Fedora](https://lists.fedoraproject.org/mailman/listinfo/br-users)
-   [Lista de discussáo para tradutores para português
    brasileiro](https://lists.fedoraproject.org/mailman/listinfo/trans-pt_br)
-   [Site](https://getfedora.org/pt_BR/)

Ou pelo IRC no canal \#fedora-br na rede irc.freenode.net.

------------------------------------------------------------------------

### Foresight Linux {#Foresight_Linux}

Esta distro foi descontinuada, conforme relatado
[aqui](http://distrowatch.com/table.php?distribution=foresight) e seus
endereços só estão acessíveis no Internet Archive até final de 2014.

O [Foresight Linux](https://web.archive.org/web/20141230183424/http://www.foresightlinux.org:80/)
era uma distribuição que demonstrave a mais nova e melhor versão do
Ambiente de Trabalho GNOME. O Foresight incluía alguns dos mais novos e
inovadores aplicativos desenvolvidos para o Linux hoje, incluindo
\"beagle\", \"f-spot\", \"avahi (zeroconf)\" e a versão mais recente de
\"hal\". Tudo isso incluído na instalação padrão, com arte e temas de
altíssima qualidade.
