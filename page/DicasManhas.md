[]{#Dicas_e_Manhas} Dicas e Manhas!
===================================

![DIcasGNOME.png](DicasManhas/DIcasGNOME.png){width="133"
height="133"}

Esta página reúne *dicas e manhas* (de preferência em português) que são
disponibilizadas em sites e blogs com o intuito de facilitar o uso dos
[softwares que compõem o Projeto GNOME](http://www.gnome.org/projects/).
Se você já documentou alguma dica ou gostaria de atualizar qualquer umas
das que se encontram abaixo, sinta-se então mais do que livre para
[colaborar com esta
página!](https://twiki.softwarelivre.org/bin/view/GNOMEBR/ComoUsarTWiki)
![smile](/pub/System/SmiliesPlugin/smile.gif "smile")

\
\

[]{#Para_usu_rios} Para usuários
--------------------------------

-   [GIMP - Editor de Imagens e
    Fotos](http://www.ogimp.com.br/categoria/tutoriais/)

<!-- -->

-   [Planner - Aplicação de Gerenciamento de Projetos](Planner.html)

\

[]{#Para_Desenvolvedores} Para Desenvolvedores
----------------------------------------------

-   [Glade - Construtor de Interfaces](Glade.html)

<!-- -->

-   *(Adicione links aqui.)*

\

[]{#Para_Administradores_de_Sistema} Para Administradores de Sistema
--------------------------------------------------------------------

-   *(Adicione links aqui.)*

\
