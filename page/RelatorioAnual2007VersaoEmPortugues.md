%Relatório Anual 2007

Nós organizamos um evento da comunidade GNOME dentro do Fórum
Internacional de Software Livre ([FISL](FISL.html)), a maior
conferência FLOSS no Brasil. Houve conversações a respeito do projeto e
de como contribuir com a equipe de tradução brasileira. Também tivemos
uma importante reunião com o propósito de estabelecer nossas metas para
2007. Nós também participamos de outras importantes conferências FLOSS
no Brasil tais como (INSIRA AQUI AS OUTRAS CONFERÊNCIAS).

O 4º Fórum GNOME, o evento anual da comunidade dos usuários e
desenvolvedores GNOME, aconteceu como um evento da comunidade da 2ª
Conferência de Software Livre do Nordeste, o maior evento de software
livre do Nordeste, em Aracaju -- SE, de 28 a 30 de setembro. O Fórum foi
um evento muito bom, ao qual pudemos trazer novos colaboradores,
especialmente tradutores.

Em 2007, tivemos que unificar todos os sites GNOME brasileiros em um
novo website. O novo website é baseado em um sistema wiki que vai tornar
mais fácil manter atualizado e consegir mais colaboradores.

No lado dos colaboradores, a equipe brasileira de tradutores conseguiu
traduzir 100% do GNOME 2.18 e 2.20 Desktop. Infelizmente, um dos nossos
tradutores mais valiosos, Raphael Higino, faleceu no dia 7 de outubro.
John Wendell se tornou co-mantenedor do Vino, o servidor de desktop
remoto (VNC) do GNOME, está agora trabalhando em um cliente de desktop
GNOME chamado Vinagre, que já foi proposto para o GNOME 2.22 desktop.

Nossos planos para 2008 são ter um Fórum GNOME mais legal. Para a 5ª
edição do Fórum, estamos planejando ter a participação de palestrantes
estrangeiros e alguns workshops com correção no local de bugs e códigos.
