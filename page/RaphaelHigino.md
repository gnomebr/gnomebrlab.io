%Raphael Higino

![Raphael.jpg](/RaphaelHigino/Raphael.jpg){width="100"
height="126"}

Por [Og Maciel](http://blog.ogmaciel.com/?p=363)

É com um grande peso no meu coração que venho comunicar a trágica
notícia do falecimento de Raphael Higino Silva. Para quem não o conheceu
(estranho me referir a ele no passado), basta-se dizer que ele foi um
dos membros mais ativos da equipe de tradução Brasileira do GNOME nos
últimos tempos, e uma pessoa que esteve sempre disposto a ajudar
qualquer pessoa!

    Estêvão Samuel Procópio tevaum em gmail.com
    Terça Outubro 9 11:45:24 BRT 2007

    É com enorme pesar que comunico que Raphael Higino Silva faleceu neste
    último dia 07, por volta das 22:00, após dois dias em coma, devido a
    um trágico acidente de moto. Todos estamos muito atordoados e tristes
    com a notícia.

    O Raphael sempre foi uma ótima pessoa, muito alegre, simpático e
    companheiro para todas as horas. Eu, como amigo, lamento a perda dessa
    tão grandiosa pessoa. Imagino que todos que tiveram a oportunidade de
    conhecê-lo também lamentam...
    --
    Estêvão Samuel Procópio
    Antigo tradutor e amigo de Raphael Higino

Se você usa GNU/Linux como seu sistema operacional e por acaso usa o
Ambiente de Trabalho GNOME em Português do Brasil, você então está
usufruindo do trabalho que o Raphael fez. (\...) Seria muito bom ver a
comunidade Brasileira mostrar sua apreciação à uma pessoa que foi-se
muito cedo e com certeza deixará muitas saudades.

