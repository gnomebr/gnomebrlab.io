::: {align="center"}
[]{#FISL_12} FISL 12
====================

[![banner
FISL](http://softwarelivre.org/articles/0030/8030/Banner_web-06.jpg?1301091099){width="469"
height="61"}](http://softwarelivre.org/fisl12/)
:::

O [FISL](http://softwarelivre.org/fisl12/), Fórum Internacional de
Software Livre, é o maior evento do segmento no Brasil e um dos maiores
do mundo. Ele acontece anualmente em Porto Alegre, RS e esta é sua
décima segunda edição.

Este ano, mais uma vez o GNOME Brasil estará presente, através de um
evento comunitário e com um estande de usuários.

------------------------------------------------------------------------

[]{#foswikiTOC}

::: {.foswikiToc}
-   [Quem vai](#Quem_vai)
-   [Programação](#Programa_o)
-   [Estande](#Estande)
-   [Slides dos
    Palestrantes](#Slides_dos_Palestrantes)
-   [Fotos do Evento](#Fotos_do_Evento)
:::

------------------------------------------------------------------------

[]{#Quem_vai} Quem vai
----------------------

   [Quem](/FISL12sortcol=0;table=1;up=0#sorted_table "Sort by this column")   [Quando?](/FISL12sortcol=1;table=1;up=0#sorted_table "Sort by this column")   [Onde](/FISL12sortcol=2;table=1;up=0#sorted_table "Sort by this column")  [Volta](/FISL12sortcol=3;table=1;up=0#sorted_table "Sort by this column")
  ------------------------------------------------------------------------------------------------------------ --------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------ -------------------------------------------------------------------------------------------------------------
                                     [KrixApolinario](/Main/KrixApolinario)                                                                                  28 Jun, 09:30, TAM                                                                                               Master Express                                                03 Jul 06:00 TAM
                                        [JonhWendell](/Main/JonhWendell)                                                                                     29 Jun, 09:33, TAM                                                                                               POA Residence                                                 03 Jul 06:15 TAM
                                   [AntonioFernandes](/Main/AntonioFernandes)                                                                               28 Jun, 17:40, WEBJET                                                                                             POA Residence                                                 03 Jul 10:40 GOL
                                     [DjavanFagundes](/Main/DjavanFagundes)                                                                                 28 Jun, 23:40, WEBJET                                                                                              Casa amigos                                                  03 Jul 10:40 GOL

[]{#Programa_o} Programação
---------------------------

\

[]{#Estande} Estande
--------------------

Este ano temos disponível um estande, onde podemos nos encontrar, trocar
idéias, programar, traduzir, tirar dúvidas, etc. Haverá sempre alguém
disponível durante os quatro dias do evento.

[]{#Slides_dos_Palestrantes} Slides dos Palestrantes
----------------------------------------------------

-   Para ter acesso aos slides de algumas das palestras apresentadas no
    [FISL](FISL.html) 12, acesse a nossa página de
    [apresentações](Apresentacoes.html).

[]{#Fotos_do_Evento} Fotos do Evento
------------------------------------

\
