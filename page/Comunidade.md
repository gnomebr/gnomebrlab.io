# Comunidade {#comunidade}

Por trás do Projeto GNOME, existe também uma [comunidade internacional]
(https://wiki.gnome.org/GnomeWorldWide) de profissionais e
voluntários que projetam, programam, traduzem, documentam e se divertem
juntos.

Os colaboradores brasileiros que participam e contribuem de alguma forma
com esse projeto se reúnem por meio da comunidade [GNOME Brasil](index.html).
Da mesma forma como acontece em comunidades de desenvolvedores e usuários
do GNOME em outros países, a comunidade brasileira é aberta e muito
receptiva a novas(os) colaboradoras(es). Para saber como contribuir, veja
a página de [colaboração](Colabore.html). A comunicação diária entre os membros
da comunidade brasileira ocorre por meio de listas de discussão e IRC
(veja abaixo).

Recomendamos também a leitura do [Código de Conduta](CodigoConduta.html),
para que tenha conhecimento das regras básicas de convivência em nossa
comunidade.

------------------------------------------------------------------------

-   [Listas de discussão do GNOME Brasil]({#listas_de_discussao})
-   [IRC](#IRC)
-   [Planeta](#planeta)
-   [Eventos](#eventos)
-   [Reuniões](#reunioes)
-   [Páginas Relacionadas](#paginas_relacionadas)

------------------------------------------------------------------------

## Listas de discussão do GNOME Brasil {#listas_de_discussao}


-   [Lista geral do GNOME
    Brasil](http://mail.gnome.org/mailman/listinfo/gnome-br-list)

<!-- -->

-   [Lista de tradutores do GNOME para o Português
    Brasileiro](http://mail.gnome.org/mailman/listinfo/gnome-pt_br-list)

Depois de inscrever-se em uma das listas acima (bem-vindo!), lembre-se
de responder aos e-mails com o comando \"Responder a todos\". Caso
contrário, sua resposta será recebida apenas pelo remetente da mensagem
à qual você respondeu! Para mais informações, leia [uma justificativa
para esse comportamento](http://www.unicom.com/pw/reply-to-harmful.html)
(em inglês).

------------------------------------------------------------------------

## IRC {#IRC}

-   Canal **\#gnome-br** na **GimpNet**
    ([irc.gnome.org](irc://irc.gnome.org/#gnome-br)) --- GNOME em geral;

<!-- -->

-   Canal **\#tradutores** no **FreeNode**
    ([irc.freenode.net](irc://irc.freenode.net/#tradutores)) ---
    Tradutores de software livre, inclusive do GNOME.

------------------------------------------------------------------------

## Planeta {#planeta}

-   Planeta GNOME Brasil: <http://planeta.br.gnome.org/>
-   [Regras para inclusão](Planeta.html)

------------------------------------------------------------------------

## Eventos {#eventos}

-   [Eventos com a presença do GNOME Brasil](Eventos.html)

------------------------------------------------------------------------

## Reuniões {#reunioes}

-   Muitas das nossas decisões são tomadas em tempo real, através de
    reuniões via IRC. [Clique aqui](Reunioes.html) para ver os logs
    das reuniões.

------------------------------------------------------------------------

## Páginas relacionadas {#paginas_relacionadas}

-   [Wiki do GNOME (BR)](https://wiki.gnome.org/GnomeBrasil)

<!-- -->

-   [Rede Software Livre Brasil](http://softwarelivre.org/projeto-gnome)

\
