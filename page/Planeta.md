%Planeta GNOME Brasil

O **Planeta GNOME Brasil** é o agregador de blogs oficial da nossa
[comunidade](Comunidade.html). O Planeta é mantido por Lucas Rocha e
Jonh Wendell. Nesta página você encontrará informações sobre como ter
seu blog incluído no Planeta e as regras para inclusão de novos membros
adotadas pelos mantenedores.

----------------------------------------------

## Como ter seu blog incluído {#incluir}

Abra um [relatório de bug no
bugzilla](http://bugzilla.gnome.org/enter_bug.cgi?product=website&component=planeta.br.gnome.org),
informando:

-   Seu nome
-   URL do seu blog
-   Contribuições feitas ao GNOME e/ou GNOME Brasil

----------------------------------------------

## Regras para Inclusão {#regras} 

O Planeta GNOME Brasil é formado por colaboradores brasileiros do
projeto. Então, a regra número 1 para entrar no Planeta é ser um
colaborador ativo.

----------------------------------------------

## Publicação de artigos {#Publicacao} 

O Planeta mostra o conteúdo integral dos blogs agregados. Como queremos
valorizar o senso de comunidade entre os participantes, queremos ouvir
tudo que você tem a dizer, não somente assuntos relacionados ao GNOME.
Confiamos no seu bom senso para não publicar artigos que vão contra o
[Código de Conduta](CodigoConduta.html) do GNOME
ou que causem quaisquer constrangimentos, tais como pornografia,
palavras de baixo calão, etc. Caso isso aconteça, seu blog será removido
do Planeta GNOME Brasil.
