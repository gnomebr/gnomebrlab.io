[]{#Um_pouco_de_Hist_ria} Um pouco de História\...
==================================================

Por [Lucas Rocha](LucasRocha.html)

A historia do surgimento do Projeto GNOME acompanha a historia dos
principais ambientes gráficos para GNU/Linux e outros sistemas Unix.
**Até meados da década de 90**, ainda existiam poucas opções de toolkits
\[1\] para construção de interfaces gráficas para sistemas Unix. O Motif
\[2\] , um toolkit proprietário criado pela Open Software Foundation
\[3\] , era o toolkit mais utilizado para a criacao de interfaces
gráficas em sistemas Unix comerciais tais como o HP-UX da Hewlett
Packard, o Solaris da Sun Microsystems e AIX da IBM. Em virtude disso, o
CDE (Common Desktop Environment)\[4\], ambiente desktop proprietário
construído pela OSF com base no Motif, ainda era a escolha mais comum
para estes sistemas.

Com o grande aumento da base de usuários do GNU/Linux em meados da
decada de 90, o interesse por soluções livres e de código aberto para
interfaces gráficas ganhou forca. **Em 1994**, surgiu o GNUstep, uma
implementação livre do toolkit gráfico
[OpenStep[?](/bin/edit/GNOMEBR/OpenStep?topicparent=GNOMEBR.Historia "Create this topic")]{.foswikiNewLink}
desenvolvido pela
[NeXT[?](/bin/edit/GNOMEBR/NeXT?topicparent=GNOMEBR.Historia "Create this topic")]{.foswikiNewLink}\[5\]
. O GNUstep serviu de base para o desenvolvimento de um ambiente desktop
livre chamado Window Maker \[6\] que emula a interface gráfica do
sistema operacional
[NeXTSTEP[?](/bin/edit/GNOMEBR/NeXTSTEP?topicparent=GNOMEBR.Historia "Create this topic")]{.foswikiNewLink}
\[7\] também da
[NeXT[?](/bin/edit/GNOMEBR/NeXT?topicparent=GNOMEBR.Historia "Create this topic")]{.foswikiNewLink}.
Surgiram também implementacoes livres do Motif, tais como o
[LessTif[?](/bin/edit/GNOMEBR/LessTif?topicparent=GNOMEBR.Historia "Create this topic")]{.foswikiNewLink}
\[8\] , que acabaram herdando as deficiências do proprio Motif: a
complexidade demasiada para o desenvolvimento de aplicações gráficas.

Neste contexto, **em 1996**, Matthias Ettrich, na época estudante de
ciência da computação da Universidade de Tubingen na Alemanha, fundou o
Projeto KDE (K Desktop Environment) com o objetivo de construir um
ambiente desktop livre completo e integrado para GNU/Linux e outros
sistemas Unix. Como base para o desenvolvimento do KDE, Ettrich optou
pelo Qt\[9\] , um toolkit criado pela empresa norueguesa Trolltech\[10\]
. Ettrich considerava o Qt tecnicamente superior em relação ao Motif e
via como uma vantagem o fato do Qt ser desenvolvido por uma empresa,
pois isso trazia confiabilidade e sustentabilidade ao desenvolvimento do
toolkit\[11\]. A decisão por utilizar o Qt como base do KDE causou
desconforto aos desenvolvedores do projeto GNU, pois, na época, este
toolkit não utilizava uma licença de software livre\[12\] . E nesse
contexto controverso em relação ao KDE e na ausencia de soluções
alternativas totalmente livres para ambientes desktop para o GNU/Linux,
que o projeto GNOME surgiu.

**Em 1997**, inspirado pela experiência positiva de desenvolvimento
baseado em componentes junto a Microsoft\[13\], Miguel de Icaza, um
hacker mexicano, começou, juntamente com Federico Mena Quintero a
desenvolver uma arquitetura de componentes gráficos para sistemas Unix
chamada Bonobo. Nessa época, Icaza ainda considerou a possibilidade de
trabalhar em conjunto com os projetos existentes voltados para o mesmo
fim, tais como o KDE (Qt), GNUstep, Wine e
[LessTif[?](/bin/edit/GNOMEBR/LessTif?topicparent=GNOMEBR.Historia "Create this topic")]{.foswikiNewLink}.
No entanto, as restrições de licenciamento do Qt e o falta de avanços
consideráveis no GNUstep\[14\], Wine e
[LessTif[?](/bin/edit/GNOMEBR/LessTif?topicparent=GNOMEBR.Historia "Create this topic")]{.foswikiNewLink}
o fizeram decidir por iniciar um novo projeto.

O Projeto GNOME foi fundado em **agosto de 1997** como parte do projeto
GNU. Um anuncio oficial\[15\] foi enviado por Icaza para as listas de
discussão dos principais projetos de software livre da época tais como
Debian, Gimp e KDE. O toolkit escolhido foi o GTK (The Gimp Toolkit) que
já estava sendo utilizado para o desenvolvimento de um editor de imagem
livre chamado Gimp (GNU Image Manipulation Program). **Em novembro de
1997**, o GNOME já havia atrasado atenção de diversos desenvolvedores na
rede e por conta disso, Icaza, Quintero e outros hackers passaram a se
dedicar totalmente ao GNOME. Ainda em dezembro de 1997, ocorreu
lançamento publico da primeira versão do GNOME, a versão 0.10.

**A partir de 1998**, diversas empresas começaram a investir no GNOME
tendo em vista que a existência de um ambiente desktop que facilitasse o
uso de sistemas Unix e requisito basico para criar uma alternativa
viável em relação aos sistemas operacionais da Microsoft e Apple
(DANIEL, 2002). **No final de 1997**, a Red Hat, empresa de
desenvolvimento de uma distribuicao\[16\] GNU/Linux, anunciou
oficialmente o apoio ao projeto. Assim, **em jáneiro de 1998**, foi
criado o Red Hat Advanced Development Labs (RHAD)\[17\] com diversos
hackers, que atualmente cumprem papel importante no GNOME, tais como
Owen Taylor, Federico Quintero e Jonathan Blanford sendo contratados
para se dedicarem exclusivamente ao desenvolvimento do GNOME. No RHAD,
diversos componentes importantes do ambiente foram desenvolvidos no
mesmo ano. **No final de 1999**, já existiam mais duas empresa dedicadas
ao desenvolvimento e prestação de serviços baseados no GNOME. A
Eazel\[18\] desenvolveu um gerenciador de arquivos chamado
Nautilus\[19\], que hoje e componente oficial do ambiente desktop GNOME;
e a Ximian\[20\] se concentrou no desenvolvimento de versões adaptadas
do ambiente GNOME para usuários corporativos e uma ferramenta integrada
de calendário, e-mail e contatos chamada Evolution\[21\].

**Em 2000**, em resposta ao grande crescimento - em numero de
colaboradores e softwares desenvolvidos - e o aumento do interesse de
grandes empresas de tecnologia no projeto, os desenvolvedores do GNOME
decidiram criar uma Fundação, a GNOME Foundation\[22\], com o objetivo
de garantir que o projeto estejá sempre caminhando na direção de suas
metas fundamentais. Para tanto, a Fundação e responsável por definir as
metas gerais do projeto, organizar o processo de desenvolvimento, ser
porta-voz do projeto para a imprensa e interface institucional para
outras organizações. A GNOME Foundation e composta por todos os
colaboradores efetivos do GNOME e e dirigida por um quadro de sete
diretores (Board of Directors) que são eleitos anualmente por votação
direta dos membros. Além disso, a Fundação conta com um quadro de
organizações parceiras (Advisory Board ) que e composta por empresas e
organizações sem fins lucrativos que apóiam o projeto. As organizações
que compõem o Advisory Board não tem nenhum poder deliberativo dentro da
Fundação e cumprem apenas o papel de conselheiros, apresentando suas
expectativas em relação ao projeto. Essas empresas devem pagar uma taxa
de manutencao anual a Fundação para fazerem parte deste quadro.
Atualmente, o Advisory Board e composto pela Access, Canonical, OLPC,
Hewlett Packard, Debian, Free Software Foundation, IBM, Intel, Imendio,
Nokia, Novell, Opened Hand, Red Hat, Igalia, Software Freedom Law Center
e Sun Microsystems.

**Ainda em 2000**, a Sun Microsystems decidiu utilizar o GNOME como
ambiente desktop padrão do seu sistema operacional Solaris em
substituição ao CDE\[23\] . Ainda no mesmo ano, a Sun criou o Sun GNOME
Accessibility Development Lab\[24\] com o objetivo de trabalhar em
conjunto com o Projeto GNOME no desenvolvimento de tecnologias que
permitam a utilização de computadores com sistemas Unix por pessoas com
algum tipo de deficiência. **Em novembro de 2003**, a Novell anunciou a
aquisição da Ximian e optou pelo GNOME como ambiente desktop oficial de
seus produtos baseados no GNU/Linux\[25\]. **A partir 2004**, a Nokia
começou a investir em tres empresas de menor porte, Opened Hand, Imendio
e Fluendo para o desenvolvimento de tecnologias para dispositivos moveis
baseados em GNOME e GNU/Linux para serem utilizadas em seus produtos. A
Opened Hand\[26\] desenvolve softwares para GNU/Linux em dispositivos
moveis. A Imendio\[27\] e focada no desenvolvimento de diversos
aplicativos e tecnologias de base para o GNOME. A Fluendo\[28\] trabalha
no desenvolvimento de uma plataforma multimídia para sistemas Unix que e
utilizada pelo GNOME chamada GStreamer. Todas essas empresas investem de
forma direta ou indireta no GNOME de forma a garantir a evolução e
sustentabilidade do projeto.
