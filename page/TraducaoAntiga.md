[]{#Tradu_o_do_GNOME_para_o_Portugu} Tradução do GNOME para o Português Brasileiro
==================================================================================

Bem-vinda(o) à página da equipe brasileira de tradução do GNOME! Somos
parte do [Projeto de Tradução do GNOME
(GTP)](http://developer.gnome.org/projects/gtp/), e nosso coordenador
local é [Guilherme de S. Pastore](http://blogs.gnome.org/gpastore). Você
pode entrar em contato conosco através da nossa [lista de
discussão](http://listas.cipsga.org.br/cgi-bin/mailman/listinfo/gnome-l10n-br),
ou então pelo canal \#tradutores no FreeNode
([irc.freenode.net](irc://irc.freenode.net/#tradutores)).

Veja também: [ErrosFrequentes](ErrosFrequentes.html),
[TradutoresTrabalhando[?](/bin/edit/GNOMEBR/TradutoresTrabalhando?topicparent=GNOMEBR.TraducaoAntiga "Create this topic")]{.foswikiNewLink}

[]{#Sum_rio} Sumário
--------------------

[]{#foswikiTOC}

::: {.foswikiToc}
-   [Relatando erros](#Relatando_erros)
-   [Como Ajudar?](#Como_Ajudar)
-   [Dicas de tradução](#Dicas_de_tradu_o)
-   [Boas Práticas](#Boas_Pr_ticas)
-   [Dicas avançadas](#Dicas_avan_adas)
    -   [Mantendo-se
        atualizado](#Mantendo_se_atualizado)
    -   [Modelo de
        cabeçalho](#Modelo_de_cabe_alho)
    -   [Onde esta mensagem será
        exibida?](#Onde_esta_mensagem_ser_exibida)
    -   [Verificando
        catálogos](#Verificando_cat_logos)
    -   [Atualizando
        catálogos](#Atualizando_cat_logos)
    -   [Começando um catálogo do
        zero](#Come_ando_um_cat_logo_do_zero)
    -   [Traduções
        incoerentes](#Tradu_es_incoerentes)
    -   [De onde vem essa
        mensagem?](#De_onde_vem_essa_mensagem)
-   [Ver também](#Ver_tamb_m)
:::

[]{#RelatandoErros}

[]{#Relatando_erros} Relatando erros
------------------------------------

O processo de tradução não é perfeito, e nem os tradutores! Se você acha
que alguma tradução contém erros, a forma mais garantida de conseguir
uma correção é através do [\"Bugzilla\" do
GNOME](http://bugzilla.gnome.org). Você pode usar o assistente de
relatório de erros a partir da página inicial do Bugzilla.

O resumo de erro (*Please enter a **short** summary of the bug*) deve
ser conciso mas suficiente para informar sobre o que é o relatório ‒
deve incluir, por exemplo, o nome do programa em questão. Quando for
descrever o erro, lembre-se de fornecer informações suficientes para que
os outros possam entender seu ponto de vista.

O acompanhamento do erro é feito por correio eletrônico: o Bugzilla do
GNOME enviará um *e-mail* às partes envolvidas toda vez que ocorrer uma
novidade em um relatório de erro.

Se você estiver interessado em \"resolver\" os erros relatados, leia a
seção seguinte e veja também quais são os [erros mais
freqüentes](ErrosFrequentes.html).

[]{#Como_Ajudar} Como Ajudar?
-----------------------------

Então, você também quer traduzir o GNOME? O primeiro passo é
inscrever-se na nossa [lista de
discussão](http://listas.cipsga.org.br/cgi-bin/mailman/listinfo/gnome-l10n-br)
e apresentar-se. Esta seção é um introdução; seria melhor ler a página
inteira antes de começar a traduzir.

Você pode obter os [catálogos de
mensagens](http://www.gnu.org/software/gettext/manual/gettext.html) a
partir do *site* com as [estatísticas de
tradução](http://l10n.gnome.org). Antes de começar a traduzir, confira
se já não há algum [tradutor
trabalhando[?](/bin/edit/GNOMEBR/TradutoresTrabalhando?topicparent=GNOMEBR.TraducaoAntiga "Create this topic")]{.foswikiNewLink}
nele.

Além disso, é bom levar em consideração o [ciclo de
desenvolvimento](http://live.gnome.org/ReleasePlanning) do GNOME.
Geralmente os programas têm uma versão estável (*branch*), como a 2.16,
e outra de desenvolvimento (*trunk* ou *HEAD*). Você só deve traduzir
uma versão estável se estiver agendada uma revisão da mesma (prazo para
a 2.20.2: 26/11/2007). Se a versão x.yy.3 já tiver sido lançada, então
não adianta traduzir porque as traduções não vão aparecer nas telas de
ninguém.

A versão de desenvolvimento, por outro lado, só deve ser traduzida a
partir do *[change announcement
period](http://live.gnome.org/ReleasePlanning/Freezes)*, quando as
modificações são menos freqüentes e sempre anunciadas na [lista de
discussão do GTP](http://mail.gnome.org/mailman/listinfo/gnome-i18n/).

Alguns programas não seguem o ciclo de desenvolvimento do GNOME, e podem
ser traduzidos a qualquer momento. Naturalmente, a tradução da interface
gráfica tem prioridade sobre a da documentação.

Você pode editar os catálogos de mensagens com qualquer editor de textos
(como o [Gedit](http://www.gnome.org/projects/gedit/), o
[Emacs](http://www.gnu.org/software/emacs/) ou o
[Vim](http://www.vim.org/scripts/script.php?script_id=695), mas existem
programas dedicados como o
[Gtranslator](http://gtranslator.sourceforge.net) (plataforma
GTK+/GNOME, desenvolvimento estagnado), o
[poEdit](http://www.poedit.org) (plataforma wxWidgets), e o
[KBabel](http://kbabel.kde.org) (plataforma Qt/KDE). Existem ainda
ferramentas de tradução *on-line*, como o
[Pootle](http://translate.sourceforge.net/) e o
[Rosetta](http://launchpad.net/rosetta), mas para serem utilizadas seria
necessário uma decisão da equipe de tradução, implicando em alguns
reveses.

Após traduzir o catálogo de mensagens, você deve acrescentar seu nome e
*e-mail* no final da tradução da mensagem `translator-credits`, se esta
existir, e também nos comentários do cabeçalho do catálogo.

Para enviar a tradução, você deve compactá-la com o *gzip* (serve o
*file-roller*, mas usando a extensão `.gz`) e anexá-la a um relatório de
\"erro\" no bugzilla. Siga as [instruções
acima](#RelatandoErros), e após concluir o
relatório clique em *Create a New Attachment* e indique o pacote com a
tradução. Assinale *patch* em *Content type* para facilitar o controle
de quem for enviar a tradução para o SVN. Se você for submeter vários
arquivos em seqüência, pode usar a opção *Clone this Bug* para agilizar
o processo.

Pronto! Agora é acompanhar o andamento do relatório através do correio
eletrônico. A tradução ficará no bugzilla até que algum membro da equipe
com acesso ao [repositório](http://developer.gnome.org/tools/svn.html)
envie o arquivo para lá. Você pode ajudar no processo revisando a
tradução de outras pessoas.

[]{#Dicas_de_tradu_o} Dicas de tradução
---------------------------------------

Traduzir um programa é uma tarefa de muita responsabilidade! Uma
interface bem traduzida permite que usuários brasileiros usem um
programa escrito em inglês; por outro lado, uma interface mal traduzida
pode impedir qualquer usuário de utilizar o programa adequadamente. Para
piorar, uma tradução com erros de sintaxe pode simplesmente travar o
programa, ou impedi-lo de ser compilado. Você não quer isso, certo?
![wink](/pub/System/SmiliesPlugin/wink.gif "wink")

Então, muito cuidado com o português! Mesmo. Está na hora de tirar do
armário aquela gramática que você não usa desde a oitava série e de
desempoeirar o dicionário. Isso vai parecer contra intuitivo, mas, para
traduzir bem, você precisa saber muito mais português do que inglês.

Além disso, traduções de informática apresentam os seus próprios
problemas, advindos de textos em inglês curtos, objetivos e, muitas
vezes, enigmáticos. Mas, calma, você não está nessa desamparado. Existem
algumas ferramentas úteis que podem ajudá-lo durante o processo de
tradução.

Nós não temos um guia de estilo específico para o GNOME, mas
recomendamos o uso do [guia de estilo adotado pela
Conectiva](http://br.tldp.org/ferramentas/guia_estilo/guia_estilo.html).
Este guia está disponível no *site* do [Projeto de Documentação do Linux
(LDP-BR)](http://br.tldp.org/). Este guia é mais voltado para a tradução
de manuais, mas também pode ser muito útil para tradutores de catálogos
de mensagens. Em especial, observe os exemplos de tradução e como é
interessante fugir de construções muito literais.

Para propiciar uma tradução mais coerente, existem o [Glossário do
GNOME](http://l10n-status.gnome.org/HEAD/PO/gnome-glossary.HEAD.pt_BR.po)
e o [Vocabulário Padrão](http://br.tldp.org/ferramentas/vp/vp.html), com
um apanhado de palavras e expressões comuns acompanhadas das suas
traduções. Você pode consultá-los diretamente, ou pode utilizá-los como
\"memória de tradução\" em Ferramentas ferramentas especializadas em
tradução de *software* livre.

Nós planejamos unificar os dois vocabulários em breve, mas por enquanto
dê preferência ao Glossário do GNOME. Ambos os vocabulários são
ferramentas de referência, e não guardiões autoritários da suprema e
única tradução perfeita
![smile](/pub/System/SmiliesPlugin/smile.gif "smile") . Sinta-se livre
para usar traduções diferentes se a sua aplicação as exige. E, se você
encontrar palavras não presentes no vocabulário ou quiser discutir
traduções lá presentes, a lista de discussão é o lugar apropriado para
fazê-lo.

O GNOME tem [diretrizes de
interface](http://developer.gnome.org/projects/gup/hig/) que incluem
botões e [menus
padronizados](http://developer.gnome.org/projects/gup/hig/2.0/menus-standard.html),
visando à coerência da interface. Às vezes o programador adota valores
pré-definidos no [GTK+](http://l10n.gnome.org/module/gtk+) ou na
[libgnomeui](http://l10n.gnome.org/module/libgnomeui); nesses casos, os
rótulos (de menu, de botão\...) já estão traduzidos, e nem vão aparecer
no catálogo de mensagens. Às vezes o programador resolve reinventar a
roda, e definir ele mesmo o rótulo dos botões de comando e outros
elementos de interface. Nesses casos, você deve consultar a tradução das
bibliotecas acima para saber o padrão. Repare que em alguns casos o
original contém uma sublinha (\"\_\"), indicando que a letra seguinte é
a [tecla de
atalho](http://developer.gnome.org/projects/gup/hig/2.0/input-keyboard.html#choosing-access-keys),
como o **A** em \"\_Arquivo\". Outro caractere estranho é o *pipe*
(\"\|\"); ele indica que o texto precedente é só para contextualizar, e
apenas o texto seguinte deve ser traduzido.

Ainda nas diretrizes de interface, repare que os botões e itens de menu
têm os verbos no infinitivo (\"Abrir\"), enquanto as dicas de ferramenta
(que também aparecem na barra de estado) devem ficar no presente (\"Abre
um arquivo\"), e as descrições do menu GNOME ficam no imperativo
(\"Navegue na Web\"). Nos diálogos de preferências as opções também
ficam no infinitivo (\"Baixar e abrir arquivos automaticamente\").

Em caso de dúvida, lembre-se da lista de discussão! Caso você encontre
uma tradução incoerente, faça um relatório de erro como explicado na
seção seguinte. Você pode inclusive já enviar um arquivo com as
correções!

[]{#Boas_Pr_ticas} Boas Práticas
--------------------------------

Visando à otimização do fluxo de trabalho da equipe, essa página deverá
ser reformulada de forma a tornar-se mais útil aos iniciantes. Como o
ótimo é inimigo do bom, comecemos por um assunto: [como revisar a
tradução da documentação](RevisandoTraducaoDeDocumentacao.html).

Outro artigo, mais incipiente, é o [Guia de Estilo de Tradução da
Interface](GuiaEstiloInterface.html). Se tivéssemos uma página única
para todas as equipes de tradução, poderíamos ter um Guia de Estilo
Único, ressaltando as particularidades de cada projeto em parágrafos à
parte, mas por enquanto vamos começar um guia só nosso.

[]{#Dicas_avan_adas} Dicas avançadas
------------------------------------

### []{#Mantendo_se_atualizado} Mantendo-se atualizado

Você pode monitorar os relatórios de erro de tradução no bugzila, de
forma a poder corrigi-los rapidamente. Além disso, também pode ajudar
revisando as traduções enviadas (se ninguém tiver assumido para si a
revisão daquele programa). Para saber quais são os erros pendentes,
acesse a [página de estatísticas](http://l10n.gnome.org/languages/pt_BR)
e clique em *Show Existing Bugs*. Se você for cadastrado no Bugzilla,
pode clicar no botão *Remember search* para acrescentar a página à sua
lista de buscas, com o nome que estiver escrito no campo de texto ao
lado. Daí em diante, toda vez que você usar o Bugzilla poderá acessar a
página facilmente clicando no vínculo no rodapé da página, em *Saved
Searches:*.

Você também pode receber *e-mails* toda vez que houver uma novidade no
componente `pt_BR` do Bugzilla do GNOME. Para isso, você deve
acrescentar o \"usuário\":

     l10n-pt-br-maint@gnome.bugs 

à sua lista de monitorados. Após acessar o bugzilla e identificar-se,
clique em *Account*, então na aba *Email preferences* e role a página
até a seção *User Watching*. Acrescente o `l10n-pt-br-maint@gnome.bugs`
ao campo de texto e clique em *Submit Changes*.

Também vale a pena manter-se atualizado em relação ao *GNOME Live!*.
Sendo cadastrado no wiki, você pode clicar em *Subscrever* no cabeçalho
de uma página, e então receberá notificações por *e-mail* sempre que a
página for alterada. Que tal fazer isso agora com a página
[TradutoresTrabalhando[?](/bin/edit/GNOMEBR/TradutoresTrabalhando?topicparent=GNOMEBR.TraducaoAntiga "Create this topic")]{.foswikiNewLink}?

### []{#Modelo_de_cabe_alho} Modelo de cabeçalho

Sempre que fizer alguma edição significativa na tradução, você deve
acrescentar suas informações aos comentários do cabeçalho do catálogo de
mensagens. Se a sua ferramenta de tradução não permitir a edição do
cabeçalho, você pode fazer isso com um editor de textos. As informações
sobre os tradutores antigos devem ser mantidas, mesmo que não tenha
restado nada do trabalho de um deles (p. ex. porque as mensagens
traduzidas se tornaram obsoletas, ou foram retraduzidas).

O cabeçalho propriamente dito costuma ser editado automaticamente pela
própria ferramenta de tradução, mas vale a pena conferir num editor de
textos. De qualquer forma, você precisa saber algumas informações, como
nome e *e-mail* da equipe de tradução, e a regra de plural, então
confira o modelo fictício abaixo:

    # Brazilian Portuguese translation of Evolution.
    # This file is distributed under the same license as the Evolution package.
    # Copyright (C) 1999,2000,2001,2002,2003 Free Software Foundation, Inc.
    # Fulano da Silva <fdsilva@hohoho.com.br>, 1999.
    # Cicrano Almeida <calmeida@bunny.net.br>, 2000,2001,2002,2003.
    #
    msgid ""
    msgstr ""
    "Project-Id-Version: evolution 1.4.5\n"
    "POT-Creation-Date: 2003-10-09 16:20-0200\n"
    "PO-Revision-Date: 2003-10-09 18:18-0200\n"
    "Last-Translator: Cicrano Almeida <calmeida@bunny.net.br>\n"
    "Language-Team: Brazilian Portuguese <gnome-l10n-br@listas.cipsga.org.br>\n"
    "MIME-Version: 1.0\n"
    "Content-Type: text/plain; charset=UTF-8\n"
    "Content-Transfer-Encoding: 8bit\n"
    "Plural-Forms: nplurals=2; plural=(n > 1);\n"

### []{#Onde_esta_mensagem_ser_exibida} Onde esta mensagem será exibida?

O desconhecimento do contexto de uma mensagem pode atrapalhar a tradução
da mesma, ainda mais com a escassez de comentários nos catálogos de
mensagens. Para isso servem os comentários automáticos indicando o lugar
no código-fonte onde a mensagem aparece. Calma, você não precisa
aprender a programar!

Se o arquivo tiver a extensão `.glade`, então você está com sorte. Para
começar, instale o programa glade ‒ você provavelmente tem apenas a
biblioteca instalada. Agora, você vai precisar do arquivo glade em si;
ele pode ser encontrado dentro do pacote de código-fonte (algo como
`file-roller-2.17.90.tar.gz`) ou a partir do SVN (algo como
<http://svn.gnome.org/viewcvs/file-roller/trunk/>). Abrindo o arquivo
glade com o respectivo programa, você vai ter acesso à interface gráfica
do programa traduzido sem precisar executá-lo, ou mesmo instalá-lo.

Se o arquivo tiver a extensão `.schema`, então é um arquivo xml do
gconf. Você pode consegui-lo como conseguiria o arquivo glade, e então
abri-lo com um editor de textos ou o mlview. O arquivo glade também é
xml, mas é bem menos legível, vale a pena instalar o programa glade.

A extensão `.desktop` indica que o arquivo é (provavelmente) um item do
menu GNOME.

### []{#Verificando_cat_logos} Verificando catálogos

Os programas de tradução verificam a integridade do catálogo de
mensagens, mas se você editou o arquivo com um editor de textos é melhor
verificar sua integridade. O comando para isso é:

     msgfmt NOME-DO-ARQUIVO -cvo /dev/null 

### []{#Atualizando_cat_logos} Atualizando catálogos

Enquanto você trabalha numa tradução, o original pode ter sofrido alguma
alteração. Se você acompanha a lista de discussão do GTP e recebeu uma
notificação nesse sentido, ou se você demorou muito para terminar a
tradução, vale a pena atualizar seu catálogo de mensagens. Para isso é
preciso ter em mãos a versão atualizada do arquivo POT, o modelo de
catálogo de mensagens. Ela pode ser obtida no *site* de estatísticas do
GNOME, na seção *Modules*, na página do programa sendo traduzido.
Cuidado para pegar da versão certa, não vá confundir GNOME 2.16 com HEAD
e por aí adiante! A atualização em si é fácil; abra o catálogo num
programa de tradução e procure por algum item de menu chamado *Atualizar
catálogo a partir do modelo* ou algo assim.

Pela linha de comando, a atualização fica assim:

     msgmerge --update ARQUIVO-PO ARQUIVO-POT 

A opção `--update` faz com que o arquivo PO original seja sobrescrito;
se você não quiser isso, pode omitir essa opção. A princípio o catálogo
de mensagens resultante será exibido no próprio terminal, mas você pode
redirecionar a saída padrão acrescentando no final do comando um dos
seguintes:

     msgmerge --update ARQUIVO-PO ARQUIVO-POT | less 

para conferir o resultado na tela, ou:

     msgmerge --update ARQUIVO-PO ARQUIVO-POT > novo-arquivo.pt_BR.po 

para gerar um terceiro arquivo. Além de usar o `--update` ou
redirecionar a saída padrão, é possível usar o comando:

     msgmerge --output-file=ARQUIVO.po ARQUIVO-PO ARQUIVO-POT 

para salvar o resultado em um terceiro arquivo.

### []{#Come_ando_um_cat_logo_do_zero} Começando um catálogo do zero

Se o programa não tem arquivo `.pt_BR.po` ainda, você vai ter que
começar com o modelo, o arquivo `POT`. Você pode abri-lo num programa de
tradução e editar os valores do cabeçalho, ou então abri-lo num editor
de texto e aproveitar nosso modelo de cabeçalho. Se você gostar de
interface de linha de comando, pode usar o `msginit`, com o cuidado de
depois conferir o cabeçalho com uma das opções anteriores.

### []{#Tradu_es_incoerentes} Traduções incoerentes

Uma mesma mensagem pode ocorrer em mais de um \"domínio de tradução\",
ou seja, em mais de um catálogo de mensagens. Você pode encontrar essas
mensagens com o comando:

     msgcat --more-than=1 ARQUIVO-1.po ARQUIVO-2.po ... 

Assim como na dica do `msgmerge`, é possível salvar o resultado com a
opção `--output-file=ARQUIVO.po` ou redirecionar a saída padrão para um
comando ou um arquivo. Agora é só ler o resultado, as divergências de
tradução estarão bem evidentes.

### []{#De_onde_vem_essa_mensagem} De onde vem essa mensagem?

Essa dica vem do Ubuntu. Às vezes acontece da pessoa ver um erro e não
saber de onde vem; isso é mais freqüente com ferramentas de linha de
comando, mas pode acontecer na interface gráfica também. Para encontrar
o erro, você pode usar o `gnome-search-tool`:

-   Em *Nome contém:*, especifique `*.mo`, que é a extensão qual a qual
    as traduções em uso são guardadas;
-   Em *Procurar na pasta:*, especifique
    `/usr/share/locale/pt_BR/LC_MESSAGES/`;
-   Clique em Selecionar mais opções, e em *Contém o texto:* digite a
    tradução errada pela qual procura.

Sabendo qual é o arquivo, você pode procurar por algo parecido na página
de estado atual da tradução.

Outra opção é simplesmente usar o `grep`.

[]{#Ver_tamb_m} Ver também
--------------------------

Sobre tradução:

-   Wikipédia: [Tradução](http://pt.wikipedia.org/wiki/Tradução),
    [i18n](http://pt.wikipedia.org/wiki/Internacionalização).
-   Wikipédia em inglês:
    [Translation](http://en.wikipedia.org/wiki/Translation),
    [Computer-assisted
    translation](http://en.wikipedia.org/wiki/Computer-assisted_translation),
    [i18n and
    l10n](http://en.wikipedia.org/wiki/Internationalization_and_localization).

Outros projetos de tradução:

-   [Projeto de Documentação Linux](http://br.tldp.org/).
-   [Equipe brasileira de tradução do
    GNU](http://www.iro.umontreal.ca/translation/registry.cgi?team=pt_BR).
-   [Equipe brasileira de tradução do
    KDE](http://kde-i18n-ptbr.codigolivre.org.br/).
-   [Equipe brasileira de tradução do
    XFCE](http://i18n.xfce.org/wiki/team_pt_br).
-   [ONG BrOffice.org](http://www.broffice.org.br).
-   [Equipe brasileira de tradução de Mozilla](http://br.mozdev.org).

Projetos de tradução de distribuições:

-   [Equipe brasileira de tradução do
    Debian](http://debian-br.alioth.debian.org/).
-   [Equipe brasileira de tradução do
    Fedora](http://fedoraproject.org/wiki/L10N/Teams/BrazilianPortuguese).
-   [Equipe brasileira de tradução do
    Gentoo](http://www.gentoo.org/doc/pt_br/overview.xml).
-   [Equipe brasileira de tradução do
    Ubuntu](http://wiki.ubuntubrasil.org/TimeDeTraducao).
