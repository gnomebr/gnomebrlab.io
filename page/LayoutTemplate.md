\%TMPL:DEF{\"deste topico somente\"}%

\%TMPL:END%

::: {#container}
::: {#top}
-   [Home](http://www.gnome.org)
-   [Fundação](http://foundation.gnome.org)
-   [Desenvolvimento](http://developer.gnome.org)
-   [Suporte](http://www.gnome.org/support)
-   [Arte](http://art.gnome.org)
-   [Projetos](http://www.gnome.org/projects)
-   [Notícias](http://www.gnomedesktop.org)
:::

::: {#header}
<div>

![GNOME BRASIL](LayoutTemplate/logo.gif)

</div>
:::

::: {#menu}
-   [Início](/WebHome){.foswikiCurrentWebHomeLink}
-   [O GNOME](Gnome.html)
    -   [O que é GNOME?](Gnome.html)
    -   [Um pouco de história](Historia.html)
    -   [O que é GNOME Brasil?](index.html)
    -   [Quem usa?](QuemUsa.html)
    -   [A Fundação GNOME](Fundacao.html)
-   [Lançamentos](Lancamentos.html)
-   [Comunidade](Comunidade.html)
    -   [Eventos](Eventos.html)
    -   [Reunioes](Reunioes.html)
-   [Documentação](Documentacao.html)
    -   [Tutoriais](Tutoriais.html)
    -   [Suporte Comunitário](SuporteComunitario.html)
    -   [Vídeos](Videos.html)
    -   [Apresentações](Apresentacoes.html)
-   [Colabore](Colabore.html)
    -   [Tradução](Traducao.html)
    -   [Desenvolvimento](Desenvolvimento.html)
    -   [GNOME Journal](Jornal.html)
    -   [Divulgação](MaterialDivulgacao.html)
    -   [Estande em Eventos](Estande.html)
    -   [Páginas Amigas](PaginasAmigas.html)
-   [Download](Download.html)
-   [Planeta](http://planeta.br.gnome.org)
-   [Twitter](http://www.twitter.com/gnomebrasil)
-   [Identi.ca](http://identi.ca/gnomebrasil)
:::

::: {#content}
::: {#side-bar}
::: {#side-bar-in}
[]{#Projeto_GNOME} Projeto GNOME
--------------------------------

O GNOME é um [projeto Internacional de software
livre](http://www.gnome.org/) que provê basicamente duas soluções: *o
ambiente desktop* GNOME, intuitivo e atraente para usuários finais; e a
*Plataforma de Desenvolvimento* GNOME, um framework extenso para
construção de aplicações que se integrem com todo o desktop.
:::
:::

::: {#page-text}
::: {#parents}
[Tópicos pais:]{#parents-pais} [ProjetoGrafico](/ProjetoGrafico)
» [Aqui]{#parents-aqui} 
:::

\%TEXT%

::: {#formTWiki}
:::

::: {#anexos}
::: {.foswikiAttachments}
  [I](/LayoutTemplatesortcol=0;table=1;up=0#sorted_table "Sort by this column")   [Attachment](/LayoutTemplatesortcol=1;table=1;up=0#sorted_table "Sort by this column")   [Action](/LayoutTemplatesortcol=2;table=1;up=0#sorted_table "Sort by this column")               [Size](/LayoutTemplatesortcol=3;table=1;up=0#sorted_table "Sort by this column") [Date](/LayoutTemplatesortcol=4;table=1;up=0#sorted_table "Sort by this column")   [Who](/LayoutTemplatesortcol=5;table=1;up=0#sorted_table "Sort by this column")   [Comment](/LayoutTemplatesortcol=6;table=1;up=0#sorted_table "Sort by this column")
  ----------------------------------------------------------------------------------------------------------------- -------------------------------------------------------------------------------------------------------------------------- -------------------------------------------------------------------------------------------------------------------------------- -------------------------------------------------------------------------------------------------------------------- -------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------
  ![gif](/pub/System/DocumentGraphics/gif.gif){width="16" height="16"}[gif]{.foswikiHidden}                         [logo.gif](LayoutTemplate/logo.gif)                                                                           [manage](/bin/attach/GNOMEBR/LayoutTemplate?filename=logo.gif;revInfo=1 "change, update, previous revisions, move, delete...")                                                                                                                  5.6 K [01 Jul 2007 - 15:10]{.foswikiNoBreak}                                                                               [UnknownUser](/Main/UnknownUser)                                                                                     
  ![png](/pub/System/DocumentGraphics/png.gif){width="16" height="16"}[png]{.foswikiHidden}                         [logo.png](/LayoutTemplate/logo.png)                                                                           [manage](/bin/attach/GNOMEBR/LayoutTemplate?filename=logo.png;revInfo=1 "change, update, previous revisions, move, delete...")                                                                                                                 10.2 K [23 Jun 2007 - 20:56]{.foswikiNoBreak}                                                                               [LucasRocha](/Main/LucasRocha)                                                                                       
  ![svg](/pub/System/DocumentGraphics/svg.gif){width="16" height="16"}[svg]{.foswikiHidden}                         [logo.svg](/LayoutTemplate/logo.svg)                                                                           [manage](/bin/attach/GNOMEBR/LayoutTemplate?filename=logo.svg;revInfo=1 "change, update, previous revisions, move, delete...")                                                                                                                 21.3 K [23 Jun 2007 - 20:56]{.foswikiNoBreak}                                                                               [LucasRocha](/Main/LucasRocha)                                                                                       
:::
:::
:::

::: {#rev-info}
rev 15 em 12 May 2012 por [AntonioTerceiro](/Main/AntonioTerceiro)
:::

::: {#btsTWiki}
[**E**ditar](https://br.gnome.org/bin/edit/GNOMEBR/LayoutTemplate?t=0109211243&nowysiwyg=1 "Editar este Tópico"){#btEdit}
[**A**nexar](http://br.gnome.org/bin/attach/GNOMEBR/LayoutTemplate?t=0109211243 "Anexar a este Tópico"){#btAnexar}
[**V**ersões](http://br.gnome.org/bin/rdiff/GNOMEBR/LayoutTemplate "Versões deste Tópico"){#btVersao}
[**M**ais\...](http://br.gnome.org/bin/oops/GNOMEBR/LayoutTemplate?template=oopsmore "Mais opções..."){#btMais}
:::
:::
:::

::: {#bottom}
Copyright © 2007, O Projeto GNOME\
[GNOME](http://www.gnome.org) é **Software Livre**.\
GNOME e a pegada são marcas registradas da [GNOME
Foundation](http://foundation.gnome.org).\
Este site está hospedado em
[Wiki.SoftwareLivre.Org](http://wiki.softwarelivre.org).
:::
