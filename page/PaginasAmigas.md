%Páginas Amigas

Tendo como referência a iniciativa criada no
[FISL](http://fisl.softwarelivre.org/8.0/www/?q=pt-BR/webfriends), a
comunidade GNOME Brasil está retribuindo a divulgação espontânea de
alguns dos banners do GNOME através da publicação da logomarca das
Páginas Amigas que solicitarem. Para tanto, apenas solicitamos que
notifiquem a [Lucas Rocha](http://live.gnome.org/LucasRocha)
repassando-o dados abaixo:

-   Imagem em **formato** .PNG e **tamanho** 120x50 pixels (formato
    livre e tamanho único);
-   Preferimos imagem com **fundo transparente** (evita-se assim
    sobrepor nosso layout);
-   A URL completa da localização do banner do GNOME Brasil (para nossa
    conferência);
-   A URL a ser apontada pela logomarca publicada nas Páginas Amigas
    (será vista ao clicar);
-   Informar a seção das Páginas Amigas que melhor agrupa a natureza da
    página indicada na URL (ou sugerir outra);
-   Recomendamos fortemente que **não** utilize janelas pop-up para
    divulgação do banner (prática desagrável);
-   O banner pode ser adaptado para ajustar-se ao layout da página
    (liberdade de uso);
-   Sendo o banner inviável na página, pode-se usar outra imagem de
    divulgação, desde que tenha evidência semelhante (veja as
    [diretrizes gerais](http://live.gnome.org/BrandGuidelines) de
    utilização da marca do GNOME); e que siga os padrões de [web badges
    / buttons](http://www.zwahlendesign.ch/en/node/19)
-   O site divulgador deve ser **totalmente funcional em navegadores
    livres**! (sem dependências proprietárias)
-   Páginas que avaliemos terem conteúdo ou material contra o software
    livre ou a livre disseminação do conhecimento ou sejam consideradas
    anti-éticas ou imorais serão recusadas na divulgação de seus links e
    logomarcas.\
    \
    -   ![gnome-br-274x80.png](PaginasAmigas/gnome-br-274x80.png){width="274"
        height="80"}

------------------------------------------------------------------------

## Blogs e páginas pessoais {#Blogs_e_paginas_pessoais}

\
[Jorge Pereira](http://www.jorgepereira.com.br/) [![Yata
Technology](PaginasAmigas/yata-technology.png){width="120"
height="51"}](http://yatatechnology.blogspot.com/)

------------------------------------------------------------------------

## Empresas

[![Training
Tecnologia](PaginasAmigas/training-tecnologia.jpg){width="120"
height="50"}](http://trainingtecnologia.wordpress.com/)

------------------------------------------------------------------------

## Revistas, jornais e outras publicações online {#Revistas_jornais_e_outras}

[![Revista Espírito
Livre](PaginasAmigas/revista-espirito-livre.jpg){width="120"
height="50"}](http://www.revista.espiritolivre.org/)\

------------------------------------------------------------------------

## Cooperativas livres {#Cooperativas_livres}

\

------------------------------------------------------------------------

## Organizações da sociedade civil {#Organizacoes_da_sociedade_civil}

\

\

------------------------------------------------------------------------
