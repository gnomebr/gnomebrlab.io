[]{#Lan_amentos} Lançamentos
============================

-   **[GNOME 3.26](https://help.gnome.org/misc/release-notes/3.26/)**
    -   [Lançado em \*13 de Setembro de
        2017\*](https://www.gnome.org/news/2017/09/gnome-3-26-released/)
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/3.26/index.html.pt_BR)
    -   [Vídeo de introdução ao GNOME
        3.26](https://www.youtube.com/watch?v=PpO4qQWFu8A)

<!-- -->

-   **[GNOME 3.24](https://help.gnome.org/misc/release-notes/3.24/)**
    -   [Lançado em \*22 de Março de
        2017\*](https://www.gnome.org/news/2017/03/gnome-3-24-released/)
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/3.24/index.html.pt_BR)
    -   [Vídeo de introdução ao GNOME
        3.24](https://www.youtube.com/watch?v=_Z1PAXiyTB0)

<!-- -->

-   **[GNOME 3.22](https://help.gnome.org/misc/release-notes/3.22/)**
    -   [Lançado em \*21 de Setembro de
        2016\*](https://www.gnome.org/news/2016/09/gnome-3-22-released-the-future-is-now/)
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/3.22/index.html.pt_BR)
    -   [Vídeo de introdução ao GNOME
        3.22](https://youtube.com/watch?v=LhY7rpWXm1Y)

<!-- -->

-   **[GNOME 3.20](https://help.gnome.org/misc/release-notes/3.20/)**
    -   [Lançado em \*23 de Março de
        2016\*](https://www.gnome.org/news/2016/03/gnome-3-20-released/)
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/3.20/index.html.pt_BR)
    -   [Vídeo de introdução ao GNOME
        3.20](https://www.youtube.com/watch?v=JU2f_jkPRq4)

<!-- -->

-   **[GNOME 3.18](https://help.gnome.org/misc/release-notes/3.18/)**
    -   [Lançado em \*23 de Setembro de
        2015\*](https://www.gnome.org/news/2015/09/gnome-3-18-released-brings-big-improvements/)
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/3.18/index.html.pt_BR)
    -   [Vídeo de introdução ao GNOME
        3.18](https://www.youtube.com/watch?v=xu0VSKvfNEI)

<!-- -->

-   **[GNOME 3.16](https://help.gnome.org/misc/release-notes/3.16/)**
    -   Lançado em **25 de Março de 2015**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/3.16/index.html.pt_BR)
    -   [Vídeo de introdução ao GNOME
        3.16](https://www.youtube.com/watch?v=WxRLa5hTGkg)

<!-- -->

-   **[GNOME 3.14](https://help.gnome.org/misc/release-notes/3.14/)**
    -   Lançado em **24 de Setembro de 2014**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/3.14/index.html.pt_BR)
    -   [Vídeo de introdução ao GNOME
        3.14](https://www.youtube.com/watch?v=7p8Prlu3owc)

<!-- -->

-   **[GNOME 3.12](https://help.gnome.org/misc/release-notes/3.12/)**
    -   Lançado em **24 de Março de 2014**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/3.12/index.html.pt_BR)
    -   [Vídeo de introdução ao GNOME
        3.12](https://www.youtube.com/watch?v=n77cwRJUrLg)

<!-- -->

-   **[GNOME 3.10](https://help.gnome.org/misc/release-notes/3.10/)**
    -   Lançado em **25 de Setembro de 2013**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/3.10/index.html.pt_BR)

<!-- -->

-   **[GNOME 3.8](https://help.gnome.org/misc/release-notes/3.8/)**
    -   Lançado em **27 de Março de 2013**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/3.8/index.html.pt_BR)

<!-- -->

-   **[GNOME 3.6](https://help.gnome.org/misc/release-notes/3.6/)**
    -   Lançado em **26 de Setembro de 2012**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/3.6/index.html.pt_BR)

<!-- -->

-   **[GNOME 3.4](https://help.gnome.org/misc/release-notes/3.4/)**
    -   Lançado em **28 de Março de 2012**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/3.4/index.html.pt_BR)

<!-- -->

-   **[GNOME 3.2](https://help.gnome.org/misc/release-notes/3.2/)**
    -   Lançado em **28 de Setembro de 2011**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/3.2/index.html.pt_BR)

<!-- -->

-   **[GNOME 3.0](https://help.gnome.org/misc/release-notes/3.0/)**
    -   Lançado em **06 de Abril de 2011**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/3.0/index.html.pt_BR)

<!-- -->

-   **[GNOME 2.32](https://help.gnome.org/misc/release-notes/2.32/)**
    -   Lançado em **29 de Setembro de 2010**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/2.32/index.html.pt_BR)

<!-- -->

-   **[GNOME 2.30](https://help.gnome.org/misc/release-notes/2.30/)**
    -   Lançado em **31 de Março de 2010**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/2.30/index.html.pt_BR)

<!-- -->

-   **[GNOME 2.28](https://help.gnome.org/misc/release-notes/2.28/)**
    -   Lançado em **23 de Setembro de 2009**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/2.28/index.html.pt_BR)

<!-- -->

-   **[GNOME 2.26](https://help.gnome.org/misc/release-notes/2.26/)**
    -   Lançado em **18 de Março de 2009**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/2.26/index.html.pt_BR)

<!-- -->

-   **[GNOME 2.24](https://help.gnome.org/misc/release-notes/2.24/)**
    -   Lançado em **24 de Setembro de 2008**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/2.24/index.html.pt_BR)

<!-- -->

-   **[GNOME 2.22](https://help.gnome.org/misc/release-notes/2.22/)**
    -   Lançado em **12 de Março de 2008**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/2.22/index.html.pt_BR)

<!-- -->

-   **[GNOME
    2.20](https://help.gnome.org/misc/release-notes/2.20/notes/pt_BR/index.html)**
    -   Lançado em **19 de Setembro de 2007**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/2.20/notes/pt_BR/index.html)

<!-- -->

-   **[GNOME 2.18](https://help.gnome.org/misc/release-notes/2.18)**
    -   Lançado em **14 de Março de 2007**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/2.18/notes/pt_BR/index.html)

<!-- -->

-   **[GNOME 2.16](https://help.gnome.org/misc/release-notes/2.16/)**
    -   Lançado em **6 de Setembro de 2006**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/2.16/notes/pt_BR/index.html)

<!-- -->

-   **[GNOME 2.14](https://help.gnome.org/misc/release-notes/2.14)**
    -   Lançado em **15 de Março de 2006**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/2.14/notes/pt_BR/index.html)

<!-- -->

-   **[GNOME 2.12](https://help.gnome.org/misc/release-notes/2.12/)**
    -   Lançado em **7 de Setembro de 2005**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/2.12/notes/pt_BR/index.html)

<!-- -->

-   **[GNOME 2.10](https://help.gnome.org/misc/release-notes/2.10/)**
    -   Lançado em **9 de Março de 2005**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/2.10/notes/pt_BR/index.html)

<!-- -->

-   **[GNOME 2.8](https://help.gnome.org/misc/release-notes/2.8/)**
    -   Lançado em **15 de Setembro de 2004**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/2.8/) (em
        inglês)

<!-- -->

-   **[GNOME 2.6](https://help.gnome.org/misc/release-notes/2.6/)**
    -   Lançado em **24 de Março de 2004**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/2.6/) (em
        inglês)

<!-- -->

-   **[GNOME 2.4](https://help.gnome.org/misc/release-notes/2.4/)**
    -   Lançado em **10 de Setembro de 2003**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/2.4/) (em
        inglês)

<!-- -->

-   **[GNOME 2.2](https://help.gnome.org/misc/release-notes/2.2/)**
    -   Lançado em **6 de Fevereiro de 2003**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/2.2/) (em
        inglês)

<!-- -->

-   **[GNOME 2.0](https://help.gnome.org/misc/release-notes/2.0/)**
    -   Lançado em **27 de Junho de 2002**
    -   [Notas de
        lançamento](https://help.gnome.org/misc/release-notes/2.0/) (em
        inglês)
