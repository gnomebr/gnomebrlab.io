[]{#Fomul_rio_de_Uso_do_Gnome} Fomulário de Uso do Gnome
========================================================

[LucasRocha](/Main/LucasRocha): Essa foi a proposta considerada pelo
grupo:

-   Instituição
-   URL da instituição
-   Cidade
-   UF
-   Natureza:
    -   Opções: Público, Privado, Terceiro Setor, Outros
-   Distribuição ou Sistema Operacional
    -   Opções: Debian, Mandrake, Ubuntu, Fedora, Outros
    -   Link de distribuições:
        <http://www.gnome.org/~davyd/footware.shtml>
-   Versão do Gnome
    -   Opções: 2.14, 2.16, 2.18
    -   Adicionar versões mais antigas à lista de versões. Sugiro \>=
        2.10.
-   Número de instalações
-   Número de usuários
-   Precisou customizar o Gnome? Se sim, qual customização?
    -   Os campos \"Precisou customizar o Gnome?: \" e \"Se sim, qual
        customização? :\" podem ser mesclados em um só campo de texto.
-   Por que Gnome? (Quem preencher deve adicionar um comentário numa
    caixinha logo abaixo.)
    -   Opções: padrão da distro, interface gráfica bonita, facilidade
        de uso, indicação do prestador de serviço, facilidade de
        customização, outros.
    -   Observação Estas opções devem ser checkbox para marcar várias
        opções
-   O que falta no Gnome hoje (do ponto de vista da organização)?
    -   deve ser um área de texto e não um campo simples
-   URL com fotos de utilização (opcional?)
-   Screenshot do Gnome utilizado (opcional)
-   Adicionar um campo com o email do responsável pela organização
    explicando qual será o uso do email.

------------------------------------------------------------------------

A distribuição será um select ou um texto para a pessoa colocar o nome
da distro? Eu acho que poderíamos ter um select
![smile](/pub/System/SmiliesPlugin/smile.gif "smile")

\-- [LeandroSantos](/Main/LeandroSantos) - 05 May 2007

[]{#T_picos_Relacionados} Tópicos Relacionados
----------------------------------------------

-   [UsoGnomeForm](UsoGnomeForm.html)
-   [CadastroOrganizacao](CadastroOrganizacao.html)
-   [CadastroOrganizacaoTemplate](CadastroOrganizacaoTemplate.html)
-   [FomularioUF](FomularioUF.html)
-   [FomularioNatureza](FomularioNatureza.html)
-   [FomularioGnomeVersao](FomularioGnomeVersao.html)
-   [FomularioDistribuicao](FomularioDistribuicao.html)

::: {.commentPlugin .commentPluginPromptBox}
  -- ---
      
  -- ---
:::
