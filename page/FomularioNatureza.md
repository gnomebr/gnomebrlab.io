[]{#Escolha_a_Natureza_da_Organiza_o} Escolha a Natureza da Organização
=======================================================================

[]{#Instru_es} Instruções
-------------------------

Inclua este tópico para ter uma caixa *dropdown* com uma lista de tipos
de natureza de organizações.

Exemplo:

    <form ...>
    ...
    %INCLUDE{FomularioNatureza}%
    ...
    </form>

***Atenção:*** o nome do campo gerado é `Natureza`.

[]{#C_digo} Código
------------------

Escolha\...Orgão PúblicoEmpresa PrivadaCooperativaAssociação ou
FundaçãoMovimento SocialOutros

\-- [LeandroSantos](/Main/LeandroSantos) - 19 May 2007
