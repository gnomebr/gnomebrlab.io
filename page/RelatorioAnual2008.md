%Relatório Anual 2008 - GNOME Brasil

## Eventos {#Eventos}

Em abril o GNOME Brasil marcou mais uma vez sua presença no
[FISL](FISL.html), maior evento de software livre do Brasil. Tivemos
um estande e um evento comunitário. Muitas pessoas estiveram presentes
no nosso estande, principalmente convidando-nos para outros eventos e
perguntando como contribuir!

Jorge Pereira - de Pernambuco, um dos mais novos hackers do GNOME -
ministrou palestras sobre como colaborar com o GNOME, em três eventos no
Recife, que aconteceram em abril, julho e outubro.

Em agosto, Jonh Wendell esteve em Fortaleza-CE, no
[CeSOL](CeSOL.html), ministrando uma palestra sobre GNOME e um
workshop de programação GTK+. Também esteve presente no Debian Day em
Maceió-AL, falando sobre o projeto GNOME.

Em setembro, Vicente Aguiar apresentou e publicou um artigo sobre o
Projeto GNOME no XXXII Encontro da ANPAD (Associação Nacional de
Pós-Graduação e Pesquisa em Administração), mais especificamente na área
de \"Impactos Socioculturais dos Sistemas de Informação\", que aconteceu
no Rio de Janeiro. O Título do artigo foi: *Software Livre e o Modelo
Colaborativo de Produção entre Pares: uma Análise Organizacional sobre o
Projeto GNOME.*

No final de outubro tivemos o 5º Fórum GNOME. Aconteceu em Foz do
Iguaçu, junto do Latinoware, e, como de costume, reuniu GNOMErs do
Brasil todo. Bem como da América Latina. Tivemos nosso 1º Encontro
Latino Americano do GNOME. Simplesmente fantástico! A meta para 2009 é
fazer o 6º fórum com o mesmo sucesso que foi o 5º. Agradecimentos
especiais a Izabel Valverde por tal feito.

## Tradução {#Traducao}

A equipe brasileira de tradução continua fazendo maravilhosamente seu
trabalho, entregando versões 100% traduzidas. Tivemos um crescimento bem
legal no número de tradutores e melhoramos nosso fluxo de tradução
graças ao uso do Vertimus. Tanto o GNOME 2.22 quanto principalmente o
GNOME 2.24 foram completamente revisados em função do aprimoramento do
Vocabulário Padrão, e as iniciais maiúsculas passaram a atender aos
critérios da Academia Brasileira de Letras, e não mais da língua
inglesa.

Membros {#Membros}

A quantidade de membros brasileiros na GNOME Foundation aumentou este
ano! Tivemos o ingresso de Vladimir Melo, Licio Fernando, Fábio Nogueira
e Leonardo Fontenelle. No total, temos 11 brasileiros na GNOME
Foundation.
