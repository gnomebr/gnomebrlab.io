[]{#Gloss_rio} Glossário
========================

Shaun
[McCance[?](/bin/edit/GNOMEBR/McCance?topicparent=GNOMEBR.Glossario "Create this topic")]{.foswikiNewLink}
está conduzindo uma revisão da [terminologia de interface e documentação
no GNOME](http://live.gnome.org/DocumentationProject/Terminology). (Leia
o
[anúncio](http://mail.gnome.org/archives/gnome-doc-list/2008-April/msg00001.html).)
Ele pretende enviar quase diariamente uma nova palavra à [lista de
discussão da equipe de documentação do
GNOME](http://mail.gnome.org/mailman/listinfo/gnome-doc-list), gerando
uma discussão a fim de padronizar a terminologia da interface e da
documentação do GNOME.

À medida que esses termos forem sendo discutidos pelos documentadores do
GNOME, eu ([LeonardoFontenelle](LeonardoFontenelle.html)) iniciarei
discussões paralelas entre na [lista de discussão do
LDP-BR](http://linux-br.conectiva.com.br/mailman/listinfo/ldp-br/), que
é a zona de convergência das equipes brasileiras de tradução de software
livre. As traduções serão ajustadas no [Vocabulário
Padrão](http://vp.godoy.homeip.net) quando as seguintes condições
tiverem sido safeitas:

-   A discussão no GTP deve ter chegado ao fim;
-   Os tradutores na lista do LDP-BR devem ter chegado a um consenso ou
    à formação de uma grande maioria; e
-   A discussão no LDP-BR deve ter durado ao menos 5 dias.
