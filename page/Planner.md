%Planner

[![planner-finalgantt-en-r1.0\_thumb.png](/Planner/planner-finalgantt-en-r1.0_thumb.png){width="171"
height="133"}](http://live.gnome.org/Planner/Screenshots)

O [Planner](http://live.gnome.org/Planner/About) é uma ferramenta livre
para gestão de projetos em geral que oferece uma variedade de
funcionalidades para este fim.

\
## Dicas e Manhas! {#cicas_e_manhas}

-   [Gerenciando seus Projetos com o
    Planner](http://www.redhat.com/magazine/009jul05/features/planner/),
    *Em Inglês*
