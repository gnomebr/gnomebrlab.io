%Tutoriais

![gnome-tutorial-text.png](Tutoriais/gnome-tutorial-text.png){width="133"
height="133"}

Esta página visa agregar referências de guias e tutoriais (de
preferência em português) para usuários e desenvolvedores do GNOME. No
[site geral de documentos oficiais do GNOME](https://help.gnome.org/)
(GNOME Help) você pode ter acesso a todo o acervo de documentos para
usuários e desenvolvedores do GNOME (apenas alguns estão traduzidos para
o português brasileiro).

\
\

## Para Usuários {#usuarios}

-   [Guia Oficial do Usuário do GNOME](https://help.gnome.org/users/gnome-help/stable/index.html.pt_BR)

<!-- -->

-   [Manual do Usuário GNOME da CELEPAR v1.0-1]
    (/Tutoriais/Manual_Usuario_Gnome.pdf) 
    (1.7MB)

<!-- -->

-   [Primeiros Passos com o GNOME 2.22 no openSUSE 11.0]
    (http://valeo.googlecode.com/svn/trunk/opensuse/Primeiros%20Passos%20com%20o%20GNOME%20no%20openSUSE%2011.0.pdf) 
    (1.6MB)

\

## Para Administradores de sistemas {#administradores}

-   [Guia de Administração de Sistema do Desktop GNOME](https://help.gnome.org/admin/system-admin-guide/stable/index.html.pt_BR)

<!-- 

-   *(Adicione links aqui.)*
-->
\

## Para Desenvolvedores {#desenvolvedores}

-   [Manual do Construtor de Interfaces Glade](https://developer.gnome.org/glade/stable/)

<!-- -->

-   [Gtk+ Beginners Guide (C)](https://developer.gnome.org/gtk3/stable/)

<!-- -->

-   [Programando com gtkmm
    (C++)](https://developer.gnome.org/gtkmm-tutorial/stable/index.html.pt_BR)

<!-- -->

-   [PyGTK FAQ (inglês)](http://faq.pygtk.org/index.py?req=index)

<!-- -->

-   [Gtk\# Beginners Guide
    (C\#)](http://www.mono-project.com/GtkSharpBeginnersGuide)

<!-- -->

-   *(Adicione links aqui.)*
