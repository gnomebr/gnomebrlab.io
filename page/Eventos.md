[]{#Eventos_com_a_presen_a_do_GNOME} Eventos com a presença do GNOME Brasil
===========================================================================

[]{#2011} 2011
--------------

-   [FISL 12, Junho/Julho de 2011](FISL12.html)
-   [8º Fórum GNOME, Mẽs de
    2011[?](/bin/edit/GNOMEBR/VIIIFORUM?topicparent=GNOMEBR.Eventos "Create this topic")]{.foswikiNewLink}

[]{#2010} 2010
--------------

-   [ Latinoware, Novembro de
    2010[?](/bin/edit/GNOMEBR/LATINOWARE2010?topicparent=GNOMEBR.Eventos "Create this topic")]{.foswikiNewLink}
-   [7º Fórum GNOME, Novembro de 2010](VIIFORUM.html)
-   [ SOLISC , Outubro de
    2010[?](/bin/edit/GNOMEBR/SOLISC2010?topicparent=GNOMEBR.Eventos "Create this topic")]{.foswikiNewLink}
-   [ EMSL , Outubro de
    2010[?](/bin/edit/GNOMEBR/EMSL2010?topicparent=GNOMEBR.Eventos "Create this topic")]{.foswikiNewLink}
-   [ Universidade Livre RJ 2010 , Outubro de
    2010[?](/bin/edit/GNOMEBR/ULIVRERJ?topicparent=GNOMEBR.Eventos "Create this topic")]{.foswikiNewLink}
-   [LINUX IN RIO 2010, Setembro de 2010](LINUXINRIO.html)
-   [FISL 11, Julho de 2010](FISL11.html)
-   [FUDCon Latam 2010, Julho de 2010](FUDCONLATAM2010.html)
-   [IV ENSOL, Maio de 2010](ENSOL.html)

[]{#2009} 2009
--------------

-   [FISL 10, Junho de 2009](FISL10.html)
-   [III ENSL, Maio de 2009](ENSL.html)

[]{#2008} 2008
--------------

-   [5º Fórum GNOME, Outubro de 2008](VForum.html)

<!-- -->

-   [I Encontro de Software Livre da Universidade Católica de
    Pernambuco, Outubro de 2008](ESLUCAP.html)

<!-- -->

-   [Congresso Estadual de Software Livre - Ceará (CESoL-CE), Agosto de
    2008](CeSOL.html)

<!-- -->

-   [ESOL - II Encontro de Software Livre do CEFET-PE, Julho de
    2008](ESOL2008.html)

<!-- -->

-   [FISL 9.0, Abril de 2008](FISL.html)

<!-- -->

-   [II Encontro de Software Livre de Pernambuco, Abril de
    2008](ESLPE.html)

[]{#2007} 2007
--------------

-   [IV Fórum GNOME, Setembro de 2007](Forum.html)
