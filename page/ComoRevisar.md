%Como revisar uma tradução

Essa página descreve como você, [tradutor do GNOME](Traducao.html),
pode conferir sua tradução antes de colocá-la no [Damned
Lies](https://l10n.gnome.org); ou como você pode revisar a tradução de
outro tradutor antes dela ser enviada ao [repositório GIT do
GNOME](https://git.gnome.org).

Como foi [discutido em outubro de 2007]
(https://web.archive.org/web/20081121143845/http://listas.cipsga.org.br/pipermail/gnome-l10n-br/2007-October/004949.html),
todos os tradutores devem revisar suas traduções antes de enviá-las ao Damned Lies.

-   [Verificando créditos de tradução e outras questões
    cosméticas](#Verificando_creditos_de_traducao)
-   [Validando um catálogo com o msgfmt, o pofilter e
    virtaal](#Validando_um_catologo)
-   [Tirando dúvidas com leitura do código fonte](#Tirando_duvidas)
-   [Verificando tradução usando programa compilado no
    JHBuild](#verificando_traducao_com_jhbuild)

------------------------------------------------------------------------

## Verificando créditos de tradução e outras questões cosméticas {#Verificando_creditos_de_traducao}

É sempre bom lembrar de verificar detalhes cosméticos nos catálogos, por
mais que o essencial na tradução seja a sua boa qualidade e sua devida
contextualização. A páginas [Guia do Tradutor](GuiaDoTradutor.html)
e [Guia de Estilo](GuiaDeEstilo.html) trazem muitas informações
neste sentido, porém ressaltamos aqui alguns outros pequenos detalhes:

-   Cabeçalho `LastTranslator` do catálogo: Se você usou um editor de
    texto, certifique-se de atualizar o nome e o e-mail do último
    tradutor (não do revisor).
-   Cabeçalho `PO-Revision-Date` do catálogo: Se você usou um editor de
    texto, certifique-se de atualizar a data e a hora da tradução.
-   Ano do catálogo: Na linha de `Copyright` (geralmente a segunda linha
    do catálogo), o último ano deve ser atualizado para o mesmo ano da
    edição do catálogo
-   Ano de atuação do tradutor: Lembre-se de atualizar/acrescentar o ano
    de atuação na tradução aos créditos, separando os anos com vírgula.
-   `msgid "translator-credits"`: Trata-se de mensagem especial na qual
    deve constar \"NOME SOBRENOME \<E-MAIL\>, YEAR1\[, YEAR2\...\]\" em
    cada linha para cada tradutor que contribuiu para o catálogo.

Note que as ferramentas de tradução, com suporte a Gettext, atualizam
automaticamente os campos `LastTranslator`, `PO-Revision-Date`.
`Virtaal` atualiza, inclusive, o ano de atuação do tradutor no começo do
arquivo.

------------------------------------------------------------------------

## Validando um catálogo com o msgfmt, o pofilter e virtaal {#Validando_um_catologo}

Antes de enviar uma tradução para o Damned Lies, é muito interessante
revisar e \"validar\" a tradução. O Damned Lies acusará se houver um
erro de sintaxe no arquivo Gettext, porém você apenas encontrará
informações detalhadas executando validação por conta própria. Os
comandos abaixo irão ajudar no trabalho de localizar pequenos erros na
tradução, tornando o trabalho dos revisores menos cansativo e/ou
melhorando a tradução.

**msgfmt**

O utilitário `msgfmt` faz parte do pacote `gettext`. Apesar de ele
verificar poucos erros, alguns destes não são apontados por outras
ferramentas. Ele acusará em qual linha está cada erro de sintaxe,
facilitando a detecção do erro no catálogo. A sintaxe é:

    msgfmt -cvo /dev/null arquivo.VERSAO.pt_BR.po

**pofilter**

O `pofilter` faz parte do pacote
[translate-toolkit](http://toolkit.translatehouse.org/) e verifica
mínimos detalhes do arquivo (cuidado, nem tudo apontado é erro).
Recomenda-se o uso da opção `--gnome`, a qual utiliza o estilo de
mensagens do GNOME, e da opção `--lang`, que utiliza motores e
dicionários de verificação ortográfica para acusar erros de escrita em
português. Mantendo o catálogo aberto em sua ferramenta de tradução,
execute o seguinte comando no terminal:

    pofilter --gnome --lang=pt_BR arquivo.po | less

**virtaal**

A ferramenta de tradução `Virtaal`, que é dos mesmos desenvolvedores do
`pofilter`, também é uma opção gráfica interessante para revisão de
catálogos de tradução. Nela, você pode selecionar modo de navegação
\"Verificações de qualidade\" e selecionar o filtro desejado, como, por
exemplo, erros ortográficos, espaços no final da mensagem, acrônimos,
etc. (lembrando: cuidado, que nem tudo apontado é erro).

------------------------------------------------------------------------

## Tirando dúvidas com leitura do código fonte {#Tirando_duvidas}

Em algumas situações, você pode se deparar mensagens ambíguas. Em
outras, a mensagem a ser traduzida possui variáveis (`%s`) colocadas de
forma que dificulta saber se, por exemplo, trata-se de uma quantidade
(ex.: `%d lines` \> `10 linhas`) ou nome de um programa (ex.: `%d lines`
\> `linhas de FOOBAR`).

Ao se deparar em uma situação como essa, é interessante que você acesse
o arquivo no código fonte no qual se encontra a mensagem a ser
traduzida. Para saber qual é esse arquivo e em que linha, basta ver qual
a localização em `#:` na mensagem do catálogo.

Vejamos o exemplo abaixo, retirado do
[pacman](https://wiki.archlinux.org/index.php/pacman) do [Arch
Linux](https://www.archlinux.org/):

    #: src/pacman/query.c:86
    #, c-format
    msgid "%s is owned by %s %s\n"
    msgstr "%s pertence a %s %s\n"

Ao analisar o arquivo `src/pacman/query.c`, na linha 86 (e 87), pode-se
presumir que a primeira variável é um nome de arquivo, a segunda é o
nome do pacote e a terceira é a versão do pacote. Vejamos:

    printf(_("%s is owned by %s %s\n"), filename,
                alpm_pkg_get_name(info), alpm_pkg_get_version(info));

------------------------------------------------------------------------

## Verificando tradução usando programa compilado no JHBuild {#verificando_traducao_com_jhbuild}

Para conhecer como está um programa, com todas suas modificações, novas
mensagens e design, você pode usar o
[JHBuild](https://wiki.gnome.org/Projects/Jhbuild), uma ferramenta para
compilar todo o ambiente do GNOME a partir do código fonte.

Trata-se de uma solução de validação e verificação avançada, pois requer
hardware (CPU, Memória RAM, espaço em disco, etc.) e certas
configurações técnicas. Inclusive, podem aparecer mensagens de erro de
compilações que sejam complexas demais para não-desenvolvedores - quando
isto acontece com você, não perca tempo neste método.

Para usar o JHBuild para verificar tradução, você basicamente precisará:

-   Instalar o JHBuild
-   Configurar o arquivo de configuração jhbuildrc
-   Instalar dependências e compilar pacote
-   Uma vez compilado com sucesso, execute o programa

Para mais informações técnicas sobre o uso do JHBuild, veja seu
[manual](https://developer.gnome.org/jhbuild/unstable/).
