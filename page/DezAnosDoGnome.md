%A Comunidade GNOME celebra 10 anos de Liberdade de Software, Inovação e Adoção da Indústria

**Boston MA, USA \-- 15 de agosto de 2007.** Uma celebração global de um
mês para os 10 anos do GNOME começa nessa semana, culminando em meados
de setembro com o ***Dia da Liberdade de Software*** e com o [lançamento
do GNOME 2.20](http://www.gnome.org/start/2.20/notes/pt_BR/index.html).
Durante esse mês de celebração, os contribuidores do GNOME criarão um
Fotolivro wiki recordando seus 10 anos de história e um \"livro de
receitas\" comemorativo com as receitas de *código aberto*
compartilhadas pelos membros da comunidade GNOME ao redor do mundo.

- [História e Realizações](#historia_e_realizacoes)
- [Algumas Palavras de nossos Fundadores](#palavras_de_fundadores)
- [Palavras dos nossos parceiros do Conselho Consultivo](#palavras_do_conselho)
- [Sobre a Fundação GNOME](#sobre_a_fundacao_GNOME)
- [Seção de Informação](#secao_de_informacao)

-------------------------------------------------

## História e Realizações {#historia_e_realizacoes}

Desde 1997, o projeto GNOME tem crescido de um punhado de
desenvolvedores para uma base de colaboradores formada por
codificadores, documentadores, tradutores, designers de interface,
especialistas de acessibilidade, artistas e testadores entrando na faixa
dos milhares.

Como a plataforma líder em *experiência do usuário* para sistemas
operacionais de Software Livre e de Código Aberto, o GNOME tem recebido
uma maciça adoção comercial e suporte entre distribuidores, tais como
Novell, Red Hat, Sun Microsystems e Ubuntu, além de vendedores de
software que almejam àquelas plataformas, tais como Eclipse, Google,
Mozilla e VMWare.

No início de 2007, o projeto lançou a iniciativa ***GNOME Mobile***,
refletindo o aumento do interesse pela plataforma GNOME para sistemas
móveis e embarcados como o OLPC XO, Nokia N800, OpenMoko Neo1973, a
Plataforma Linux ACCESS e vários dispositivos móveis e embarcados de
finalidades específicas.

Com uma comunidade tão grande de contribuidores individuais e
corporativos, o GNOME tornou-se a força motriz do Software Livre e
Código Aberto em inovação em *experiência do usuário*,
internacionalização, usabilidade e acessibilidade, trazendo os
benefícios do Software Livre e do código aberto para usuários ao redor
do mundo.

## Algumas Palavras de nossos Fundadores {#palavras_de_fundadores}

\"[Nos últimos 10 anos](Historia.html) a comunidade GNOME não apenas
alcançou nosso objetivo de criar um magnífico *desktop*, os aplicativos
e ferramentas de desenvolvimento que vêm com ele: nosso software é agora
um componente central em todo sistema operacional livre disponível, e
nós estamos crescendo cada vez mais em áreas cada vez mais importantes,
tais como dispositivos móveis e embarcados.\" - afirmou Miguel de Icaza,
fundador do Projeto GNOME. \"A Comunidade GNOME continua a se
desenvolver, inovar e surgir com novas idéias para aprimorar a
usabilidade dos softwares livres para todos. Eu espero que os próximos
dez anos sejam tão divetidos e fascinantes quanto os primeiros.\"

Em agosto de 2007, Miguel de Icaza fundou o [\"Projeto Desktop
GNOME\"](http://mail.gnome.org/archives/gtk-list/1997-August/msg00123.html)
como uma \"coleção livre e completa de aplicações e ferramentas de
*desktop* amigáveis \[\...\] baseado inteiramente em software livre.\"

\"Dez anos atrás, usando apenas software livre, você não podia fazer
design gráfico ou ilustrações, você não podia fazer o balanço do seu
talão de cheque, você não podia baixar imagens da sua câmera para o
computador, você não podia fazer ligações telefônicas pela Internet,
você não podia criar uma planilha eletrônica com gráficos de pizza, e
você não podia conectar uma impressora ou disco rígido no seu computador
e esperar que ele funcione,\" relembra Federico Mena Quintero, um ativo
desenvolvedor GNOME ao longo desses dez anos de história. \"Hoje, eu
estou feliz em dizer que nós alcançamos e superamos enormemente o
objetivo original do GNOME. Muito obrigado a todos os colaboradores que
tornaram a visão do GNOME uma realidade. Vocês nos deram liberdade, bons
trabalhos \-- e um grupo inestimável de amigos.\"

Mês passado, durante a Conferência Anual de Usuários e Desenvolvedores
do GNOME (GUADEC), Federico Mena Quintero foi o ganhador do *GNOME Thank
You Pants*, um prêmio anual para o Serviço Excepcional voltado para a
comunidade GNOME. O público presente o aplaudiu de pé durante cinco
minutos.

## Palavras dos nossos parceiros do Conselho Consultivo {#palavras_do_conselho}

\"A base de boa parte da Plataforma Linux ACCESS(tm) é a tecnologia
GNOME de código aberto. A ACCESS está orgulhosa em ser um membro do
Conselho de Consultores da Fundação GNOME, um membro fundador da *GNOME
Mobile Initiative* e um patrocinador do GUADEC. As tecnologias GNOME têm
permitido inovações há uma década inteira, e se tornaram cada vez mais
importantes no campo dos dipositvos móveis. Esperamos ansiosos pelos
próximos dez anos! Feliz aniversário, GNOME! \-- David \'Lefty\'
Schlesinger, Diretor da *Open Source Technologies*, **ACCESS**.

\"10 anos de GNOME é uma tamanha realização. Todos do movimento de
software livre têm uma grande dívida de gratidão a vocês e seu trabalho,
da mesma forma que todo usuário do seu *desktop*. Então, muito amor da
comunidade Ubuntu e um brinde aos próximos 10.\" \-- Mark Shuttleworth,
fundador do Ubuntu, **Canonical.**

\"O Debian tem disponibilizado os lançamentos do GNOME desde suas
primeiras versões, e sente orgulho de ver o Projeto GNOME crescer e
amadurecer nos últimos 10 anos. Sendo um componente chave do nosso
sistema operacional, nós esperamos continuar mantendo as inovações do
GNOME disponíveis para nossos usuários. Feliz aniversário!\" \-- Jordi
Mallach, **Debian**.

\"Como membro fundador do Conselho de Consultores da Fundação GNOME, a
IBM parabeniza o Projeto GNOME nos seus dez anos de vida. A comunidade
GNOME deve estar orgulhosa de suas inovações e do seu esforço para
melhorar a experiência do usuário de *desktop* livre. A usabilidade e
acessibilidade das tecnologias assistivas do GNOME levaram a IBM a
disponibilizar produtos baseados no Eclipse que alavancam os componentes
do GNOME.\" - \-- John Walicki, *Open Client Architect*, **IBM**.

\"A Imendio não teria existido se não fosse pelo GNOME e muito dos
nossos desenvolvedores estão aqui desde o começo. Nós estamos felizes em
ter a oportunidade de participar dessa celebração e todos nós da Imendio
gostaríamos de estender nossos parabéns para todo o projeto!\" \--
Mikael Hallendal, Chefe do setor executivo (Chief Executive Officer -
CEO) e co-fundador, **Imendio AB**.

\"O GNOME tem demonstrado, ao longo dos últimos dez anos, o poder do
software livre, disponibilizando um *desktop* que todas as pessoas ao
redor do mundo podem usar. A Novell está orgulhosa em ter condições para
contribuir para a continuidade desse sucesso.\" \-- Ron Hovsepian,
Presidente, **Novell**.

\"Nós gostaríamos de parabenizar todos envolvidos como o Projeto GNOME,
tanto no passado como no presente, pelas realizações e progresso
surpreendente que o Projeto fez nos seus primeiros dez anos. Nós estamos
honrados em ter feito nossa parte em levar o GNOME além do *desktop* e
trazer a experiência GNOME para novos dispositivos e usuários. Nós temos
muito orgulho em ser contribuidores do Projeto GNOME, um membro do
Conselho de Consultores da Fundação GNOME, um membro fundador da *GNOME
Mobile Initiative*, e um patrocinador do GUADEC. Um brinde aos próximos
10 anos!\" \-- Matthew Allum, CEO, **OpenedHand**.

\"A Red Hat está orgulhosa por ter sido uma patrocinadora firme, e
grande contribuidora, do Projeto GNOME por mais tempo do que qualquer
outra empresa. Nos parabenizamos o time por alcançar este marco. O
amadurecimento do Projeto GNOME, com tantos avanços ao longo da última
década, é prova do trabalho inovador que fará o *desktop* no GNU/Linux
uma realidade para muitos ao redor do mundo nos próximos anos. O
compromisso da Red Hat ao projeto GNOME permanece tão forte quanto
nunca.\" \-- Gerry Riveros, Gerente de Marketing, **Red Hat**.

\"Sun Microsystems parabeniza a Fundação GNOME nestes 10 maravilhosos
anos. Em particular, A Sun e a o envolvimento de membros da comunidade
nos recursos de acessibilidade do GNOME foi peça chave para as
capacidades do GNOME serem usadas pelo Sistema Operacional Solaris (TM)
e foram um facilitador para seu uso em outras ambientes comerciais. Nós
esperamos participar na Comunidade GNOME por um frutífero, acessível e
livre amanhã.\" \-- Simon Phipps, *Chief Open Source Officer*, **Sun
Microsystems**.

## Sobre a Fundação GNOME {#sobre_a_fundacao_GNOME}

Formada por centenas de desenvolvedores voluntários e empresas líderes
do mercado, a Fundação GNOME é uma organização comprometida em apoiar o
desenvolvimento do GNOME. A Fundação é uma organização
sem-fins-lucrativos dirigida por membros que promove apoio financeiro,
organizacional e legal para o Projeto GNOME e ajuda a determinar sua
visão e trajetória. Mais informações sobre a Fundação GNOME podem ser
encontradas em <http://foundation.gnome.org>

## Seção de Informação {#secao_de_informacao}

GNOME Foundation Press Officer\
Jeff Waugh (Sydney, Australia. UTC+10.)\
Email: <gnome-press-contact@gnome.org>\
Celular: +61 423 989 818
