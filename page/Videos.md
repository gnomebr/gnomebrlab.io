%Vídeos

![gnome-tutorial-video.png](Videos/gnome-tutorial-video.png){width="133"
height="133"}

Esta página visa agregar referências para vídeos relacionados ao GNOME.
Sinta-se a vontade para adicionar novos

------------------------------------------------------

## Vídeos oficiais do GNOME {#videos_oficiais}

O canal de youtube
[GNOMEDesktop](https://www.youtube.com/user/GNOMEDesktop) é o canal
oficial para compartilhamento de vídeos. Lá, são compartilhados vídeos
de Introdução ao GNOME (dê uma espiada nos recursos antes de instalar!),
vídeos de apresentações nos GUADEC e muito mais.

## YouTube {#youtube}

Um filtro de busca no sites do
[YouTube](http://br.youtube.com/results?search_query=GNOME+Linux&search=Pesquisar),
sobre os vídeos mais bem votados com o termo \"GNOME Linux\". Se você
gostar de algum vídeo sobre o GNOME em algum destes sites, ajude a
organizar a lista abaixo, votando em seus vídeos preferidos. Veja a
[lista de vídeos atual]
(http://br.youtube.com/results?search_query=GNOME+Linux&search=Pesquisar&so=3&num=20)!

## Outros {#Outros}

Encontrou algum vídeo que poderia estar aqui? Adicione aqui outros links.
