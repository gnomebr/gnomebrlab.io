::: {align="center"}
[]{#F_rum_GNOME} Fórum GNOME
============================

![bannerFixo\_468x60.png](Forum/bannerFixo_468x60.png){width="468"
height="60"}
:::

O IV Fórum GNOME é o evento anual da comunidade brasileira de usuários,
tradutores e desenvolvedores do GNOME. Na sua quarta edição, o Fórum
será um evento comunitário do [II ENSL - Encontro Nordestino de Software
Livre](http://www.ensl.org.br/site/modules/boasvindas/), o maior evento
de software livre do Nordeste, em Aracaju/SE, de 28 a 30 de setembro de
2007.

Além dos membros brasileiros, os debates em Aracaju contaram com a
participação de importantes personalidades internacionais, como é o caso
de Nathan Wilson do [Dream Works](http://www.dreamworks.com/). Confira
então abaixo as fotos e a grade da programação que rolou durante esse
evento no Nordeste.

------------------------------------------------------------------------

[]{#Fotos} Fotos
----------------

-   **[Veja aqui as fotos do evento](FotosForum.html)**

\

------------------------------------------------------------------------

[]{#Slides_dos_Palestrantes} Slides dos Palestrantes
----------------------------------------------------

Para ter acesso aos slides de algumas das palestras apresentadas no IV
Fórum GNOME, acesse a nossa página de
[apresentações](Apresentacoes.html).

\

------------------------------------------------------------------------

[]{#Grade_de_Programa_o} Grade de Programação
---------------------------------------------

Dia 28/09 - Sexta-Feira

09h-10h

[Ensinando GNOME](http://grade.ensl.org.br/pub/programacao/113)

Aparecido Quesada

09h-12h

[Editando Fotografias Digitais com o
GIMP](http://grade.ensl.org.br/pub/programacao/92)

João S. de O. Bueno Calligaris

10h-11h

[Encontro de Usuários, Desenvolvedores e Contribuidores
GNOME.](http://grade.ensl.org.br/pub/programacao/118)

Comunidade

11h-12h

[Um trabalho a troco de nada?\" A resposta da comunidade GNOME para Jô
Soares e Bill Gates à luz da teoria da
Dádiva](http://grade.ensl.org.br/pub/programacao/91)

VicenteAguiar

14h-15h

[TWiki do GNOME Brasil](http://grade.ensl.org.br/pub/programacao/117)

Comunidade

16h-17h

[The Nokia N800, a device powered by
Linux](http://grade.ensl.org.br/pub/programacao/108)

Kenneth Rohde Christiansen

17h-18h

[Campanha de mapeamento sobre as organizações que usam GNOME no
Brasil](http://grade.ensl.org.br/pub/programacao/119)

Comunidade

Dia 29/09 - Sábado

09h-10h

[Gnome 2.20 - O novo Gnome turbinando o seu
desktop](http://grade.ensl.org.br/pub/programacao/90)

Tiago Cardoso Menezes

10h-11h

[Como contribuir com o
GNOME](http://grade.ensl.org.br/pub/programacao/84)

Jonh Wendell

16h-17h

[SUSE Linux do Data Center ao
Desktop](http://grade.ensl.org.br/pub/programacao/112)

Carlos Ribeiro

Dia 30/09 - Domingo

10h-11h

[Migração de aplicações .NET para plataformas livres com
Mono](http://grade.ensl.org.br/pub/programacao/93)

Everaldo Canuto

11h-12h

[GNOME, um \"bazar\"
organizado?](http://grade.ensl.org.br/pub/programacao/89)

VicenteAguiar

14h-15h

[Developing and Instant Messenger using
python](http://grade.ensl.org.br/pub/programacao/109)

Kenneth Rohde Christiansen

\

Ver traduções: [en](Forum.html)

::: {style="margin-left:-9999px;"}
:::
