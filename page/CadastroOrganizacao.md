%Cadastro de Organização

::: {style="padding: 0.25em; color: #006; border: 2px dashed #006; background: #aaf; "}
![edittopic](/pub/System/DocumentGraphics/edittopic.gif){width="16"
height="16"} **Atenção!** Esta página, assim como todas as informações e
formulários relacionados à esta campanha, ainda está em fase de
elaboração. Por favor, não realize nenhum cadastro oficial até que a
campanha seja lançada publicamente.
:::

::: {#cadastro}
Nome da Instituição:
:::

Url da Instituição:

UF:

Escolha\...AcreAlagoasAmapáAmazonasBahiaCearáDistrito FederalEspírito
SantoGoiásMaranhãoMato GrossoMato Grosso do SulMinas
GeraisParáParaíbaParanáPernambucoPiauíRio de JaneiroRio Grande do
NorteRio Grande do SulRondôniaRoraimaSanta CatarinaSergipeSão
PauloTocantins

Cidade:

Natureza:

Escolha\...Orgão PúblicoEmpresa PrivadaCooperativaAssociação ou
FundaçãoMovimento SocialOutros

Distribuição ou Sistema Operacional:

Escolha\...DebianFedoraFreeBSDKNOPPIXMandrivaNetBSDOpenBSDRed
HatSlackwareSuseUbuntuOutra

Versão do GNOME:

Escolha\...\<=2.102.142.162.18

Número de Instalações:

Número de Usuários:

Precisou customizar o GNOME? Se sim, qual customização?

Porque GNOME?:

Facilidade de Customização\
Facilidade de Uso\
Indicação do Prestador de Serviço\
Interface Gráfica Bonita\
Padrão da Distribuição\
Outros\

O que falta no GNOME hoje (do ponto de vista da organização)?

URL com fotos de utilização:

Url da screenshot do GNOME utilizado:
