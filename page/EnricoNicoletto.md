[]{#Enrico_Nicoletto} Enrico Nicoletto
======================================

Brazilian, Gnome translator since January 11, 2008.\
Brasileiro, tradutor Gnome desde 11 de Janeiro de 2008.\
Brasileño, traductor Gnome hasta 11 de Enero de 2008

[]{#projects_projetos_proyectos} projects/projetos/proyectos:
-------------------------------------------------------------

Also collaborated on following projects: / Colaborando também com os
seguintes projetos: / También colabora con los proyectos:

-   Launchpad
-   Project Translation
-   Blog do Dissidente

[]{#Web_Sites} Web Sites:
-------------------------

Blog do Dissidente:

-   <http://liverig.wordpress.com/>

[]{#Details_Detalhes_Detalles} Details/Detalhes/Detalles:
---------------------------------------------------------

-   Wiki:
    [EnricoNicoletto](EnricoNicoletto.html)

<!-- -->

-   Yahoo Respostas: Liverig

[]{#Emails} Emails:
-------------------

-   Gmail: liverig (at) gmail (dot) com

[]{#Visitors_of_this_profile_Visitan} Visitors of this profile / Visitantes deste profile / Visitantes de este perfil:
----------------------------------------------------------------------------------------------------------------------

![](http://www.globetrackr.com/dynimg/0ab0a818b03c338db60535d047739508/150-FFFFFF-FFFFFF-000000-7-5/display.png)
