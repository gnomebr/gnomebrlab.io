[]{#Fotos_do_IV_F_rum_GNOME} Fotos do IV Fórum GNOME
====================================================

::: {#igp1 .igp .jqSlimbox .{itemSelector:'.igpThumbNail', .singleMode:true}}
[![Quesada, Everaldo e Nathan Wilson no terceiro dia fazendo pose para a
posterioridade](/pub/images/GNOMEBR/FotosForum/1/thumb_004.JPG)](/pub/images/GNOMEBR/FotosForum/1/004.JPG "Quesada, Everaldo e Nathan Wilson no terceiro dia fazendo pose para a posterioridade"){.igpThumbNail
.{origUrl:"/FotosForum/004.JPG"}} [![Foto de encerramento IV
Forum
Gnome](/pub/images/GNOMEBR/FotosForum/1/thumb_095.JPG)](/pub/images/GNOMEBR/FotosForum/1/095.JPG "Foto de encerramento IV Forum Gnome"){.igpThumbNail
.{origUrl:"/FotosForum/095.JPG"}} [![JS e Prof Queiroz
confraternizando com os
participantes](/pub/images/GNOMEBR/FotosForum/1/thumb_117.JPG)](/pub/images/GNOMEBR/FotosForum/1/117.JPG "JS e Prof Queiroz confraternizando com os participantes"){.igpThumbNail
.{origUrl:"/FotosForum/117.JPG"}} [![Almoço de
Confraternização no Antigo
Sapatão](/pub/images/GNOMEBR/FotosForum/1/thumb_134.JPG)](/pub/images/GNOMEBR/FotosForum/1/134.JPG "Almoço de Confraternização no Antigo Sapatão"){.igpThumbNail
.{origUrl:"/FotosForum/134.JPG"}}
[![Gnome](/pub/images/GNOMEBR/FotosForum/1/thumb_135.JPG)](/pub/images/GNOMEBR/FotosForum/1/135.JPG "Gnome"){.igpThumbNail
.{origUrl:"/FotosForum/135.JPG"}} [![Militantes Gnome
planejam o
futuro](/pub/images/GNOMEBR/FotosForum/1/thumb_143.JPG)](/pub/images/GNOMEBR/FotosForum/1/143.JPG "Militantes Gnome planejam o futuro"){.igpThumbNail
.{origUrl:"/FotosForum/143.JPG"}} [![Brunobol e Jonh fazendo
um \"hackzinho\" para fechar o dia.
:-)](/pub/images/GNOMEBR/FotosForum/1/thumb_1455924794_9f4e8753d1_b.jpg)](/pub/images/GNOMEBR/FotosForum/1/1455924794_9f4e8753d1_b.jpg "Brunobol e Jonh fazendo um "hackzinho" para fechar o dia. :-)"){.igpThumbNail
.{origUrl:"/FotosForum/1455924794_9f4e8753d1_b.jpg"}}
[![Aparecido Quesada \"ensinando
GNOME\".](/pub/images/GNOMEBR/FotosForum/1/thumb_DSC01408.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSC01408.JPG "Aparecido Quesada "ensinando GNOME"."){.igpThumbNail
.{origUrl:"/FotosForum/DSC01408.JPG"}} [![Vicente
respondendo a Bill Gates e Jô
Soares\...](/pub/images/GNOMEBR/FotosForum/1/thumb_DSC01415.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSC01415.JPG "Vicente respondendo a Bill Gates e Jô Soares..."){.igpThumbNail
.{origUrl:"/FotosForum/DSC01415.JPG"}} [![Izabel se
preocupando com a logística do
Forum.](/pub/images/GNOMEBR/FotosForum/1/thumb_DSC01420.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSC01420.JPG "Izabel se preocupando com a logística do Forum."){.igpThumbNail
.{origUrl:"/FotosForum/DSC01420.JPG"}} [![Brunobol se
apaixonando pelo
OLPC](/pub/images/GNOMEBR/FotosForum/1/thumb_DSC01427.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSC01427.JPG "Brunobol se apaixonando pelo OLPC"){.igpThumbNail
.{origUrl:"/FotosForum/DSC01427.JPG"}} [![Nathan Wilson e
Quesada no
bate-papo.](/pub/images/GNOMEBR/FotosForum/1/thumb_DSC01433.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSC01433.JPG "Nathan Wilson e Quesada no bate-papo."){.igpThumbNail
.{origUrl:"/FotosForum/DSC01433.JPG"}} [![Lançamento Oficial
do TWiki Site do GNOME Brasil
:-)](/pub/images/GNOMEBR/FotosForum/1/thumb_DSC01438.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSC01438.JPG "Lançamento Oficial do TWiki Site do GNOME Brasil :-)"){.igpThumbNail
.{origUrl:"/FotosForum/DSC01438.JPG"}} [![Auditório do
Centro de Convenções de
Aracaju/Se](/pub/images/GNOMEBR/FotosForum/1/thumb_DSC01447.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSC01447.JPG "Auditório do Centro de Convenções de Aracaju/Se"){.igpThumbNail
.{origUrl:"/FotosForum/DSC01447.JPG"}} [![Workshop com o
JS](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9391.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9391.JPG "Workshop com o JS"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9391.JPG"}} [![Mais fotos do
lancamento do
Twiki](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9403.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9403.JPG "Mais fotos do lancamento do Twiki"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9403.JPG"}} [![Mais fotos do
lancamento do
Twiki](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9406.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9406.JPG "Mais fotos do lancamento do Twiki"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9406.JPG"}} [![Mais fotos do
lancamento do
Twiki](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9416.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9416.JPG "Mais fotos do lancamento do Twiki"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9416.JPG"}} [![Tiago falando
sobre o Gnome
2.20](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9441.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9441.JPG "Tiago falando sobre o Gnome 2.20"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9441.JPG"}} [![Tiago falando
sobre o Gnome
2.20](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9442.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9442.JPG "Tiago falando sobre o Gnome 2.20"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9442.JPG"}} [![Vicente, John e
Bruno na palestra do
Tiago](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9453.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9453.JPG "Vicente, John e Bruno na palestra do Tiago"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9453.JPG"}} [![Tiago falando
sobre o Gnome 2.20 e Silvio
documentando!](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9464.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9464.JPG "Tiago falando sobre o Gnome 2.20 e Silvio documentando!"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9464.JPG"}} [![Izabel
prestigiando a palestra do
Tiago](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9481.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9481.JPG "Izabel prestigiando a palestra do Tiago"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9481.JPG"}} [![Auditorio
principal lotado no Forum
Gnome](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9484.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9484.JPG "Auditorio principal lotado no Forum Gnome"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9484.JPG"}} [![John iniciando sua
palestra](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9498.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9498.JPG "John iniciando sua palestra"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9498.JPG"}} [![Nathan Wilson em
sua palestra no
Forum](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9532.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9532.JPG "Nathan Wilson em sua palestra no Forum"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9532.JPG"}} [![Nathan Wilson em
sua palestra no
Forum](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9537.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9537.JPG "Nathan Wilson em sua palestra no Forum"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9537.JPG"}} [![Carlos (Novell) em
sua
palestra](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9543.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9543.JPG "Carlos (Novell) em sua palestra"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9543.JPG"}} [![Everaldo Canuto e
Rodrigo (O futuro do
SL)](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9547.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9547.JPG "Everaldo Canuto e Rodrigo (O futuro do SL)"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9547.JPG"}} [![Tiago falando
sobre o Gnome
2.20](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9556.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9556.JPG "Tiago falando sobre o Gnome 2.20"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9556.JPG"}} [![Izabel e Luciana
(Quem for observador me conta o que esta
faltando)](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9567.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9567.JPG "Izabel e Luciana (Quem for observador me conta o que esta faltando)"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9567.JPG"}} [![Tiago e
Luciana](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9569.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9569.JPG "Tiago e Luciana"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9569.JPG"}} [![Turma do Gnome
reunida](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9572.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9572.JPG "Turma do Gnome reunida"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9572.JPG"}} [![Turma do Gnome
reunida](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9573.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9573.JPG "Turma do Gnome reunida"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9573.JPG"}} [![Turma do Gnome
reunida](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9575.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9575.JPG "Turma do Gnome reunida"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9575.JPG"}} [![Nathan, Lu e
Tiago](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9576.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9576.JPG "Nathan, Lu e Tiago"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9576.JPG"}} [![Vicente iniciando
sua
palestra](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9591.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9591.JPG "Vicente iniciando sua palestra"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9591.JPG"}} [![Silvio com o
\`uniforme\` documentando
tudo](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9595.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9595.JPG "Silvio com o `uniforme` documentando tudo"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9595.JPG"}} [![Vicente, Izabel e
o Coordenador de
mesa](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9603.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9603.JPG "Vicente, Izabel e o Coordenador de mesa"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9603.JPG"}}
[![Izabel](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9606.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9606.JPG "Izabel"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9606.JPG"}} [![Gnomers unidos
para converter o \`Sao
Jorge\`](/pub/images/GNOMEBR/FotosForum/1/thumb_DSCF9614.JPG)](/pub/images/GNOMEBR/FotosForum/1/DSCF9614.JPG "Gnomers unidos para converter o `Sao Jorge`"){.igpThumbNail
.{origUrl:"/FotosForum/DSCF9614.JPG"}}[]{.foswikiClear}
:::
