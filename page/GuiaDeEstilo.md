[]{#Guia_de_estilo_de_tradu_o_do_GNO} Guia de estilo de tradução do GNOME
=========================================================================

Essa página da [equipe brasileira de tradução do
GNOME](Traducao.html) reúne as informações complementares ao [Guia
do tradutor](GuiaDoTradutor.html) para obtermos uma interface mais
usável, acessível e agradável. Se você não encontrar uma informação
aqui, consulte as [diretrizes de interface do
GNOME](http://library.gnome.org/devel/hig-book/stable/), o [guia de
estilo do GNOME](http://library.gnome.org/devel/gdp-style-guide), o
[manual de documentação do
GNOME](http://library.gnome.org/devel/gdp-handbook), e o antigo [guia de
estilo da
Conectiva](http://br.tldp.org/ferramentas/guia_estilo/guia_estilo.html).

[]{#Sum_rio} Sumário
--------------------

[]{#foswikiTOC}

::: {.foswikiToc}
-   [Maiusculização](#Maiusculiza_o)
-   [Aspas](#Aspas)
-   [Tempo e modo verbal](#Tempo_e_modo_verbal)
    -   [Infinitivo](#Infinitivo)
    -   [Presente](#Presente)
    -   [Imperativo](#Imperativo)
    -   [Gerúndio](#Ger_ndio)
-   [Chaves de configuração do
    GConf](#Chaves_de_configura_o_do_GConf)
-   [Convenções de
    documentação](#Conven_es_de_documenta_o)
-   [Miscelânia](#Miscel_nia)
:::

[]{#Maiusculiza_o} Maiusculização
---------------------------------

As iniciais maiúsculas são reservadas para nomes próprios, primeira
palavra das frases, e títulos de documentos. *Save As\...*, por exemplo,
deve ser traduzido como *Salvar como\...*. Da mesma forma, títulos de
seções de um documento de ajuda não devem ter iniciais maiúsculas; veja,
por exemlo, o nome dessa página: *Guia de estilo de tradução do GNOME*.

Para ajudar na correção de traduções que ainda usam as regras de
maiusculização da língua inglesa, use o script `muda-iniciais`
disponível na página
[FerramentasDeTraducao](FerramentasDeTraducao.html).

[]{#Aspas} Aspas
----------------

As aspas devem ser \"duplas, retas\". Em inglês é comum usar \'aspas
simples\', às vezes usando \`crase para abrir aspas\'; em português as
aspas simples são reservadas para uso dentro de aspas duplas (exemplo:
\"Pressione \'OK\' para continuar.\"). Alguns catálogos também usam
"aspas duplas curvas" (AltGr+v e AltGr+b), mas, pela dificuldade de
digitação, preferimos as retas. Devido à [estrutura dos catálogos de
mensagem](http://wiki.ubuntubrasil.org/TimeDeTraducao/CatalogoDeMensagens),
é importante que as aspas sejam escapadas, ou seja, \\\"precedidas de
barra invertida\\\". Algumas ferramentas de tradução já cuidam disso
automaticamente; na dúvida, é só olhar como estão as traduções antigas.

Para ajudar na correção de traduções que ainda usam as regras de
maiusculização da língua inglesa, use o script `muda-aspas` disponível
na página [FerramentasDeTraducao](FerramentasDeTraducao.html).

[]{#Tempo_e_modo_verbal} Tempo e modo verbal
--------------------------------------------

Como podemos notar depois de fazer tantas traduções, o inglês não possui
a quantidade de declinações, nem de flexões de modo e tempo verbal
presentes nas línguas românicas, das quais o português faz parte. E isso
tende a dificultar um pouco o trabalho de tradução. Por exemplo, *Open a
File* poderia ser ao mesmo tempo um comando (*Abrir um arquivo*), uma
descrição do que um comando faz (*Abre um arquivo*) ou ainda um comando
ao usuário para seguir em frente (*Abra um arquivo*). (Nota: *Abre um
arquivo* seria *Opens a File*, mas na prática os autores originais não
se dão ao trabalho de diferenciar, e fica tudo *Open a File*.)

A seguir são apresentados os modos e tempos verbais e seus usos, bem
como as dicas de como detectar cada caso. As dicas consistem geralmente
em observar as linhas de comentário que começam com `#:`, que são
mensagens automáticas geradas pelo `gettext` e que dizem de qual arquivo
a tradução foi retirada.

Caso as dicas não sejam sufucientes para decidir o tempo verbal da
tradução, você pode espiar o código-fonte ou pedir a opinião de outros
membros da equipe. De qualquer forma, se a dificuldade em escolher o
tempo verbal for culpa do desenvolvedor do aplicativo, ou seja, se ele
não nos tiver deixado informações suficientes para traduzir
corretamente, então você deverá abrir um relatório de erro para o
aplicativo, notificando os desenvolvedores da situação. Enquanto o
problema não for resolvido, você deverá deixar a mensagem afetada sem
tradução, mas poderá prosseguir traduzindo o resto do catálogo.

### []{#Infinitivo} Infinitivo

O infinitivo é usado nos menus (nome de menu, de submenu e de item de
menu); em botões de comando e outros controles (*widgets*); e em
descrições curtas de chaves de configuração (ver abaixo).

Nesses casos, as mensagens costumam ser muito curtas; não têm ponto
final, a não ser quando terminam em reticências. No caso de menus e
botões, a mensagem estará em iniciais maiúsculas. Mensagens com teclas
de acesso (como nos dois primeiros exemplos abaixo) praticamente sempre
são traduzidas no infinitivo: itens de menu; botões; rótulos de caixas
de verificação (*checkboxes*, como nos diálogos de preferências) etc..
Em grande parte dos casos, será possível observar que, próximo a essas
mensagens curtas, existem mensagens longas correspondentes, que deverão
ser traduzidas no presente (ver abaixo). As mensagens podem vir de
arquivos `.c`, `.h`, `.py` etc., de acordo com a linguagem, mas também
de arquivos `.glade`, `.ui`, `.schemas.in` e outros. Nesses últimos
casos, o comentário automático exibirá o nome do arquivo com um \".h\"
ao final, mas isso não importa.

Esse é um exemplo de item de menu:

    #: ../gedit/gedit-ui.h:60
    msgid "_Open..."
    msgstr "_Abrir..."

Esse é um exemplo de botão de comando:

    #: ../extensions/error-viewer/ephy-error-viewer-extension.c:104
    msgid "Check _Links"
    msgstr "Verificar _links"

Esse é um exemplo de descrição curta de chave de configuração:

    #: ../extensions/smart-bookmarks/smart-bookmarks.schemas.in.h:1
    msgid "Open search result in new tab"
    msgstr "Abrir os resultados da pesquisa em uma nova aba"

### []{#Presente} Presente

Sempre que a mensagem descrever algo, o verbo principal deverá ser
conjugado no presente. Isso se aplica, por exemplo, a dicas de
ferramentas (*tooltips*); a mensagens de barra de status; a descrições
longas de chaves de configuração (ver abaixo); e a descrições de
miniaplicativos, plug-ins e similares (essas descrições são exibidas em
diálogos de habilitação e remoção desses suplementos).

As mensagens traduzidas no presente são mais longas que as traduzidas no
infinitivo, e aquelas servem justamente para explicar o funcionamento
destas. Muitas vezes um olhar panorâmico no catálogo de configuração vai
permitir que o tradutor perceba um grupo de mensagens no infinitivo,
seguidas de um grupo de mensagens no presente; ou então de mensagens no
infinitivo e no presente, intercaladas. As mensagens a serem traduzidas
no presente costumam terminar em ponto final, embora isso nem sempre
aconteça.

Cada um dos exemplos abaixo corresponde a um dos exemplos acima. O
primeiro exemplo é a mensagem exibida na barra de status quando o item
de menu é selecionado. No caso, não há ponto final, embora isso não seja
uma regra.

    #: ../gedit/gedit-ui.h:61 ../gedit/gedit-window.c:1463
    msgid "Open a file"
    msgstr "Abre um arquivo"

Essa é a descrição daquele botão de comando:

    #: ../extensions/error-viewer/ephy-error-viewer-extension.c:106
    msgid "Display invalid hyperlinks in the Error Viewer dialog"
    msgstr "Exibe hiperlinks inválidos no diálogo Visualizador de erros"

Essa é uma descrição longa de chave de configuração:

    #: ../extensions/smart-bookmarks/smart-bookmarks.schemas.in.h:2
    msgid "Open search result in new tab if true, in new window if false"
    msgstr ""
    "Abre os resultados da pesquisa em uma nova aba se verdadeiro, ou em uma "
    "nova janela se falso."

Esse exemplo não tem correspondência na seção anterior; é a descrição de
uma extensão do Epiphany:

    #: ../extensions/actions/ephy-actions-extension.c:113
    msgid "Customize actions"
    msgstr "Personaliza ações"

### []{#Imperativo} Imperativo

O imperativo é usado para a tradução da descrição dos programas. Essa
descrição está em inglês em arquivos `.desktop.in`, que junto ao
catálogo de mensagens será usado durante a compilação para gerar os
arquivos `.desktop`. A descrição é então exibida quando o ponteiro do
mouse repousa sobre um ícone da área de trabalho; sobre um lançador no
painel do GNOME; ou em um item dos menus *Aplicativos*, *Locais* e
*Sistema*. Exemplo:

    #: ../data/paperbox.desktop.in.in.h:1
    msgid "Browse and tag your documents"
    msgstr "Navegue e marque seus documentos"

Além disso, o imperativo é muito usado na tradução de ajuda. *See also*,
por exemplo, é traduzido como *Veja também*, em vez de *Ver também*.

### []{#Ger_ndio} Gerúndio

O gerúndio é empregado com frequência na tradução de títulos de
subseções da ajuda de um aplicativo. *To Encrypt Files in an Archive*,
por exemplo, é equivalente a *Encrypting Files in an Archive*, e deve
ser traduzido como *Criptografando arquivos em um pacote*.

[]{#Chaves_de_configura_o_do_GConf} Chaves de configuração do GConf
-------------------------------------------------------------------

Alguns catálogos de mensagens contém mensagens cujo arquivo de origem é
algo como `NOME.schemas.in.h`. Na verdade, os arquivos são
`NOME.schemas.in`, e na hora de compilar o programa eles darão origem
(junto aos catálogos de mensagem) aos arquivos `NOME.schemas`, que
armazenam a estrutura das chaves de configuração do programa. Cada chave
de configuração tem uma **descrição curta** e uma **descrição longa**.
Quase sempre a descrição longa tem um ponto final, e a curta, não. Se
for necessário, o tradutor tirar a dúvida espiando o código fonte (é só
pedir orientação na lista de discussão). Para um exemplo, veja essa
[imagem](http://www.soulsphere.org/img/gconf-editor.png) ou abra o
Editor de Configurações (comando `gconf-editor`).

Além de seguir as [diretrizes do GNOME para chaves de
configuração](http://library.gnome.org/devel/hig-book/stable/gconf-keys.html),
os tradutores devem:

-   Sempre terminar as descrições curtas sem ponto final, e as longas
    com ponto final, mesmo que no original não esteja assim.
-   Preferencialmente manter o verbo principal no infinitivo, nas
    descrições curtas; e conjugar no presente, nas descrições longas
    (ver acima).
-   Preferencialmente traduzir *Whether to do (or not) \...* como *Fazer
    \...* nas descrições curtas, e como *Faz ou não \...* na longas.

É necessário pestar atenção especial a termos que estejam entre aspas
duplas no original. Esses termos devem ser mantidos exatamente como no
original! Para um exemplo, confira a respectiva [seção no Guia de
Localização para
Desenvolvedores](http://live.gnome.org/TranslationProject/DevGuidelines/Enclose%20literal%20values%20in%20double%20quotes).
Se você encontrar uma mensagem com um termo desses mas que não esteja
entre aspas duplas, abra um relatório de erro para o produto em questão
(não para a equipe de tradução).

O `pofilter`, ferramenta do [Translate
Toolkit](http://translate.sourceforge.net/) para detecção de possíveis
erros de tradução, tem um filtro que confere se valores literais foram
traduzidos. Esse teste também é invocado como parte da rotina do script
`review`, mencionado na página
[FerramentasDeTraducao](FerramentasDeTraducao.html).

[]{#Conven_es_de_documenta_o} Convenções de documentação
--------------------------------------------------------

Conforme definido em 01/02/2013, abaixo seguem algumas convenções a
serem utilizadas para tradução de documentações Gnome:

   [Termo original    ](/GuiaDeEstilosortcol=0;table=1;up=0#sorted_table "Sort by this column")   [    Deve ser traduzido como](/GuiaDeEstilosortcol=1;table=1;up=0#sorted_table "Sort by this column")
  -------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------
                                                        \"Getting Started\"                                                                                                                  \"Primeiros passos\"
                                                                                                                                                                                                        

[]{#Miscel_nia} Miscelânia
--------------------------

Apesar de *copyright* ser geralmente traduzido como *direitos autorais*,
em mensagens curtas como a dos diálogos *Sobre o Aplicativo* é melhor
usar o termo *copyright*, mesmo. Referência: [Cyberdúvidas da Língua
Portuguesa](http://www.ciberduvidas.com/pergunta.php?id=25963).
