%Código de Conduta do GNOME

## Resumo {#resumo}

O GNOME cria software para um mundo melhor. Nós conseguimos isto nos
comportando bem uns com os outros.

Portanto, este documento sugere que consideremos o comportamento ideal,
assim você sabe o que esperar quando se envolver no GNOME. Isto é o que
somos e o que queremos. Não existe uma aplicação oficial destes
princípios, e este não deve ser interpretado com um documento legal.

## Conselho {#conselho}

-   Seja respeitoso e atencioso:\
    Discordância não é desculpa para mau comportamento ou ataques
    pessoais. Lembre-se que uma comunidade onde as pessoas se sentem
    desconfortáveis não é produtiva.

<!-- -->

-   Seja paciente e generoso:\
    Se alguém pede ajuda é porque precisa. Faça educadamente sugestão da
    documentação específica ou mais locais apropriados quando
    necessários, mas evite respostas agressivas ou vagas tal como
    \"RTFM\".

<!-- -->

-   Suponha que as pessoas pretendem o bem:\
    Lembre-se que as decisões são muitas vezes uma escolha difícil entre
    prioridades concorrentes. Se você discordar, faça-o educadamente. Se
    algo parece ultrajante, veja se você não interpretou errado. Peça
    esclarecimentos, mas não pense o pior.

<!-- -->

-   Tente ser conciso:\
    Evite repetir o que já foi dito. Conversas compridas são difíceis de
    acompanhar, e as pessoas normalmente se sentem atacadas se recebem
    várias mensagens dizendo a mesma coisa.

## Isto aplica-se a {#Isto_aplica_se_a}

-   [GNOME Bugzilla](http://bugzilla.gnome.org/)
-   [Listas de discussão GNOME](http://mail.gnome.org/)
-   [Pessoas](https://wiki.gnome.org/Foundation/CodeOfConduct/Signatures) que tenham
    assinado nossa lista.

**Fonte:** *<https://wiki.gnome.org/Foundation/CodeOfConduct>*
