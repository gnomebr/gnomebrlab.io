%Domínio deste novo site Gnome-BR

::: {style="padding: 0.25em; color: #006; border: 2px dashed #006; background: #aaf; "}
![edittopic](/pub/System/DocumentGraphics/edittopic.gif){width="16"
height="16"} Este tópico existe de forma provisória para ajudar no
desenvolvimento desta TWiki Web.
:::

## A tarefa

A idéia aqui é de apenas definir qual será o domínio da nossa página e
quem ficará responsável (caso seja necessário) de registrar o domínio.
Como ponto de partida, temos os seguintes domínios:

-   <http://br.gnome.org/>
-   <http://www.gnome.org.br/>

## Idéias e Propostas

-   Acho que o nome de domínio br.gnome.org seria bem vindo, como o
    fazem os outros países: es.gnome.org, fr.gnome.org, it.gnome.org e
    nl.gnome.org. Penso que seja inviável a criação de um domínio
    independente ao projeto oficial, se temos a possibilidade de apenas
    possuirmos um alias ou subdomínio no domínio principal. \--
    [FernandoMarostica](/Main/FernandoMarostica) - 02 May 2007 - 20:38

