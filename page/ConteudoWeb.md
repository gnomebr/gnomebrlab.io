Conteúdo da TWiki Web Gnome BR {#Conte_do_da_TWiki_Web_Gnome_BR}
==================================================================

Este tópico existe de forma provisória para ajudar no
desenvolvimento desta TWiki Web.

A tarefa {#a_tarefa}
----------------------

Esta tarefa de desenvolvimento do conteúdo do site envolve basicamente
dois pontos:

1.  **Árvore do site** -\> que envolve tanto a estrutura inicial de menu
    como também seus desdobramentos.
2.  **Texto e Imagens** -\> Depois de fechar o \"esqueleto\" a gente
    parte para os textos e imgens que irão dar vida a web.

Proposta de Árvore {#proposta_de_arvore}
-----------------------------------------

-   Página Inicial
    -   Notícias

<!-- -->

-   Gnome
    -   O que é o Gnome?
    -   O que é o Gnome-BR?
    -   Quem usa?

<!-- -->

-   Lancamentos
    -   Gnome 2.18 (Notas de Lancamentos Traduzidas)
    -   Gnome 2.16
    -   Gnome 2.14
    -   Gnome 2.12

<!-- -->

-   Comunidade
    -   Listas Gnome-BR/Tradução
    -   Planets
    -   IRCs
    -   Orkut?!? :-P
    -   Wikis

<!-- -->

-   Colabore
    -   Tradução
    -   Desenvolvimento
        -   Referências! Baldes de Referências!
    -   Gnome Jornal
    -   Divulgação
        -   Material de Divulgação
        -   Estande em eventos
        -   Páginas amigas

<!-- -->

-   Documentação
    -   Guia do Usuário (On-Line??)
    -   Suporte Comunitário (Debian-Br/ Ubuntu-Br/ Fedora-Br/ etc\...)
    -   Vídeos Demonostrativos (youtube, etc)
    -   Apresentacoes (Slides)

Idéias e propostas {#Ideias e propostas}
------------------------------------------

-   \"Notícias\" e \"Lancamentos\" podem ter a criação e listagem
    automatizadas via TWikiAplication \--
    AurelioAHeckert - 02 May 2007 - 23:34
-   Eu acho que poderíamos adicionar um item no menu onde os usuário
    pudessem ver tutoriais \"made for users\" de coisinhas legais que a
    pessoa poderia fazer com o gnome\... nada muito técnico seriam dicas
    de preferencia com screenshots de como adicionar itens da barra do
    menu por exemplo. eu acho que este item poderia ser adicionado com o
    nome tutoriais em documentação. o que vcs acham? \--
    LeandroSantos - 09 May 2007 - 23:03
-   Por mim, tudo bem. Mas temos que ter o cuidado para não virar um
    suporte, tendo em vista que este já é o papel das distribuições com
    os usuários. \--
    VicenteAguiar - 11 May 2007 - 22:48
-   AH! Para editar o menu, acessem o link:
    <https://twiki.softwarelivre.org/bin/view/GNOMEBR/WebLeftBar> \--
    VicenteAguiar - 12 May 2007 - 00:15

