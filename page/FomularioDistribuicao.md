[]{#Escolha_a_Distribui_o_que_voc_us} Escolha a Distribuição que você usa o Gnome
=================================================================================

[]{#Instru_es} Instruções
-------------------------

Inclua este tópico para ter uma caixa *dropdown* com uma lista de
distribuições a serem escolhidas.

Exemplo:

    <form ...>
    ...
    %INCLUDE{FomularioDistribuicao}%
    ...
    </form>

***Atenção:*** o nome do campo gerado é
`DistribuioouSistemaOperacional`.

[]{#C_digo} Código
------------------

Escolha\...DebianFedoraFreeBSDKNOPPIXMandrivaNetBSDOpenBSDRed
HatSlackwareSuseUbuntuOutra
