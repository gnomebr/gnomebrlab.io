::: {align="center"}
[]{#ENSOL_2010} ENSOL 2010
==========================

[![banner
ENSOL](http://www.ensol.org.br/images/logo.png)](http://www.ensol.org.br/)\
**6 a 9 de maio de 2010**
:::

O [ENSOL](http://www.ensol.org.br/oevento/sobreivensol), Encontro de
Software Livre da Paraíba, é um evento organizado pelo Grupo de Usuários
GNU/Linux da Paraíba -- G/LUG-PB e Projeto Software Livre Paraíba --
PSL-PB. Ambos são iniciativas não governamentais que reúnem mais de 600
estudantes, profissionais, empresários e usuários de Software Livre na
Paraíba e Estados circunvizinhos.

Este ano o GNOME Brasil estará presente, através de um grupo de usuários
e algumas palestras.

------------------------------------------------------------------------

[]{#foswikiTOC}

::: {.foswikiToc}
-   [Programação](#Programa_o)
-   [Estande](#Estande)
-   [Slides dos
    Palestrantes](#Slides_dos_Palestrantes)
-   [Fotos do Evento](#Fotos_do_Evento)
:::

------------------------------------------------------------------------

[]{#Programa_o} Programação
---------------------------

  --------------------- ---------------------------------------------------- -------------------
  Dia 07 - **13:30h**   Conhecendo mais sobre o GNOME e a GNOME Foundation      Jonh Wendell
  Dia 08 - **14:30h**   Colaborando com a Tradução do GNOME                   Antonio Fernandes
  Dia 09 - **11:20h**   GNOME Woman                                            Izabel Valverde
  --------------------- ---------------------------------------------------- -------------------

[Programação completa do
evento](http://www.ensol.org.br/oevento/grade-de-programacao)

[]{#Estande} Estande
--------------------

O GNOME-BR foi confirmado entre as comunidades/grupos do IV
[ENSOL](ENSOL.html). Teremos disponível um
estande, onde podemos nos encontrar, trocar idéias, programar, traduzir,
tirar dúvidas, etc. Haverá sempre alguém disponível durante os dias do
evento.

[]{#Slides_dos_Palestrantes} Slides dos Palestrantes
----------------------------------------------------

Para ter acesso aos slides de algumas das palestras apresentadas no IV
[ENSOL](ENSOL.html), acesse a nossa página
de [apresentações](Apresentacoes.html).

[]{#Fotos_do_Evento} Fotos do Evento
------------------------------------

-   A definir

\
