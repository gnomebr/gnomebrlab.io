%Idéia de Partida

No 8° Fórum Internacional de Software Livre, foi organizado um [evento
comunitário](http://fisl.softwarelivre.org/8.0/papers/pub/programacao/557)
de Integração e articulação do grupo de usuários e desenvolvedores
brasileiros do GNOME Brasil. Neste encontro, foram colocados alguns
encaminhamentos para fortalecermos e ampliarmos a comunidade GNOME
Brasil em terras (ou bits) tupiniquins. Um deles foi a construção de um
site *simples, bonito e colaborativo* para o \"GNOME Brasil\", unindo
assim todos estes sites abaixo num só. Participe e colabore com esta
iniciativa! ![smile](/pub/System/SmiliesPlugin/smile.gif "smile")

-   <http://www.gnome.org.br/index.php>
-   <http://live.gnome.org/GnomeBR/Traducao>
-   <http://br.gnome.org/>
-   <http://gnome-br.sourceforge.net/>

## Desenvolvimento GNOME Brasil Web

[]{#edittable1}

::: {.editTable}
  [Tarefa](/PlanejamentoSitesortcol=0;table=1;up=0#sorted_table "Sort by this column")   [Responsável](/PlanejamentoSitesortcol=1;table=1;up=0#sorted_table "Sort by this column")                                                                                      [Início](/PlanejamentoSitesortcol=2;table=1;up=0#sorted_table "Sort by this column")   [Prazo](/PlanejamentoSitesortcol=3;table=1;up=0#sorted_table "Sort by this column")   [Observações](/PlanejamentoSitesortcol=4;table=1;up=0#sorted_table "Sort by this column")
  ------------------------------------------------------------------------------------------------------------------------ ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------ ----------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------
  Hospedagem do site (TWiki) Gnome BR                                                                                      [VicenteAguiar[?](/bin/edit/Main/VicenteAguiar?topicparent=GNOMEBR.PlanejamentoSite "Create this topic")]{.foswikiNewLink}                                                                                       14/04/07                                                                                                                 19/04/07                                                                                                                ok!
  [Dominio](Dominio.html)                                                                                              [LucasRocha](/Main/LucasRocha)                                                                                                                                                                                   14/04/07                                                                                                                 dd/mm/aa                                                                                                                ok!
  [Conteudo Web](ConteudoWeb.html)                                                                                     [VicenteAguiar[?](/bin/edit/Main/VicenteAguiar?topicparent=GNOMEBR.PlanejamentoSite "Create this topic")]{.foswikiNewLink}, [LucasRocha](/Main/LucasRocha), [LeandroSantos](/Main/LeandroSantos), Quem mais???   14/04/07                                                                                                                 dd/mm/aa                                                                                                                ok!
  [Layout](Layout.html)                                                                                                [ViniciusDepizzol](/Main/ViniciusDepizzol), [LucasRocha](/Main/LucasRocha) e [AurelioAHeckert](/Main/AurelioAHeckert)                                                                                            04/05/07                                                                                                                 dd/mm/aa                                                                                                                ok!
  [FomularioGnome](FomularioGnome.html)                                                                                [LeandroSantos](/Main/LeandroSantos)                                                                                                                                                                             dd/mm/aa                                                                                                                 dd/mm/aa                                                                                                                 
:::

## Como contribuir?

-   Em primeiro lugar, increva-se na **[lista do GNOME
    Brasil](http://mail.gnome.org/mailman/listinfo/gnome-br-list)**,
    apresente-se ao grupo e informe em que parte desta atividade você
    pretende contribuir.
-   Depois leia o **[Tutorial básico do TWiki](/ComoUsarTWiki)**
    e saiba como contribuir diretamente nesta Web.
-   Por fim, atualize a tabela acima (Desenvolvimento
    [GnomeBr](index.html) BR Web) para que todas/os saibam o que
    você está fazendo e informe mais ou menos quando você pretende
    concluir. ![smile](/pub/System/SmiliesPlugin/smile.gif "smile")
