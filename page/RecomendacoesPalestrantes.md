%Orientações ao palestrante

Estas orientações ao palestrante são uma adição ao [Código de
Conduta](CodigoConduta.html), para ajudá-lo a dar palestras que
possam ser apreciadas por toda a plateia.

## Origens {#Origens}

A comunidade GNOME tem pessoas de diversas origens. Portanto, você pode
ofender os membros da plateia involuntariamente. Estas orientações podem
ajudá-lo a evitar isso, minimizando impactos negativos e
descontentamento.

Esta não é uma lista precisa de regras porque a Fundação GNOME não pode
prever todas a circunstâncias. Estas orientações não devem ser
interpretadas como impedimento ao mais importante: Agir com boa-fé e ter
cuidado com questões legais ou éticas durante uma apresentação.

A aplicação está sujeita ao julgamento do palestrante.

## Orientações {#Orientacoes}

-   A comunidade GNOME é positiva e acolhedora. Tenha em mente que as
    pessoas podem ter visões diferente da sua. Portanto, por favor, não
    critique as pessoas ou ideias de forma severa desnecessariamente.
    Atenha-se aos aspectos técnicos e ofereça sugestões construtivas
    para melhoria.

<!-- -->

-   Lembre-se do público fora da sala. Muitas apresentações são gravadas
    e discutidas na Internet. Então seja atencioso com todos, não apenas
    aqueles que estão sentados em frente a você.

<!-- -->

-   Evite coisas que possivelmente ofendam algumas pessoas. O conteúdo
    de sua apresentação deve ser adequado para visualização pela maioria
    da plateia, então, evite slides contendo imagens sexuais, violência
    ou palavrões gratuitos.

<!-- -->

-   Evite assuntos desnecessários. Sua plateia tem pessoas de várias
    origens, de diferente sexualidade, etnia, gênero e religião.
    Referências a estes assuntos não relacionadas ao tópico podem
    facilmente deixar alguém desconfortável.

<!-- -->

-   Um evento GNOME de sucesso envolve a todos com diversão. Se alguém
    em sua plateia estiver desconfortável com algo que você disse, você
    não fez o seu trabalho. Peça desculpas a mesma assim que for
    possível, e tente evitar o tópico que ocasionou isto pelo resto de
    sua apresentação.

<!-- -->

-   A comunidade GNOME é colaborativa. Se você se preocupa com alguma
    coisa que esteja inadequada, pergunte a maioria das pessoas
    previamente.

## Lidando com problemas {#Lidando_com_problemas}

Se você não respeitar estas orientações, alguém da Fundação GNOME irá
levantar a questão com você na próxima oportunidade. Além disso, se
necessário, a Fundação GNOME poderá distanciar-se publicamente de suas
opiniões.

Por favor, tenha em mente que a Fundação GNOME não é o fórum adequado
para debater se alguém se sentiu ofendido ou não; você deve simplesmente
evitar ofender as pessoas, mesmo que você não compartilhe de suas
opiniões. Estas orientações não constituem censura, uma vez que você tem
muitos outros fóruns e oportunidades para dizer o que quiser.

**Fonte:** <https://wiki.gnome.org/Foundation/CodeOfConduct/SpeakerGuidelines>

