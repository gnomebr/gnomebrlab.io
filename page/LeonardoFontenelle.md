[]{#Leonardo_Fontenelle} Leonardo Fontenelle
============================================

**Leonardo Ferreira Fontenelle** traduz voluntariamente o GNOME e
ocasionalmente outros projetos de software livre. Atualmente ocupa a
coordenação da [equipe brasileira de tradução do
GNOME](Traducao.html), e mantém um [blog sobre tradução de software
livre para a língua portuguesa](http://leonardof.org). Para mais
informações, leia esta [página Sobre no
blog](http://leonardof.org/sobre/pt/).
