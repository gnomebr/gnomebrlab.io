%5º Fórum GNOME - Chegadas e Saídas - Foz do Iguaçu

Escreva aqui a data e hora que você chegará em Foz. Assim, será possível
planejar encontros no aeroporto e passeios juntos.

## Quarta, 29 de outubro

-   9:30 - Jonh Wendell
-   12:50 - Vinicius Depizzol
-   13:30 - Izabel Valverde
-   15:50 - Fabio Nogueira
-   16:00 - Diego Escalante Urrelo
-   23:55 - Germán Póo-Caamaño

## Quinta, 30 de outubro

-   15:00 - Pedro Villavicencio Garrido
-   19:30 - Vicente Aguiar e Bruno Bol

## Sexta, 31 de outubro
