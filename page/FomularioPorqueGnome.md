[]{#Escolha_o_motivo_pelo_qual_voc_u} Escolha o motivo pelo qual você usa Gnome
===============================================================================

[]{#Instru_es} Instruções
-------------------------

Inclua este tópico para ter uma caixa *dropdown* com uma lista de
motivos para a utilização do Gnome a ser escolhido.

Exemplo:

    <form ...>
    ...
    %INCLUDE{FomularioGnomeVersao}%
    ...
    </form>

***Atenção:*** o nome do campo gerado é `VersodoGnome`.

[]{#C_digo} Código
------------------

Facilidade de Customização\
Facilidade de Uso\
Indicação do Prestador de Serviço\
Interface Gráfica Bonita\
Padrão da Distribuição\
Outros\

\-- [LeandroSantos](/Main/LeandroSantos) - 19 May 2007
