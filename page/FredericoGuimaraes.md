[]{#Frederico_Gon_alves_Guimar_es_ak} Frederico Gonçalves Guimarães (aka Aracnus)
=================================================================================

Biólogo, professor e entusiasta do software livre desde 1999. Trabalho
atualmente am alguns projetos de tradução: [Claws
Mail](http://claws-mail.org), [Dokuwiki](http://wiki.splitbrain.org),
[GCompris](http://gcompris.net), [Unclassified
NewsBoard](http://newsboard.unclassified.de) entre outros.

**Na Web:**

-   Sítio pessoal: <http://teia.bio.br>
-   Correio eletrônico: frederico arroba teia ponto bio ponto br
-   Comunicador instantâneo (Jabber): frederico arroba teia ponto bio
    ponto br
-   Comunicador instantâneo (GTalk): fgguimaraes arroba gmail ponto com
-   [Identi.ca](http://identi.ca): [aracnus](http://identi.ca/aracnus)
-   [FriendFeed](http://friendfeed.com):
    [aracnus](http://friendfeed.com/aracnus)
