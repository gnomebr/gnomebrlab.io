[]{#Tradu_o_dos_termos_de_uso_do_Sof} Tradução dos termos de uso do Software
----------------------------------------------------------------------------

Muitos programas do GNOME possuem certos avisos que nunca mudam, tais
como termos de compromisso, termos de uso, notificação da licença etc.,
que são necessários quando o software está licenciado sob a GPL. No
sentido de uniformizar a qualidade da tradução, o ideal é usar sempre a
mesma tradução para essas mensagens. A expressão entre os `<>` pode ser
o nome do programa ou simplesmente ***this program*** (este programa).
Por favor, substitua-o com o termo adequado.

`msgid "<program name> is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.\n"`

`msgstr "O <nome do programa> é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos da Licença Pública Geral GNU (GNU General Public License) publicada pela Free Software Foundation; tanto na versão 2 da Licença, quanto (a seu critério) qualquer versão mais nova.\n"`

------------------------------------------------------------------------

`msgid "<program name> is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details."`

`msgstr "O <nome do programa> é distribuído na esperança de ser útil, mas SEM NENHUMA GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou ADEQUAÇÃO A UM PROPÓSITO EM PARTICULAR. Veja a Licença Pública Geral GNU (GNU General Public License) para mais detalhes."`

------------------------------------------------------------------------

`msgid "You should have received a copy of the GNU General Public License along with <program name>; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA\n"`

`msgstr "Você deve ter recebido uma cópia da Licença Pública Geral GNU (GNU General Public License) junto com o <nome do programa>; caso contrário, escreva para a Free Software Foundation, Inc. 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA\n"`

------------------------------------------------------------------------
