%Tradução do GNOME para o Português Brasileiro

::: {style="padding: 0.25em; color: #006; border: 2px dashed #006; background: #aaf; "}
![edittopic](/pub/System/DocumentGraphics/edittopic.gif){width="16"
height="16"} **Atenção!** Este tópico do site está passando por uma boa
reforma! Para orientações sobre tradução leia a **[antiga página de
tradução](TraducaoAntiga.html)** até
que este tópico fique pronto
![wink](/pub/System/SmiliesPlugin/wink.gif "wink")
:::

------------------------------------------------------------------------

Bem-vinda(o) à página da equipe brasileira de tradução do GNOME! Somos
parte do [Projeto de Tradução do GNOME
(GTP)](https://wiki.gnome.org/TranslationProject), cujo objetivo é
oferecer o ambiente em todos os idiomas existentes. Seguimos trabalhando
para que os usuários possam usar o GNOME em seu idioma.

**Coordenador local:** [Leonardo Fontenelle](https://leofontenelle.wordpress.com/).

**Contatos:**

-   Canal \#tradutores - Freenode
    ([irc.freenode.net](irc://irc.freenode.net/#tradutores))

<!-- -->

-   <http://listas.cipsga.org.br/cgi-bin/mailman/listinfo/gnome-l10n-br>

**Veja também:** [Erros Frequentes](ErrosFrequentes.html),
[Tradutores
Trabalhando[?](/bin/edit/GNOMEBR/TradutoresTrabalhando?topicparent=GNOMEBR.TraducaoPrevia "Create this topic")]{.foswikiNewLink},

------------------------------------------------------------------------

**ESSA PÁGINA DEVERÁ SER ATUALIZADA EM BREVE!** Nosso fluxo de trabalho
foi alterado. Enquanto essa página não é corrigida, confira a [ata da
reunião](/TraducaoPrevia?/gnome-br-2008-04-23-20h23.log.gz).

## Sumário {#Sumario}

[]{#foswikiTOC}

::: {.foswikiToc}
-   [Como relatar um
    erro?](#Como_relatar_um_erro)
-   [Como Ajudar?](#Como_Ajudar)
-   [Um resumo do processo de
    tradução](#Um_resumo_do_processo_de_tradu_o)
-   [A tradução](#A_tradu_o)
    -   [O que é traduzido nos arquivos
        .PO?](#O_que_traduzido_nos_arquivos_PO)
    -   [O que devo observar ao editar arquivos
        .PO](#O_que_devo_observar_ao_editar_ar)
-   [Onde pego os arquivos a serem
    traduzidos?](#Onde_pego_os_arquivos_a_serem_tr)
    -   [Qual versão devo
        traduzir?](#Qual_vers_o_devo_traduzir)
-   [Quais programas usar para
    traduzir?](#Quais_programas_usar_para_traduz)
-   [Cuidados necessários durante a realização da
    tradução](#Cuidados_necess_rios_durante_a_r)
-   [Modelo de Cabeçalho](#Modelo_de_Cabe_alho)
    -   [Créditos de
        tradução](#Cr_ditos_de_tradu_o)
-   [Validando uma
    tradução](#Validando_uma_tradu_o)
-   [Terminei e validei a tradução, e
    agora?](#Terminei_e_validei_a_tradu_o_e_a)
-   [Como abrir um relatório no
    Bugzilla?](#Como_abrir_um_relat_rio_no_Bugzi)
    -   [Exemplo de um
        relatório](#Exemplo_de_um_relat_rio)
-   [O Processo de
    Revisão](#O_Processo_de_Revis_o)
-   [Relacionados](#Relacionados)
-   [Ver também](#Ver_tamb_m)
    -   [Outros Projetos de
        Tradução](#Outros_Projetos_de_Tradu_o)
    -   [Projetos de tradução de
        distribuições:](#Projetos_de_tradu_o_de_distribui)
-   [Referências](#Refer_ncias)
:::

------------------------------------------------------------------------

## Como relatar um erro? {#Como_relatar_um_erro}

Caso tenha encontrado um erro na tradução de algum programa GNOME (ops,
errar é humano!), ajude-nos relatando-o. O relato de erros é muito
importante para se garantir um ambiente de qualidade! A forma mais
apropriada para comunicá-lo será através de um relatório do [Bugzilla do
GNOME](http://bugzilla.gnome.org). O relatório será enviado a um membro
do time de tradução que irá verificar e corrigir o erro. Para saber como
agir nesses casos, veja a seção **[Relatando erros no
Bugzilla](#Relatando_erros_no_Bugzilla)**.
Caso esteja interessado em resolver você mesmo o problema ou então se
deseja se tornar um membro. Leia a seção **[Como
Ajudar?](#Como_Ajudar)**.

------------------------------------------------------------------------

## Como ajudar? {#Como_ajudar}

Então, você também quer traduzir o GNOME? Seja bem vindo! Primeiramente
é necessário inscrever-se na nossa [lista de
discussão](http://listas.cipsga.org.br/cgi-bin/mailman/listinfo/gnome-l10n-br)
e apresentar-se. Em seguida, leia a documentação desta página e se
possível, também das páginas relacionadas, isso lhe permitirá conhecer o
funcionamento do processo de tradução, além de oferecer a possibilidade
de realização de um trabalho preciso e de qualidade. Como requisito
mínimo de leitura, temos a seção [Um resumo do processo de
tradução](#Resumo_processo_de_traducao).

------------------------------------------------------------------------

## Um resumo do processo de tradução[]{#Resumo_processo_de_traducao}

É apresentado nesta seção um resumo do processo de tradução. Para
entender todo o processo, é fortemente indicada a leitura de todo o
conteúdo desta página.

Após inscrever-se na nossa [lista de
discussão](http://listas.cipsga.org.br/cgi-bin/mailman/listinfo/gnome-l10n-br)
e apresentar-se, acesse o [Vertimus](http://www.bani.com.br/vertimus) e
cadastre-se lá também. Uma vez cadastrado, você poderá clicar em
\"Interface\" ou \"Documentação\" no menu do lado direito da página (É
interessante lembrar que as traduções de interface possuem prioridade
sobre a documentação.). Feito isso irão aparecer as versões (para saber
sobre qual escolher, leia [Qual versão devo
traduzir?[(#Qual_versao_devo_traduzir)
e classes de aplicativos. Escolha uma classe de aplicativos (entre
*admin, desktop, devtools, platform, proposed*) clicando sobre esta.
Aparecerá uma lista de aplicativos e suas respectivas estatísticas e seu
estado de tradução. Escolha alguma que esteja incompleta e sem ninguém
traduzindo (isto é, se o total for diferente de 100% e a campo estado
estiver em branco) clicando sobre o nome do aplicativo. Na página que
abrirá, baixe o *arquivo.po* e selecione \"Reservar para Tradução\" no
campo \"Ação\" e clique em \"Aplicar\".

Após obter o arquivo com o catálogo de mensagem, realize a tradução das
mensagens utilizando qualquer editor de textos, como o
[Gedit](http://www.gnome.org/projects/gedit/), o
[Emacs](http://www.gnu.org/software/emacs/) ou o
[Vim](http://www.vim.org/), ou então, use programas dedicados, como
[Gtranslator](http://gtranslator.sourceforge.net/),
[poEdit](http://www.poedit.net/), [KBabel](http://kbabel.kde.org/) e
[Pootling](http://translate.sourceforge.net/wiki/), que apresentam uma
série de vantagens. Após traduzir o catálogo de mensagens, é importante
[revisar a
tradução[?](/bin/edit/GNOMEBR/#Validando_uma_tradução?topicparent=GNOMEBR.TraducaoPrevia "Create this topic")]{.foswikiNewLink};
existem ainda [instruções específicas para a revisão da tradução da
documentação](RevisandoTraducaoDeDocumentacao.html). Em seguida,
acesse novamente o site do [Vertimus](http://www.bani.com.br/vertimus) e
entre na página do aplicativo anteriormente reservado por você. No campo
Ação selecione \"Enviar a nova tradução\" e no campo arquivo indique o
arquivo .po compactado no formato .gz, no campo comentário, se
necessário, coloque alguma informação pertinente sobre sua tradução e
clique em Aplicar. Acompanhe através do
[Feed](http://www.bani.com.br/vertimus/rss.php) as eventuais novidades
sobre sua tradução tais como revisões e commits.

------------------------------------------------------------------------

## A tradução {#A_traducao}

Cada pacote possui um catálogo que representa as mensagens que são
exibidas na sua interface. Os catálogos são arquivos compostos de pares
de mensagens, formados por um texto original e o equivalente traduzido.
Cada programa possui o seu próprio catálogo de mensagens por idioma,
embora também existam casos em que programas relacionados compartilhem
os seus catálogos de mensagens. Existem três formatos de catálogos,
vejamos:

-   **PO** - Que vem do acrônimo inglês *\"Portable Object\"* (Objeto
    Portável). É um arquivo de texto com a extensão \".po\", que adota
    em seu conteúdo uma linguagem de marcação especial. É o formato que
    é editado e mantido pelos tradutores. O nome do arquivo deve
    referenciar o idioma de tradução que o arquivo trata, por exemplo,
    `"pt_BR.po"`.
-   **MO** - Que vem do acrônimo inglês *\"Machine Object\"* (Objeto
    Máquina). É um arquivo binário com a extensão \".mo\", gerado a
    partir do arquivo PO e utilizado por um programa para
    internacionalizar sua interface com o usuário. O nome do arquivo
    costuma ser o nome do programa que o arquivo trata, por exemplo,
    `"epiphany.mo"`.
-   **POT** - Que vem do acrônimo inglês *\"Portable Object Template\"*
    (Modelo de Objeto Portável). Este arquivo é gerado pelo
    desenvolvedor da interface, servindo de modelo para que um tradutor,
    ou time de tradução. Ou seja, ele não é editado diretamente, mas
    usado para gerar um catálogo com formato PO no idioma desejado.

A realização da tradução é a simples ação de passar partes do catálogo
de mensagens de um pacote para o português. As \"partes\" que devem ser
traduzidas são explicadas na seção **[O que é traduzido nos arquivos
.PO?[?](/bin/edit/GNOMEBR/#O_que_é_traduzido_nos_arquivos_P?topicparent=GNOMEBR.TraducaoPrevia "Create this topic")]{.foswikiNewLink}**

------------------------------------------------------------------------

### O que é traduzido nos arquivos .PO? {#O_que_traduzido_nos_arquivos_PO}

Apresentamos abaixo, o trecho de um arquivo PO. Note que o arquivo está
apenas com a mensagem original, no campo `msgid`. Como trabalho de
tradução, você deve interpretar a mensagem em inglês e colocá-la no
português, no campo seguinte, chamado `msgstr`.

    #: ../src/widgets/cria-slide-show.c:281
    #, c-format
    msgid "Error disabling xscreensaver: %s"
    msgstr ""

Aqui a mensagem já foi interpretada e traduzida, mantendo a formatação
original.

    #: ../src/widgets/cria-slide-show.c:281
    #, c-format
    msgid "Error disabling xscreensaver: %s"
    msgstr "Erro desabilitando xscreensaver: %s"

Sim, é realmente bem simples!

------------------------------------------------------------------------

### O que devo observar ao editar arquivos .PO {#O_que_devo_observar_ao_editar}

Antes de começar a traduzir, alguns detalhes nos arquivos .PO devem ser
observados. Os arquivos **PO** possuem uma linguagem de marcação
padronizada, cujo objetivo é traçar uma linguagem/protocolo comum de
comunicação entre os tradutores, desenvolvedores e programas. Vejamos
abaixo um exemplo do formato de um arquivo PO:

          6901| # comentário simples
          6902| #: ../nome_do_arquivo.c:001
          6903| #| msgid "texto original anterior"
          6904| msgid "texto original"
          6905| msgstr "texto traduzido"
          6906|
          6907| #. comentário automático
          6908| #: ../nome_do_arquivo.c:002
          6909| msgid "texto original que"
          6910| "pode ser quebrado em quantas"
          6911| "linhas forem necessárias"
          6912| msgstr "texto traduzido que"
          6913| "pode ser quebrado em quantas"
          6914| "linhas forem necessárias"
          6915|
          6916| #: ../nome_do_arquivo.c:003 ../nome_do_arquivo.c:004
          6917| #, sinalizador, sinalizador
          6918| msgid "texto original no singular"
          6919| msgid_plural "texto original no plural"
          6920| msgstr[0] "texto traduzido no singular"
          6921| msgstr[1] "texto traduzido no plural"
          6922|
          6923| #~ msgid "texto original obsoleto"
          6924| #~ msgstr "texto traduzido obsoleto"

Podemos notar, no exemplo citado, que:

1.  Todas as linhas iniciadas com `msgid` indicam o texto original e as
    linhas com `msgstr` indicam o texto traduzido por um tradutor.
2.  Ambas as linhas possuem textos que devem estar entre aspas.
3.  O texto poderá ser quebrado em diversas linhas, como nas diretivas
    (`msgid/msgstr`) exemplificadas pelas linhas **6909** e **6912**.
4.  A linha iniciada com `msgid_plural` representa um texto original no
    plural que deverá ser traduzido em diretivas `msgstr[n]`, onde **n**
    poderá ser um intervalo entre números inteiros de **0** até o número
    de possibilidades que o plural em seu idioma ofereça. Por exemplo,
    acima temos de **0** a **1**, onde **0** é o singular e **1** é o
    plural. A quantidade de possibilidades que o plural no arquivo pode
    possuir deve ser definido no cabeçalho do arquivo.
5.  Todas as linhas iniciadas com tralha `#` são tratados como
    comentários, porém existe uma diferença entre eles, como descrito a
    seguir:
    -   **\#** comentários gerados e mantidos pelo tradutor.
    -   **\#.** comentários automáticos extraídos do código fonte,
        criados pelo desenvolvedor para auxiliar a tradução.
    -   **\#:** local onde o texto é utilizado no(s) código(s) fonte.
    -   **\#,** sinalizador(es).
    -   **\#\|** contém o texto original anterior.
    -   **\#\~** texto(s) obsoleto(s).

Os comentários sinalizadores podem ser iguais a `fuzzy` (aproximada),
muitas vezes são inseridos pelo desenvolvedor, quando alguma alteração
no texto original ocorre, ou por um tradutor que não tem certeza da
tradução atual. Este sinalizador além de indicar uma tradução errada ou
incerta, indica que o texto sinalizado não constará no arquivo MO gerado
a partir do catálogo PO.

Outros sinalizadores possíveis são os de formatação de linguagem de
programação, por exemplo, c-format. Estes sinalizadores indicam que o
texto deve respeitar a sintaxe da linguagem de programação indicada.
Atualmente temos sinalizadores para c, java, python, perl, gcc, lisp,
smalltalk, etc.

------------------------------------------------------------------------

## Onde pego os arquivos a serem traduzidos? {#Onde_pego_os_arquivos_PO}

Pelo [Vertimus](http://www.bani.com.br/vertimus/), ao clicar no nome do
aplicativo, você verá uma página com as estatísticas da tradução, um
link para o arquivo .PO e verá se o módulo foi reservado para tradução
por outro tradutor. O link para o arquivo .PO estará lado da frase
\"Arquivo PO atualizado do repositório:\".

------------------------------------------------------------------------

### Qual versão devo traduzir? {#Qual_versao_devo_traduzir}

Geralmente os programas possuem a versão estável, chamada de *branch*, e
a de desenvolvimento, que é chamada de *trunk* ou *HEAD*. A tradução
ocorrerá na maioria das vezes na versão que está em desenvolvimento.
Você só deve traduzir uma versão estável se estiver agendada uma revisão
da mesma. Se uma versão já tiver sido lançada, então não adianta
traduzir porque as traduções não vão aparecer nas telas de ninguém.

As traduções na versão de desenvolvimento, por outro lado, só devem
acontecer a partir do [change announcement
period](http://live.gnome.org/ReleasePlanning/Freezes), que é o período
em que as modificações são menos freqüentes. Não se preocupe em não
saber o período, ele é sempre divulgado na lista. De qualquer forma, é
interessante acompanhar o [ciclo de
desenvolvimento](http://live.gnome.org/ReleasePlanning) do GNOME.

Há alguns programas que não seguem o calendário do GNOME e portanto,
podem ser traduzidos a qualquer hora. É o caso de alguns citados na
seção **[Onde pego os arquivos a serem
traduzidos?](#Onde_pego_os_arquivos_a_serem_tr)**
cuja chamada para tradução será feita na lista.

------------------------------------------------------------------------

##Quais programas usar para traduzir? {#Quais_programas_usar_para_traduzir}

Você pode abrir/editar os catálogos de mensagens com qualquer editor de
textos, como o [Gedit](http://www.gnome.org/projects/gedit/), o
[Emacs](http://www.gnu.org/software/emacs/) ou o
[Vim](http://www.vim.org/), mas existem programas dedicados como
[Gtranslator](http://gtranslator.sourceforge.net/),
[poEdit](http://www.poedit.net/), [KBabel](http://kbabel.kde.org/) e
[Pootling](http://translate.sourceforge.net/wiki/) que apresentam uma
série de vantagens e são fortemente indicados.

------------------------------------------------------------------------

## Cuidados necessários durante a realização da tradução {#Cuidados_necess_rios_durante_a_r}

Durante a realização da tradução, deve ser considerada uma série de
pequenos cuidados, que irá garantir a integridade e qualidade da mesma:

-   Respeite todos os caracteres especiais utilizados no texto original,
    no texto traduzido, por exemplo, caractere que representa uma
    nova-linha, tabulação, recuo, dentre outros.
-   Tenha cuidado ao traduzir um termo já padronizado em seu idioma.
    Como ferramentas de consulta de termos, deve-se sempre consultar:
    -   A [busca](http://vp.godoy.homeip.net/busca/) do Vocabulário
        Padrão do [Projeto de Documentação Linux](http://br.tldp.org/),
        que permite a consulta de termos traduzidos padronizados.
    -   O site [Open-tran.eu](http://pt-br.open-tran.eu/), que permite a
        consulta de termos já empregados em módulos dos ambientes de
        trabalho *GNOME* e *KDE*.
    -   A página de [Erros Frequentes](ErrosFrequentes.html).
-   Freqüentemente, você encontrará mensagens marcadas com a expressão
    `fuzzy`. Conforme citado na seção **[O que devo observar ao editar
    arquivos
    .PO](#O_que_devo_observar_ao_editar_ar)**.
    Somente remova o sinalizador após ter certeza que a string está
    traduzida corretamente. (programas específicos, como o poEdit, o
    KBabel e o Gtranslator, fazem isso automaticamente.)
-   Procure seguir o [Guia de Estilo de Tradução da
    Interface](GuiaEstiloInterface.html) e o [Guia de Estilo de
    Tradução da Documentação](GuiaEstiloDocumentacao.html).

Se a ajuda não for suficiente, entre em contato no canal \#tradutores ou
então na lista. Veja no início da página os endereços de contato.

------------------------------------------------------------------------

## Modelo de Cabeçalho {#Modelo_de_cabecalho}

Todos os catálogos de mensagens possuem um cabeçalho. Os cabeçalhos
contém as informações do pacote. Note que além das informações de
identificação do pacote, há também as informações de *copyright* e dos
tradutores que já trabalharam no mesmo.

    # Brazilian Portuguese translation of Evolution.
    # This file is distributed under the same license as the Evolution package.
    # Copyright (C) 2000,2001,2002,2003,2004,2005,2006,2007 Free Software Foundation, Inc.
    # Fulano da Silva <fdsilva@hohoho.com.br>, 2000.
    # Cicrano Almeida <calmeida@bunny.net.br>, 2001,2002,2003,2004,2005,2006,2007.
    #
    msgid ""
    msgstr ""
    "Project-Id-Version: evolution 1.4.5\n"
    "POT-Creation-Date: 2007-10-09 16:20-0200\n"
    "PO-Revision-Date: 2007-10-09 18:18-0200\n"
    "Last-Translator: Cicrano Almeida <calmeida@bunny.net.br>\n"
    "Language-Team: Brazilian Portuguese <gnome-l10n-br@listas.cipsga.org.br>\n"
    "MIME-Version: 1.0\n"
    "Content-Type: text/plain; charset=UTF-8\n"
    "Content-Transfer-Encoding: 8bit\n"
    "Plural-Forms: nplurals=2; plural=(n > 1);\n"

Sempre que fizer alterações significativas em um catálogo, você deverá
acrescentar o seu nome e *e-mail*, conforme explicado na seção
**[Créditos de
tradução[?](/bin/edit/GNOMEBR/#Créditos_de_tradução?topicparent=GNOMEBR.TraducaoPrevia "Create this topic")]{.foswikiNewLink}**.

------------------------------------------------------------------------

### Créditos de tradução {#Creditos_de_traducao}

Você deve acrescentar seu nome e *e-mail* nos comentários do cabeçalho
do catálogo seguindo o mesmo esquema utilizado pelos demais tradutores.
Há na maioria dos arquivos PO, uma parte chamada *translator-credits*,
você deve também colocar o seu nome e *e-mail* quando encontrar essa
seção. Nunca retire nenhum nome da lista de tradutores, mesmo que não
tenha restado nada do trabalho realizado por eles.

------------------------------------------------------------------------

## Validando uma tradução {#Validando_uma_traducao}

Um procedimento muito importante no processo de tradução é a validação.
A validação serve como uma revisão do próprio tradutor e visa encontrar
possíveis problemas e erros, permitindo que o revisor/ *committer*
realize um trabalho mais rápido, tendo em vista que o número de
revisores/ *committers* é bem menor que o número de tradutores.

Procedimentos comuns de validação que não devem ser esquecidos:

-   Ativação da verificação ortográfica.
-   Ao concluir a tradução de uma mensagem (*\"string\"*), leia-a do
    início ao fim.
-   Preste atenção à pontuação, espaçamento, e principalmente ao
    fraseamento.
-   Ler e reler as mensagens com mais de 40 caracteres. Em geral, nos
    fixamos nas partes e esquecemos do conjunto.

Como procedimento de validação padrão, é necessário obter o *pofilter*,
que é parte do
[Translate-Toolkit](http://translate.sourceforge.net/wiki/) para
verificar o catálogo. Para verificar, execute os comandos:

    msgfmt -cvo /dev/null meucatálogo.po

    pofilter --gnome pt_BR.po | less

O programa irá realizar uma validação de consistência no arquivo,
avisando sobre maiúsculas incorretas, falta de ponto no final da
tradução, excesso de espaço, etc. Claro que nem tudo do que ele reclama
está errado, mas a chance de passar erros vai ser bem pequena.

Caso utilize os programas de tradução
[Pootle](http://pootle.wordforge.org/)/[Pootling](http://sourceforge.net/project/showfiles.php?group_id=91920&package_id=215928)
não é necessário realizar os comandos, pois eles já realizam a
verificação através do próprio *pofilter*.

------------------------------------------------------------------------

## Terminei e validei a tradução, e agora? {#Terminei_e_validei}
-----------------------------------------------------------------------------

Após a tradução e validação do arquivo, o próximo passo é enviá-lo para
revisão. Para isso, vá no [Vertimus](http://www.bani.com.br/vertimus) e
na mesma página do aplicativo que você fez o download do arquivo .PO e
reservou a tradução, vá no item ações, escolha \"Enviar a Nova
Tradução\" e, utilizando o botão \"browse\", escolha o arquivo .PO
atualizado para fazer o upload. No campo comentário escreva alguma
informação interessante que você gostaria de avisar ao revisor. Clique
em Aplicar. Após isso, será enviado um e-mail para a lista avisando do
upload do arquivo .PO.

------------------------------------------------------------------------

## Como abrir um relatório no Bugzilla? {#Como_abrir_um_relatorio}

Acesse o site do [Bugzilla](http://bugzilla.gnome.org/), crie uma conta
e um novo relatório clicando no *link* ***New Bug***. Na parte ***Bug
Type***, escolha a opção ***Translations***, na próxima opção
***Application selection***, selecione a categoria e o programa que foi
traduzido. Em ***Language selection***, selecione ***Brazilian
Portuguese \[pt\_BR\]***. Na próxima página selecione a versão do
***HEAD*** do GNOME. Na página ***Enter bug information***, é
interessante realizar a descrição com informações suficientes para a
identificação do programa traduzido. Um procedimento que ajuda na
identificação do pacote traduzido é inserir nos campos do relatório
informações como as explicitadas abaixo, lembrando que a comunicação no
Bugzilla deve ser realizada sempre em inglês.

Exemplos de mensagem para serem inseridas nos campos:

-   ***Please enter a short summary of the bug*** - *Brazilian
    Portuguese Translations of* NOME DO PROGRAMA

<!-- -->

-   ***Where and what is the erroneous translation?*** - *Updated
    translations of nomedopacote.HEAD.po.gz*

Para pedidos de correção, somente a indicação do local de erro já é o
suficiente. Após criado o relatório de erro, será necessário inserir o
pacote com o arquivo da tradução. Clique em ***Create a New
Attachment***, indique o pacote com a tradução. Marque a opção
***patch*** em ***Content type***.

------------------------------------------------------------------------

### Exemplo de um relatório {#Exemplo_de_relatorio}

Em caso de dúvidas sobre a criação de relatórios, veja os exemplos
[aqui](http://bugzilla.gnome.org/buglist.cgi?query=component%3A%22Brazilian+Portuguese+%5Bpt_BR%5D%22+product%3Al10n+).

------------------------------------------------------------------------

## O Processo de Revisão {#O_Processo_de_Revisao}

Ao enviar uma tradução pelo Bugzilla, ela ficará disponível para que um
dos membros do time de tradução com acesso ao [repositório do
GNOME](http://developer.gnome.org/tools/svn.html) possa revisá-la e
enviá-la. Você poderá acompanhar todo o processo de correção através dos
*e-mails* que são enviados pelo *Bugzilla* sempre que for realizada
alguma ação relacionada com o pacote enviado. Para se certificar do
recebimento de *e-mail*, confirme no *Bugzilla* as suas preferências de
*e-mail* na seção *Account \> Email Preferences*. As possíveis correções
da sua tradução serão comentadas na lista pelo revisor que as realizar.

------------------------------------------------------------------------

## Relacionados {#Relacionados}

-   [Erros Frequentes](ErrosFrequentes.html)
-   [Tradutores Trabalhando[?](/bin/edit/GNOMEBR/TradutoresTrabalhando?topicparent=GNOMEBR.TraducaoPrevia "Create this topic")]{.foswikiNewLink}
-   [Tradução dos Termos de Uso do Software](TraducaoDosTermosDeUsoDoSoftware.html)

------------------------------------------------------------------------

## Ver também {#Ver_tambem}

Sobre tradução:

-   Wikipédia: [Tradução](http://pt.wikipedia.org/wiki/Tradução),
    [i18n](http://pt.wikipedia.org/wiki/Internacionalização%20(software)).
-   Wikipédia em inglês:
    [Translation](http://en.wikipedia.org/wiki/Translation),
    [Computer-assisted
    translation](http://en.wikipedia.org/wiki/Computer-assisted_translation),
    [i18n and
    l10n](http://en.wikipedia.org/wiki/Internationalization_and_localization).

------------------------------------------------------------------------

### Outros projetos de tradução {#Outros_projetos_de_traducao}

-   [Projeto de Documentação Linux](http://br.tldp.org/).
-   [Equipe brasileira de tradução do
    GNU](http://translationproject.org/team/pt_BR.html)
-   [KDE](http://kde-i18n-ptbr.codigolivre.org.br/)
-   [XFCE](http://i18n.xfce.org/wiki/team_pt_br)
-   [Ubuntu](http://wiki.ubuntubrasil.org/TimeDeTraducaoPrevia)
-   [ONG BrOffice.org](http://www.broffice.org.br)
-   [Equipe brasileira de tradução da Mozilla](http://br.mozdev.org).

------------------------------------------------------------------------

### Projetos de tradução de distribuições: {#Projetos_de_traducao_de_distribuicoes}

-   [Equipe brasileira de tradução do
    Debian](http://debian-br.alioth.debian.org/).
-   [Equipe brasileira de tradução do
    Fedora](http://fedoraproject.org/wiki/L10N/Teams/BrazilianPortuguese).
-   [Equipe brasileira de tradução do
    Gentoo](http://www.gentoo.org/doc/pt_br/overview.xml).
-   [Equipe brasileira de tradução do
    Ubuntu](http://wiki.ubuntubrasil.org/TimeDeTraducaoPrevia).

------------------------------------------------------------------------

## Referências {#Referencias}

-   <http://wiki.ubuntubrasil.org/TimeDeTraducaoPrevia>\
-   <http://wiki.ubuntubrasil.org/TimeDeTraducaoPrevia/CatalogoDeMensagens>\
-   <http://l10n.gnome.org/languages/pt_BR>\
-   <http://i18n.xfce.org/wiki/team_pt_br>
-   [\#gnome-br-2008-04-23-20h23.log.gz](TraducaoPrevia/gnome-br-2008-04-23-20h23.log.gz):
    Ata da reunião de 23 de abril de 2008, sobre reordenação da dinâmica
    da equipe de tradução. Primeira reunião após Leonado Fontenelle
    assumir a coordenação.
