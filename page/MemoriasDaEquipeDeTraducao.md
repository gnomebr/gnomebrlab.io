%Memórias da equipe brasileira de tradução do GNOME

A tradução do GNOME começou como parte do
[LDP-BR](http://ldp-br.conectiva.com.br/) (ver [site
atual](https://web.archive.org/web/20090724114828/http://br.tldp.org/)),
que por sua vez estava atrelado à
[Conectiva](https://web.archive.org/web/20020327163722/http://www.conectiva.com.br:80/) 
(hoje fundida à Mandrakesoft para criar a
[Mandriva](https://web.archive.org/web/20100106134757/http://www2.mandriva.com.br:80/)).
O LDP-BR contava principalmente com o trabalho de voluntários, mas os
funcionários da Conectiva ocupavam posições chave. Um funcionário da
Conectiva (Ricardo Guimarães?), por exemplo, era a única pessoa da equipe
com uma conta no servidor de CVS do GNOME. Gustavo Maciel Dias Vieira foi
a primeira pessoa da equipe mas fora da Conectiva a conseguir uma conta no
CVS do GNOME, através de contato direto com o GNOME Translation Project,
em 2000 ou 2001. A partir daí passou a assumir cada vez mais a coordenação
da equipe. Foi ele quem elaborou a primeira página web da tradução do
GNOME, colocada no site do LDP-BR através da Lisiane, funcionária da
Conectiva que assumiu a coordenação do LDP-BR quando Ricardo saiu.

Com o fim do mestrado, Gustavo Vieira começou a trabalhar, e tinha pouco
tempo para a tradução do GNOME. Isso foi um grande problema, já que
todas as traduções tinham que passar por suas mãos para chegar ao CVS do
GNOME. Foi então que Gustavo \"kov\" Noronha Silva sugeriu (em 2002 ou
2003) que mais pessoas da equipe ganhassem contas no CVS do GNOME, o que
ficou conhecido como \"coordenação distribuída\". A partir daí começou a
desvinculação da equipe brasileira de tradução do GNOME. Também foi kov
quem criou a primeira lista de discussão própria da equipe, em 22 de
agosto de 2002, usando a infra-estrutura da ONG brasileira
[CIPSGA](http://www.cipsga.org.br) (Comitê de Incentivo a Produção do
Software GNU e Alternativo). Em 2003 a equipe adotou a infraestrutura
do SourceForge, mais especificamente para o wiki
[gnome-br.sourceforge.net](http://gnome-br.sourceforge.net/) e
para a organização do fluxo de trabalho. As traduções prontas eram
colocadas na ferramenta de acompanhamento de \"erros\" do SF, e a partir
de lá algum membro com acesso ao CVS pegava, revisava e enviava para o
GNOME. O GNOME 2.4 foi a primeira versão completamente traduzida para o
português do Brasil, feito mantido ao longo das versões 2.6 e 2.8.

Do final de 2004 até 2006 Gustavo Vieira praticamente não pôde
participar da tradução do GNOME. Nesse período Raphael Higino (in
memoriam) se destacou pelo desempenho, mas quando Gustavo Vieira
resolveu passar a coordenação adiante quem se prontificou para assumir
foi Gustavo Pastore. Sob sua coordenação, e com a ajuda de Lucas Rocha,
a equipe começou a aproveitar melhor a infrastrutura do GNOME. Foi
criado uma [nova página no GNOME
Live](http://wiki.gnome.org/GnomeBR/Traducao) (o wiki antigo já não
funcionava direito), e adotou-se o bugzilla do GNOME para o fluxo de
trabalho. No começo as traduções eram anexadas aos relatórios \"de
erro\" na forma de patches, o que criou um grande problema, porque os
tradutores não usavam como referência o CVS do GNOME, e sim os arquivos
expostos pelo precursor do l10n.gnome.org. Depois de algum tempo se
convencionou anexar traduções inteiras, mas marcá-las como patches para
colaborar na estatística de quem revisava mais patches. Em novembro de
2005 Guilherme Pastore criou o canal [\#gnome-br no
GimpNet](irc://irc.gnome.org/#gnome-br) para atender tanto à equipe de
tradução quanto aos usuários e desenvolvedores brasileiros; até então
usava-se um canal de mesmo nome no
[FreeNode[?](/bin/edit/GNOMEBR/FreeNode?topicparent=GNOMEBR.MemoriasDaEquipeDeTraducao "Create this topic")]{.foswikiNewLink}.

## Referências

-   Gustavo Maciel Dias Vieira, em e-mail particular a Leonardo Ferreira
    Fontenelle
-   [E-mail inaugural da primeira lista de discussão própria da
    equipe](http://listas.cipsga.org.br/pipermail/gnome-l10n-br/2002-August/000000.html).
-   [Anúncio de afastamento de Gustavo Maciel Dias
    Vieira](http://listas.cipsga.org.br/pipermail/gnome-l10n-br/2004-August/002542.html)
-   [Anúncio da transferência de coordenação oficial do projeto por
    Gustavo Maciel Dias
    Vieira](http://listas.cipsga.org.br/pipermail/gnome-l10n-br/2006-July/003594.html)
-   [Anúncio da criação do canal de IRC no
    GimpNet](http://listas.cipsga.org.br/pipermail/gnome-l10n-br/2005-November/003314.html)
    
    

