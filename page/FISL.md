::: {align="center"}
[]{#FISL_9_0} FISL 9.0
======================

[![banner\_FISL](http://fisl.softwarelivre.org/9.0/www/files/uploads/u69/fisl9_identidade_460x60_banner.png){width="460"
height="60"}](http://fisl.softwarelivre.org/9.0/www/)
:::

O [FISL](http://fisl.softwarelivre.org/9.0/www/), Fórum Internacional de
Software Livre, é o maior evento do segmento no Brasil e um dos maiores
do mundo. Ele acontece anualmente em Porto Alegre, RS e já está em sua
nona edição.

Este ano, mais uma vez o GNOME Brasil esteve presente, através de um
evento comunitário e com um estande de usuários.

------------------------------------------------------------------------

[]{#foswikiTOC}

::: {.foswikiToc}
-   [Programação](#Programa_o)
-   [Estande](#Estande)
-   [Slides dos
    Palestrantes](#Slides_dos_Palestrantes)
-   [Fotos do Evento](#Fotos_do_Evento)
:::

------------------------------------------------------------------------

[]{#Programa_o} Programação
---------------------------

Tivemos nosso encontro no dia **[18/04, sexta, as 9h da
manhã](http://fisl.softwarelivre.org/9.0/papers/pub/programacao/242)**,
na sala Tim Berners Lee (**Sala 41E**), com **2 horas** de duração.

[Dia 18/04 -
Sexta-Feira](http://fisl.softwarelivre.org/9.0/papers/pub/programacao/242)

09h-09:30h

Novidades no GNOME 2.22

Tiago Menezes

09:30h-10:30h

Envolvendo-se com o GNOME

Jonh Wendell

10:30h-11h

Atividades do GNOME Brasil em 2008

Todas/os

[]{#Estande} Estande
--------------------

Este ano temos disponível um estande, onde podemos nos encontrar, trocar
idéias, programar, traduzir, tirar dúvidas, etc. Há sempre alguém
disponível durante os três dias do evento.

[]{#Slides_dos_Palestrantes} Slides dos Palestrantes
----------------------------------------------------

Para ter acesso aos slides de algumas das palestras apresentadas no
[FISL](FISL.html) 9.0, acesse a nossa
página de [apresentações](Apresentacoes.html).

[]{#Fotos_do_Evento} Fotos do Evento
------------------------------------

-   Confira [aqui as fotos](FotosFisl9.html) do evento.

\
