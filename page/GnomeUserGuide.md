[]{#Tradu_o_do_Guia_do_Usu_rio_do_GN} Tradução do Guia do Usuário do GNOME para o português brasileiro (reloaded)
=================================================================================================================

O Guia do Usuário 2.18.1 foi lançado com 71% de tradução. Agora só
faltam três capítulos, e cada um deles já está sendo traduzido. A nova
meta é lançar a tradução completa do Guia do Usuário na versão 2.18.2.

Se você quiser ajudar a traduzir a documentação GNOME, pode [escolher
outro módulo](http://l10n.gnome.org/languages/pt_BR/gnome-2-18) para
traduzir.

1.  **Desktop User Guide** (57 traduzidas, 4 aproximadas, 5 não
    traduzidas) ---
    [JonhWendell[?](/bin/edit/GNOMEBR/JonhWendell?topicparent=GNOMEBR.GnomeUserGuide "Create this topic")]{.foswikiNewLink}
    --- Gnomebug:415355 --- **Feito**;
2.  **Basic Skills** (176 traduzidas, 17 aproximadas, 5 não traduzidas)
    --- [LeonardoFontenelle](LeonardoFontenelle.html) ---
    Gnomebug:415313 --- **Feito**;
3.  **Desktop Overview** (154 traduzidas, 14 aproximadas, 24 não
    traduzidas) --- [LeonardoFontenelle](LeonardoFontenelle.html)
    --- Gnomebug:415325 --- **Feito**;
4.  **Desktop Sessions** (76 traduzidas, 4 aproximadas) ---
    [JonhWendell[?](/bin/edit/GNOMEBR/JonhWendell?topicparent=GNOMEBR.GnomeUserGuide "Create this topic")]{.foswikiNewLink}
    --- Gnomebug:415339 --- **Feito**;
5.  **Using the Panels** (153 traduzidas, 69 aproximadas, 182 não
    traduzidas) --- [LeonardoFontenelle](LeonardoFontenelle.html)
    --- Gnomebug:415343;
6.  **Using the Main Menubar** (39 traduzidas, 10 aproximadas, 1 não
    traduzida) --- [LeonardoFontenelle](LeonardoFontenelle.html) ---
    Gnomebug:415346 --- **Feito**;
7.  **Working with Files** (839 traduzidas, 52 aproximadas, 111 não
    traduzidas) ---
    [ViniciusPinheiro[?](/bin/edit/GNOMEBR/ViniciusPinheiro?topicparent=GNOMEBR.GnomeUserGuide "Create this topic")]{.foswikiNewLink}
    --- Gnomebug:415349 --- **Feito**;
8.  **Tools and Utilities** (189 traduzidas, 25 aproximadas, 7 não
    traduzidas) ---
    [ViniciusPinheiro[?](/bin/edit/GNOMEBR/ViniciusPinheiro?topicparent=GNOMEBR.GnomeUserGuide "Create this topic")]{.foswikiNewLink} -
    Gnomebug:415350;
9.  **Configuring your Desktop** (203 traduzidas, 115 aproximadas, 377
    não traduzidas) ---
    [AndreNoel[?](/bin/edit/GNOMEBR/AndreNoel?topicparent=GNOMEBR.GnomeUserGuide "Create this topic")]{.foswikiNewLink}
    --- Gnomebug:415351;
10. **Glossary** (55 traduzidas);
11. **Feedback** (6 traduzidas, 2 aproximadas, 10 não traduzidas) ---
    [JonhWendell[?](/bin/edit/GNOMEBR/JonhWendell?topicparent=GNOMEBR.GnomeUserGuide "Create this topic")]{.foswikiNewLink}
    --- Gnomebug:415352 --- **Feito**.
