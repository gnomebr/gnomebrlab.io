[]{#A_Funda_o_GNOME} A Fundação GNOME
=====================================

A Fundação GNOME, ou [GNOME Foundation](http://foundation.gnome.org/), é
uma entidade sem fins lucrativos - sediada nos Estados Unidos - que foi
criada para ajudar o GNOME a atingir seu objetivo: criar uma plataforma
de computação que possa ser usada pelo público em geral e que seja
totalmente livre.

Ela se encarrega da parte burocrática do projeto, e nasceu justamente
para isso: Apoiar o GNOME juridicamente. Ela também é responsável por
controlar as finanças, gerenciando as entradas (doações, anuidades do
quadro de consultores, etc) e cuidando para que essa receita tome o rumo
certo (hackfests, reembolso de despesas de palestrantes, pagamento da
diretora executiva, GUADEC, etc)

Ela é composta de:

[]{#Quadro_de_diretores} Quadro de diretores
--------------------------------------------

Anualmente é eleito (pelos membros da fundação) um quadro de (7)
diretores, que cuidam das tarefas da fundação. O quadro se reúne
quinzenalmente em reuniões por conferência telefônica para discutir
tópicos pertinentes às atividades da fundação e do GNOME.

[]{#Diretora_executiva} Diretora executiva
------------------------------------------

Stormy Peters é a diretora executiva da fundação. Sua função é gerenciar
e fazer crescer a fundação GNOME como uma entidade. Ela trabalha junto
com o quadro de diretores, quadro de consultores e os membros da
fundação.

[]{#Quadro_de_consultores} Quadro de consultores
------------------------------------------------

O quadro de consultores (Advisory Board) é composto de organizações e
empresas que apoiam o GNOME. Este quadro não tem poder de decisão, mas
ajuda o quadro de diretores no direcionamento geral do projeto. Para
saber mais sobre este quadro, quem o compõe, quais as responsabilidades,
clique [aqui](http://foundation.gnome.org/about/advisoryboard/).

[]{#Membros_da_funda_o} Membros da fundação
-------------------------------------------

A membresia consiste em todos os colaboradores do GNOME. Membros podem
concorrer a diretor da fundação, podem votar para diretor e podem
sugerir um referendo (para mudar algo no estatuto da fundação ou mudar
algo profundo no projeto). Para se tornar um membro da fundação, basta
ter colaborado com o GNOME de forma não-trivial. Veja informações sobre
como se tornar um membro
[aqui](http://foundation.gnome.org/membership/).

Para saber mais sobre a fundação, vá para a página oficial (em inglês)
clicando [aqui](http://foundation.gnome.org/about/).
