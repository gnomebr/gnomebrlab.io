%Publicações Acadêmicas

![ArtigosAcadêmicod.png](/PublicacaoAcademica/ArtigosAcad%eamicod.png){width="133"
height="133"}

Esta página reúne *publicações acadêmicas* que tiveram como objeto de
análise o ecossistema computacional ou social que compõem o Projeto
GNOME.

## Instruções para publicação nesta página

-   Informações sobre a licença utilizada, os autores, a data e o tipo
    de publicação relacionados devem estar presentes para todos os
    itens.
-   O link para o arquivo deve ser colocado no título da publicação.

\

## Artigos e Papers

  [Título](/PublicacaoAcademicasortcol=0;table=1;up=0#sorted_table "Sort by this column")                                                                                           [Autor(es)](/PublicacaoAcademicasortcol=1;table=1;up=0#sorted_table "Sort by this column")   [Licença](/PublicacaoAcademicasortcol=2;table=1;up=0#sorted_table "Sort by this column")   [Tipo](/PublicacaoAcademicasortcol=3;table=1;up=0#sorted_table "Sort by this column")   [Ano de Publicação](/PublicacaoAcademicasortcol=4;table=1;up=0#sorted_table "Sort by this column")
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------ ---------------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------- --------------------------------------------------------------------------------------------------------------------------------------
  [Software Engineering Practices in the GNOME Projetc](http://mitpress.mit.edu/books/chapters/0262562278.pdf)                                                                                                        GERMAN, Daniel M.                                                                                                              \-                                                                                                                           Capítulo 11 de Livro                                                                                                      2005
  [\"Software Livre e o Modelo Colaborativo de Produção entre Pares: uma Análise Organizacional sobre o Projeto GNOME\"](https://twiki.softwarelivre.org/pub/Blogs/BlogPostVicenteAguiar20080911000357/ADIC857.pdf)   AGUIAR, Vicente M.                                                                                                             [CC-BY-SA](http://creativecommons.org/licenses/by-sa/2.5/br/)                                                                Artigo                                                                                                                    2008

\

## Monografias, Dissertações e Teses

  [Título](/PublicacaoAcademicasortcol=0;table=2;up=0#sorted_table "Sort by this column")                                                                                                                [Autor(es)](/PublicacaoAcademicasortcol=1;table=2;up=0#sorted_table "Sort by this column")   [Licença](/PublicacaoAcademicasortcol=2;table=2;up=0#sorted_table "Sort by this column")   [Tipo](/PublicacaoAcademicasortcol=3;table=2;up=0#sorted_table "Sort by this column")   [Ano de Publicação](/PublicacaoAcademicasortcol=4;table=2;up=0#sorted_table "Sort by this column")
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------ ---------------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------- --------------------------------------------------------------------------------------------------------------------------------------
  [\"Os Argonautas da internet: uma análise netnográfica sobre a comunidade on-line de software livre do Projeto GNOME a luz da teoria da Dádiva.\"](https://www.colivre.coop.br/pub/Main/VicenteAguiar/DissertacaoGnomeVersaoFinal.pdf)   AGUIAR, Vicente M.                                                                                                             [CC-BY-SA](http://creativecommons.org/licenses/by-sa/2.5/br/)                                                                Dissertação de Mestrado                                                                                                   2007
