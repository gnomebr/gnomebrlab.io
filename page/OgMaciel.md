[]{#Og_Maciel} Og Maciel
------------------------

### []{#Who_am_I} Who am I?

My name is Og Maciel, born in Brazil but living in the United States
since 1991. Graduated from Pace University with a BS in Biochemistry in
1997. Married since 2000 and father of two wonderful girls!

Been involved with Open Source, and more specifically with the Brazilian
community since 2005, thanks to incentive from Stephan Hermann. Started
as a translator for the Brazilian Team, eventually being appointed as
the administrator, and proud leader of the team until 2007.

Currently I work exclusively with upstream projects, committing
Brazilian Portuguese translations to GNOME and XFCE, as well as helping
out with Openbox and KDE translations. I also maintain a project called
[Bill Reminder](http://billreminder.gnulinuxbrasil.org), where I get to
flex my programming muscles whenever I have some free time.

### []{#Web_Presence} Web Presence:

### []{#Blogs} Blogs:

-   <http://www.ogmaciel.com> (en\_US)
-   <http://blog.ogmaciel.com> (pt\_BR)

### []{#Involvement} Involvement:

\* Leader of Ubuntu Brazilian Portuguese Translators Team (2006-2007) \*
Contributor to the Gnome Brazilian Translators Team \* Founding Member
of Ubuntu Brazilian Planet Team \* Spearheaded the creation of the
Brazilian Data Miners Team \* Spearheaded the creation of the Brazilian
Documentation Team \* Represented Ubuntu at the Linux World Expo in
Boston (2006). \* Represented Ubuntu at the Princeton Computer Show
(2006). \* Member of Norther New Jersey MySQL User Group (2004-2006) \*
Founding member of the Ubuntu United States - NJ Team \* Organizer of
Nothern New Jersey Linux User Group (2004 - 2005)

### []{#Details} Details:

-   \'\'\'Wiki:\'\'\'
    [OgMaciel](OgMaciel.html)
-   \'\'\'IRC:\'\'\'
    [OgMaciel](OgMaciel.html) on network
    chat.freenode.org, irc.gimp.org
-   \'\'\'IRC:\'\'\'
    [GnuKemist[?](/bin/edit/GNOMEBR/GnuKemist?topicparent=GNOMEBR.OgMaciel "Create this topic")]{.foswikiNewLink}
    on network chat.freenode.org
-   \'\'\'Jabber:\'\'\' <og.maciel@gmail.com>
-   \'\'\'OpenPGP keys:\'\'\'
    [D5CFC202[?](/bin/edit/GNOMEBR/D5CFC202?topicparent=GNOMEBR.OgMaciel "Create this topic")]{.foswikiNewLink}
