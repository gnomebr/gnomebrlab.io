[]{#Estande_de_Eventos} Estande de Eventos
==========================================

No Brasil existem inúmeros eventos de pequeno, médio e grande porte
relacionados direta ou indiretamente a tecnologias livres. É importante
que nós, do GNOME Brasil, tenhamos presença tanto nas grandes
conferências internacionais de software livre que ocorrem no país quanto
nos pequenos eventos dentro de universidades e grupos de usuários
espalhados pelo Brasil.

Se você sabe de alguma evento de software livre que irá acontecer em sua
cidade ou estado e deseja ajudar a divulgar o GNOME, envie um anúncio
para a nossa [lista de discussão geral](Comunidade.html) e podemos
ajudar a organizar algum stand e/ou outras atividades de divulgação.

Para sermos realmente eficientes em relação a presença em eventos, é
fundamental que tenhamos [materiais de
divulgação](MaterialDivulgacao.html) prontos para serem utilizados e
reproduzidos em diferentes eventos.
