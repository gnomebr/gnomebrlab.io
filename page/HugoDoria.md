[]{#Hugo_D_ria} Hugo Dória
==========================

[]{#Quem_sou_eu} Quem sou eu?
-----------------------------

Meu nome é Hugo Dória e sou natural de Aracaju-SE, Brasil. Uso Linux
desde 2004 e já participei e colaborei com diversos projetos livres.

**Blog e sites:**

Pessoal: <http://hdoria.archlinux-br.org>

Projeto: <http://www.archlinux-br.org>

**Contato:**

Email: <hugodoria@gmail.com>

Jabber: <hugodoria@gmail.com>

IRC: **hdoria** na freenode.net
