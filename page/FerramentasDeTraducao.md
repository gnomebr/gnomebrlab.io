%Ferramentas de tradução

Essa é uma página puramente informativa, listando recursos disponíveis
para os [tradutores brasileiros do GNOME](Traducao.html). Se você
tiver alguma sugestão, acrescente-a abaixo!

## Aplicativos de tradução {#Aplicativos_de_traducao} 

Eles não traduzem para você, mas fornecem uma interface moldada para
facilitar seu processo de trabalho.

-   **[Gtranslator](http://gtranslator.sourceforge.net/)** --- **não** é
    uma boa opção; espere o lançamento da versão 2.0.
-   **[KBabel](http://kbabel.kde.org/)** --- ferramenta padrão do KDE 3,
    mas pode ser usado para traduzir catálogos do GNOME.
-   **[Lokalize](http://userbase.kde.org/Lokalize)** --- ferramenta
    padrão do KDE 4; se você o experimentar, conte como foi!
-   **[poEdit](http://www.poedit.net/)** --- simples, prático e
    multiplataforma; informe o idioma e o país do catálogo de mensagens
    para habilitar a verificação ortográfica.
-   **[Virtaal](http://translate.sourceforge.net/wiki/virtaal/index)**
    --- baseado no translate-toolkit e no GTK+.
-   **[Wordforge](http://www.khmeros.info/drupal612/Translation_Editor)**
    --- baseado no Qt 4, e no translator-toolkit; não depende do KDE.

## Editores de texto {#Editores_de_texto}

Como os catálogos de mensagem são essencialmente arquivos de texto,
qualquer editor de texto poderia ser usado para editá-los. Na prática,
você vai querer um editor de texto com recursos específicos para os
arquivos PO, como destaque de sintaxe e comandos para navegação (por
exemplo, \"ir para a próxima mensagem não traduzida\"). Se você usa
Windows ou Mac OS X, certifique-se de que o editor de texto use UTF-8
sem [BOM](http://en.wikipedia.org/wiki/Byte_Order_Mark), e que as
[quebras de linha](http://pt.wikipedia.org/wiki/Nova_linha) sejam LF,
não CR+LF ou CR.

-   **Gedit** --- já vem de fábrica com destaque de sintaxe para
    catálogos de mensagem; instale o
    [gedit-pomode](http://sourceforge.net/projects/gedit-pomode/) para
    navegação e outros recursos, e talvez também o plug-in para
    Open-Tran (confira a [página de plug-ins do
    Gedit](http://live.gnome.org/Gedit/Plugins)).
-   **Emacs** --- já vem de fábrica com o
    [po-mode](http://www.gnu.org/software/gettext/manual/gettext.html#PO-Mode).
-   **Vim** --- instale os plug-ins de [destaque de
    sintaxe](http://www.vim.org/scripts/script.php?script_id=913) e de
    [navegação](http://www.vim.org/scripts/script.php?script_id=695), e
    habilite a verificação ortográfica. Aviso: o plug-in de navegação
    \"pula\" as traduções com plural.

## Scripts {#Scripts}

Confira também os [scripts do GNOME Translation
Project](http://live.gnome.org/TranslationProject/Scripts).

-   **[muda-aspas](/FerramentasDeTraducao/muda-aspas)**
    --- converte \'aspas simples\' ou "aspas duplas curvas" em \"aspas
    duplas retas\".
-   **[muda-iniciais](FerramentasDeTraducao/muda-iniciais)**
    --- converte *Header Case* para *Sentence case*, ou seja, corrige a
    iniciais maiúsculas para seguir as normas da ABNT; baixe também o
    [dicionário de nomes
    próprios](FerramentasDeTraducao/pt_BR-palavras-em-maiusculo.dic)
    para que esses termos não sejam afetados.
-   **review** --- obsoleto pela migração para o Git.
