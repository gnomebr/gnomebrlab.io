%5° Fórum GNOME

![Banner V Forum](VForum/bannerVForumMed.png)

O Fórum GNOME é o evento anual da comunidade brasileira de usuários,
tradutores e desenvolvedores do GNOME. Na sua quinta edição, o 5° Fórum
GNOME aconteceu junto à [Latinoware](http://www.latinoware.org), um dos
maiores eventos de software livre da América Latina, de 30 de outubro à
1 de novembro de 2008.

Além dos membros brasileiros, os debates em Foz do Iguaçu contaram com a
participação de outros colaboradores Latino-Americanos, como Dieguito,
German e Pedro.

------------------------------------------------------------------------

## [Fotos](VForumFotos.html) {#fotos}

-   [Confira as fotos do evento!](VForumFotos.html)


## Grade de Programação {#grade_de_programacao}

**Dia 01/11 - Sábado**

-------       -----------------------------------------------------------   ------------------------------
10h-11h       How to help with the development                              Diego Escalante Urrelo
              of GNOME: testing applications
11h-12h       Developing Powerful PyGTK with                                Sandino Flores
              no hassle
12h-13h       Vamos traduzir o GNOME?                                       Fábio Nogueira
13h-14h       GNOME, um \"bazar\" organizado?                               Vicente Aguiar
14h-15h       Recriando o gksu com PolicyKit                                Gustavo Noronha da Silva (Kov)
15h-16h       Massacrando os insetos:                                       Bruno Boaventura
              Triagem de bugs no GNOME
16h-17h       [Desenvolvimento do extensões para                            Germán Póo-Caamaño
              Nautilus usando Python]
              (/VForumDesenvolvimentoDoExtensoeParaNautilusUsandoPython.html)
16h-20h       Curso - Plugins para o GIMP (em python)                       João Bueno
17h-18h       Computador para todos: Acessibilidade no GNOME                Jonh Wendell
19h-20h       GNOME: Interface, Tango e o Usuário                           Vinicius Scopel Depizzol
-------       -----------------------------------------------------------   ------------------------------


------------------------------------------------------------------------

## Slides dos Palestrantes {#slides_dos_palestrantes}

-   Para ter acesso aos slides de algumas das palestras apresentadas no
    IV Fórum GNOME, acesse a nossa página de
    [apresentações](Apresentacoes.html).

------------------------------------------------------------------------

## Comentários {#comentarios}

-   [de
    John\...](http://www.bani.com.br/lang/pt-br/2008/11/04/latinoware-and-gnome-forumlatinoware-e-forum-gnome/)

<!-- -->

-   [de Fábio\...](http://ubuntuser.com.br/blog/?p=158)

<!-- -->

-   [de
    Vicente\...](https://twiki.softwarelivre.org/bin/view/Blogs/BlogPostVicenteAguiar20081106035825)
