[]{#Escolha_a_UF} Escolha a UF
==============================

[]{#Instru_es} Instruções
-------------------------

Inclua este tópico para ter uma caixa *dropdown* com uma lista de
estados a escolher.

Exemplo:

    <form ...>
    ...
    %INCLUDE{FormularioUF}%
    ...
    </form>

***Atenção:*** o nome do campo gerado é `UF`.

[]{#C_digo} Código
------------------

Escolha\...AcreAlagoasAmapáAmazonasBahiaCearáDistrito FederalEspírito
SantoGoiásMaranhãoMato GrossoMato Grosso do SulMinas
GeraisParáParaíbaParanáPernambucoPiauíRio de JaneiroRio Grande do
NorteRio Grande do SulRondôniaRoraimaSanta CatarinaSergipeSão
PauloTocantins

\-- [LeandroSantos](/Main/LeandroSantos) - 12 May 2007
