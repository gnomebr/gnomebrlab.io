::: {align="center"}
[]{#IV_Brazil_GNOME_Forum} IV Brazil GNOME Forum
================================================

![bannerFixo\_468x60.png](/ForumEn/bannerFixo_468x60.png){width="468"
height="60"}
:::

IV Fórum GNOME is the yearly event of the brazilian GNOME users and
developers community. In its 4th edition, the Fórum will take place as a
community event of the [2nd Northeastern Free Software
Conference](http://www.ensl.org.br/site/), the biggest free software
event in northeastern Brazil, at
[Aracaju/SE](http://en.wikipedia.org/wiki/Aracaju), from September 28th
to 30th, 2007.

------------------------------------------------------------------------

[]{#Schedule} Schedule
----------------------

28/09

09h-10h

[Ensinando GNOME](http://grade.ensl.org.br/pub/programacao/113)

Aparecido Quesada

09h-12h

[Editando Fotografias Digitais com o
GIMP](http://grade.ensl.org.br/pub/programacao/92)

João S. de O. Bueno Calligaris

10h-11h

[Encontro de Usuários, Desenvolvedores e Contribuidores
GNOME.](http://grade.ensl.org.br/pub/programacao/118)

Comunidade

11h-12h

[Um trabalho a troco de nada?\" A resposta da comunidade GNOME para Jô
Soares e Bill Gates à luz da teoria da
Dádiva](http://grade.ensl.org.br/pub/programacao/91)

[VicenteAguiar[?](/bin/edit/Main/VicenteAguiar?topicparent=GNOMEBR.ForumEn "Create this topic")]{.foswikiNewLink}

14h-15h

[TWiki do GNOME Brasil](http://grade.ensl.org.br/pub/programacao/117)

Comunidade

16h-17h

[The Nokia N800, a device powered by
Linux](http://grade.ensl.org.br/pub/programacao/108)

Kenneth Rohde Christiansen

17h-18h

[Campanha de mapeamento sobre as organizações que usam GNOME no
Brasil](http://grade.ensl.org.br/pub/programacao/119)

Comunidade

29/09

09h-10h

[Gnome 2.20 - O novo Gnome turbinando o seu
desktop](http://grade.ensl.org.br/pub/programacao/90)

Tiago Cardoso Menezes

10h-11h

[Como contribuir com o
GNOME](http://grade.ensl.org.br/pub/programacao/84)

Jonh Wendell

16h-17h

[SUSE Linux do Data Center ao
Desktop](http://grade.ensl.org.br/pub/programacao/112)

Carlos Ribeiro

30/09

10h-11h

[Migração de aplicações .NET para plataformas livres com
Mono](http://grade.ensl.org.br/pub/programacao/93)

Everaldo Canuto

11h-12h

[GNOME, um \"bazar\"
organizado?](http://grade.ensl.org.br/pub/programacao/89)

[VicenteAguiar[?](/bin/edit/Main/VicenteAguiar?topicparent=GNOMEBR.ForumEn "Create this topic")]{.foswikiNewLink}

14h-15h

[Developing and Instant Messenger using
python](http://grade.ensl.org.br/pub/programacao/109)

Kenneth Rohde Christiansen
