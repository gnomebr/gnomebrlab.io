::: {align="center"}
[![Logo
FUDCon](https://fedoraproject.org/w/uploads/3/30/FUDCon_Santiago_2010_LOGO.png)](https://fedoraproject.org/wiki/FUDCon:Santiago_2010)\
:::

A [FUDCon Latam
2010](https://fedoraproject.org/wiki/FUDCon:Santiago_2010), (Fedora
Users and Developers Conference) é uma conferência voltada para usuários
e desenvolvedores da distribuição Fedora Linux. Ela acontece anualmente
em várias partes do mundo, com edições na América do Norte, Europa e
Ásia, tendo como objetivo difundir a distribuição Fedora e compartilhar
conhecimento com a comunidade de colaboradores ao redor do mundo.

------------------------------------------------------------------------

[]{#foswikiTOC}

::: {.foswikiToc}
-   [Apresentações](#Apresenta_es)
-   [Slides dos
    Palestrantes](#Slides_dos_Palestrantes)
-   [Fotos do Evento](#Fotos_do_Evento)
:::

------------------------------------------------------------------------

[]{#Apresenta_es} Apresentações
-------------------------------

  --------------- ------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------
  16/07 - 14:30    GNOME 3.0 - GNOMEShell e as novidades da versão   [RodrigoPadula[?](/bin/edit/GNOMEBR/RodrigoPadula?topicparent=GNOMEBR.FUDCONLATAM2010 "Create this topic")]{.foswikiNewLink}
  --------------- ------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------

[]{#Slides_dos_Palestrantes} Slides dos Palestrantes
----------------------------------------------------

Logo após o evento você poderá acessar os slides dos palestrantes na
nossa página de [apresentações](Apresentacoes.html).

[]{#Fotos_do_Evento} Fotos do Evento
------------------------------------

Aguardem!

\
