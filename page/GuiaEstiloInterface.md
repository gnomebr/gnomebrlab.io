[]{#Guia_de_Estilo_de_Tradu_o_da_Int} Guia de Estilo de Tradução da Interface do GNOME para o Português do Brasil
=================================================================================================================

Esta é uma página da [equipe brasileira de tradução do
GNOME](Traducao.html). Esse documento estende as [Diretrizes de
Interface Humana (HIG) do
GNOME](http://library.gnome.org/devel/hig-book/stable/) onde necessário,
e ressalta as particularidades do idioma português. Existe ainda o [Guia
de Estilo da
Conectiva](http://br.tldp.org/ferramentas/guia_estilo/guia_estilo.html),
que (como o nome indica) está infelizmente sem manutenção há muitos
anos; além disso, seu foco era documentação, não interface. Confira
também os [erros freqüentes de tradução](ErrosFrequentes.html).

------------------------------------------------------------------------

[]{#Sum_rio} Sumário
--------------------

[]{#foswikiTOC}

::: {.foswikiToc}
-   [Iniciais maiúsculas](#Iniciais_mai_sculas)
-   [Tempo e modo verbal](#Tempo_e_modo_verbal)
    -   [Infinitivo](#Infinitivo)
    -   [Presente](#Presente)
    -   [Imperativo](#Imperativo)
-   [Chaves de
    configuração](#Chaves_de_configura_o)
-   [Miscelânia](#Miscel_nia)
:::

------------------------------------------------------------------------

\

[]{#Iniciais_mai_sculas} Iniciais maiúsculas
--------------------------------------------

Seguimos as [diretrizes de maiusculização do idioma
inglês](http://library.gnome.org/devel/hig-book/stable/design-text-labels.html.en#layout-capitalization).

[]{#Tempo_e_modo_verbal} Tempo e modo verbal
--------------------------------------------

Como podemos notar depois de fazer tantas traduções, o inglês não possui
a quantidade de declinações, nem de flexões de modo e tempo verbal
presentes nas línguas românicas, das quais o português faz parte. E isso
tende a dificultar um pouco o trabalho de tradução. Por exemplo, ***Open
a File*** poderia ser ao mesmo tempo um comando (***Abrir um
Arquivo***), uma descrição do que um comando faz (***Abre um Arquivo***)
ou ainda um comando ao usuário para seguir em frente (***Abra um
arquivo***). (Nota: ***Abre um Arquivo*** seria ***Opens a File***, mas
na prática os autores originais não se dão ao trabalho de diferenciar, e
fica tudo ***Open a File***.)

Então, para melhor traduzir um texto em especial do inglês para o
português, devemos compreender sua função exata dentro da aplicação ou
do ambiente a que faz parte. Para distinguir todas essas funções
diferentes, a equipe de tradução criou uma lista de tempos e modos
verbais para cada uma dessas funções, que são aplicáveis a elementos em
comum a todas as aplicações e/ou ao ambiente como um todo.

No entanto, está criado um problema. É preciso, antes de mais nada,
descobrir a função de cada frase do texto original. E os arquivos que
armazenam a tradução (aqueles que possuem a extensão `.PO`) não trazem
dentro deles nenhuma informação semântica que nos permita saber com
exatidão o propósito de cada porção de texto. A melhor forma de
descobrir isso seria executando a própria aplicação e vasculhando cada
janela e controle da interface gráfica ‒ sem esquecer o texto de saída
da linha de comando ‒ em busca da correspondência entre cada *string* de
texto e seu propósito (e é isso que geralmente é feito!). Mas é uma
tarefa muito trabalhosa e, mesmo assim, existem casos em que não é
possível encontrar todo e qualquer o texto presente no arquivo `.PO`.

Então, o que se faz na prática para acelerar o processo é usar algumas
dicas deixadas no próprio arquivo `.PO` para nos ajudar a decifrar a
função de cada pedaço de texto.

A seguir são apresentados os modos e tempos verbais e seus usos, bem
como as dicas de como detectar cada caso. As dicas consistem geralmente
em observar as linhas de comentário que começam com `#:`, que são
mensagens automáticas geradas pelo `gettext` e que dizem de qual arquivo
a tradução foi retirada. Os modos e tempos verbais são:

### []{#Infinitivo} Infinitivo

O infinitivo é usado nos menus (nome de menu, de submenu e de item de
menu); em botões de comando e outros controles (*widgets*); e em
descrições curtas de chaves de configuração (ver abaixo).

Nesses casos, as mensagens costumam ser muito curtas; não têm ponto
final, a não ser quando terminam em reticências. No caso de menus e
botões, a mensagem estará em iniciais maiúsculas. Mensagens com teclas
de acesso (como nos dois primeiros exemplos abaixo) praticamente sempre
são traduzidas no infinitivo: itens de menu; botões; rótulos de caixas
de verificação (*checkboxes*, como nos diálogos de preferências) etc..
Em grande parte dos casos, será possível observar que, próximo a essas
mensagens curtas, existem mensagens longas correspondentes, que deverão
ser traduzidas no presente (ver abaixo). As mensagens podem vir de
arquivos `.C`, `.H`, `.PY` etc., de acordo com a linguagem, mas também
de arquivos `.GLADE`, `.SCHEMAS.IN` e outros. Nesses últimos casos, o
comentário automático exibirá o nome do arquivo com um \".H\" ao final,
mas isso não importa.

Esse é um exemplo de item de menu:

    #: ../gedit/gedit-ui.h:60
    msgid "_Open..."
    msgstr "_Abrir..."

Esse é um exemplo de botão de comando:

    #: ../extensions/error-viewer/ephy-error-viewer-extension.c:104
    msgid "Check _Links"
    msgstr "Verificar _Links"

Esse é um exemplo de descrição curta de chave de configuração:

    #: ../extensions/smart-bookmarks/smart-bookmarks.schemas.in.h:1
    msgid "Open search result in new tab"
    msgstr "Abrir os resultados da pesquisa em uma nova aba"

### []{#Presente} Presente

Sempre que a mensagem descrever algo, o verbo principal deverá ser
conjugado no presente. Isso se aplica, por exemplo, a dicas de
ferramentas (*tooltips*); a mensagens de barra de status; a descrições
longas de chaves de configuração (ver abaixo); e à descrições de
miniaplicativos, plug-ins e similares (essas descrições são exibidas em
diálogos de habilitação e remoção desses suplementos).

As mensagens traduzidas no presente são mais longas que as traduzidas no
infinitivo, e aquelas servem justamente para explicar o funcionamento
destas. Muitas vezes um olhar panorâmico no catálogo de configuração vai
permitir que o tradutor perceba um grupo de mensagens no infinitivo,
seguidas de um grupo de mensagens no presente; ou então de mensagens no
infinitivo e no presente, intercaladas. As mensagens a serem traduzidas
no presente costumam terminar em ponto final, embora isso nem sempre
aconteça.

Cada um dos exemplos abaixo corresponde a um dos exemplos acima. O
primeiro exemplo é a mensagem exibida na barra de status quando o item
de menu é selecionado. No caso, não há ponto final, embora isso não seja
uma regra.

    #: ../gedit/gedit-ui.h:61 ../gedit/gedit-window.c:1463
    msgid "Open a file"
    msgstr "Abre um arquivo"

Essa é a descrição daquele botão de comando:

    #: ../extensions/error-viewer/ephy-error-viewer-extension.c:106
    msgid "Display invalid hyperlinks in the Error Viewer dialog"
    msgstr "Exibe hiperlinks inválidos no diálogo Visualizador de Erros"

Essa é uma descrição longa de chave de configuração:

    #: ../extensions/smart-bookmarks/smart-bookmarks.schemas.in.h:2
    msgid "Open search result in new tab if true, in new window if false"
    msgstr ""
    "Abre os resultados da pesquisa em uma nova aba se verdadeiro, ou em uma "
    "nova janela se falso."

Esse exemplo não tem correspondência na seção anterior; é a descrição de
uma extensão do Epiphany:

    #: ../extensions/actions/ephy-actions-extension.c:113
    msgid "Customize actions"
    msgstr "Personaliza ações"

### []{#Imperativo} Imperativo

O imperativo só é usado para a tradução da descrição dos programas. Essa
descrição está em inglês em arquivos `.DESKTOP.IN`, que junto ao
catálogo de mensagens será usado durante a compilação para gerar os
arquivos `.DESKTOP`. A descrição é então exibida quando o ponteiro do
mouse repousa sobre um ícone da área de trabalho; sobre um lançador no
painel do GNOME; ou em um item dos menus Aplicações, Locais e Sistema.
Exemplo:

    #: ../data/paperbox.desktop.in.in.h:1
    msgid "Browse and tag your documents"
    msgstr "Navegue e marque seus documentos"

------------------------------------------------------------------------

[]{#Chaves_de_configura_o} Chaves de configuração
-------------------------------------------------

Alguns catálogos de mensagens contém mensagens cujo arquivo de origem é
algo como `NOME.schemas.in.h`. Na verdade, os arquivos são
`NOME.schemas.in`, e na hora de compilar o programa eles darão origem
(junto aos catálogos de mensagem) aos arquivos `NOME.schemas`, que
armazenam a estrutura das chaves de configuração do programa. Cada chave
de configuração tem uma **descrição curta** e uma **descrição longa**.
Quase sempre a descrição longa tem um ponto final, e a curta, não. Se
for necessário, o tradutor tirar a dúvida espiando o código fonte (é só
pedir orientação na lista de discussão). Para um exemplo, veja essa
[imagem](http://www.soulsphere.org/img/gconf-editor.png) ou abra o
Editor de Configurações (comando `gconf-editor`).

Além de seguir as [diretrizes do GNOME para chaves de
configuração](http://library.gnome.org/devel/hig-book/stable/gconf-keys.html),
os tradutores devem:

-   Sempre terminar as descrições curtas sem ponto final, e as longas
    com ponto final, mesmo que no original não esteja assim.
-   Preferencialmente manter o verbo principal no infinitivo, nas
    descrições curtas; e conjugar no presente, nas descrições longas
    (ver acima).
-   Preferencialmente traduzir *Whether to do (or not) \...* como *Fazer
    \...* nas descrições curtas, e como *Faz ou não \...* na longas.

------------------------------------------------------------------------

[]{#Miscel_nia} Miscelânia
--------------------------

As aspas devem ser \"duplas, retas\". Em inglês é comum usar \'aspas
simples\', às vezes usando \`crase para abrir aspas\'. Poderíamos usar
"aspas duplas curvas" (AltGr+v e AltGr+b), mas, pela dificuldade de
digitação, preferimos as retas. Devido à [estrutura dos catálogos de
mensagem](http://wiki.ubuntubrasil.org/TimeDeTraducao/CatalogoDeMensagens),
é importante que as aspas sejam \\\"precedidas de barra invertida\\\".
Algumas ferramentas de tradução já cuidam disso automaticamente; na
dúvida, é só olhar como está nas traduções antigas. O seguinte comando
corrige as aspas (observe que o seu terminal deve estar configurado para
aceitar caracteres *unicode*):


    msgfilter --keep-header sed "s/[\`'“”]\([^\`'“”]*\)['”]/\\\"\1\\\"/g" \
    < catálogo-antigo.po > catálogo-novo.po

::: {style="margin-left:-9999px;"}
:::
