::: {align="center"}
[]{#FISL_10} FISL 10
====================

[![banner\_FISL](http://fisl.softwarelivre.org/drupal5/files/banners/fullbanner2.gif){width="468"
height="60"}](http://fisl.softwarelivre.org/10/www/)
:::

O [FISL](http://fisl.softwarelivre.org/10/www/), Fórum Internacional de
Software Livre, é o maior evento do segmento no Brasil e um dos maiores
do mundo. Ele acontece anualmente em Porto Alegre, RS e esta é sua
décima edição.

Este ano, mais uma vez o GNOME Brasil estará presente, através de um
evento comunitário e com um estande de usuários.

------------------------------------------------------------------------

[]{#foswikiTOC}

::: {.foswikiToc}
-   [Quem vai](#Quem_vai)
-   [Programação](#Programa_o)
-   [Estande](#Estande)
-   [Slides dos
    Palestrantes](#Slides_dos_Palestrantes)
-   [Fotos do Evento](#Fotos_do_Evento)
:::

------------------------------------------------------------------------

[]{#Quem_vai} Quem vai
----------------------

      [Quem](/FISL10sortcol=0;table=1;up=0#sorted_table "Sort by this column")     [Quando?](/FISL10sortcol=1;table=1;up=0#sorted_table "Sort by this column")          [Onde](/FISL10sortcol=2;table=1;up=0#sorted_table "Sort by this column")
  ------------------------------------------------------------------------------------------------------------------ ---------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------
                                     [CarlosJosePereira](/Main/CarlosJosePereira)                                    24 jun                                                                                                                 Coral Tower Express
                                        [DjavanFagundes](/Main/DjavanFagundes)                                       24 jun, 10:15, [WebJet[?](/bin/edit/GNOMEBR/WebJet?topicparent=GNOMEBR.FISL10 "Create this topic")]{.foswikiNewLink}   Real Palace Hotel
                                         [FabioNogueira](/Main/FabioNogueira)                                        24 jun, 10:15, TAM                                                                                                     Master Express
                                             [HugoDoria](/Main/HugoDoria)                                            20 jun, 08:45, Azul                                                                                                    Não sei
                                           [JonhWendell](/Main/JonhWendell)                                          24 jun, 08:45, Azul                                                                                                    Master Express
                                          [JorgePereira](/Main/JorgePereira)                                         23 jun, 09:40, TAM                                                                                                     Não sei
                                        [KrixApolinario](/Main/KrixApolinario)                                       23 jun, 09:40, TAM                                                                                                     Não sei
                                          [LicioFonseca](/Main/LicioFonseca)                                         24 jun, 10:15, [WebJet[?](/bin/edit/GNOMEBR/WebJet?topicparent=GNOMEBR.FISL10 "Create this topic")]{.foswikiNewLink}   Master Express
                                         [MichelRecondo](/Main/MichelRecondo)                                        Nativo                                                                                                                 N/A
                                         [RodrigoFlores](/Main/RodrigoFlores)                                        23 jun, 12:45, Azul                                                                                                    Não sei
                                             [TiagoTrex](/Main/TiagoTrex)                                            Nativo                                                                                                                 N/A
                                      [ViniciusDepizzol](/Main/ViniciusDepizzol)                                     24 jun, 12:30, TAM                                                                                                     Master Express
                                    [UlissisGomesCorrea](/Main/UlissisGomesCorrea)                                   23 jun                                                                                                                 Não sei
   [VicenteAguiar[?](/bin/edit/Main/VicenteAguiar?topicparent=GNOMEBR.FISL10 "Create this topic")]{.foswikiNewLink}  23 jun, 19:10h, AZUL                                                                                                   Não sei

[]{#Programa_o} Programação
---------------------------

Dia 26/06 - Sexta

14h-16h

[O Projeto GNOME: Evolução e
Participação](http://fisl.softwarelivre.org/10/papers/pub/programacao/277)

[JonhWendell](/Main/JonhWendell) e Gustavo Noronha

\

Dia 27/06 - Sábado

13h-14h

[GIMP Avançado para
Designers](http://fisl.softwarelivre.org/10/papers/pub/programacao/412)

João Bueno

15h-16h

[WebKitGTK+ - o que é e como
usar](http://fisl.softwarelivre.org/10/papers/pub/programacao/131)

Gustavo Noronha

18h-19h

[Software livre, cultura hacker e o ecossistema da
colaboração](http://fisl.softwarelivre.org/10/papers/pub/programacao/238)

[Vicente
Aguiar[?](/bin/edit/Main/VicenteAguiar?topicparent=GNOMEBR.FISL10 "Create this topic")]{.foswikiNewLink}
e Sérgio Amadeu

[]{#Estande} Estande
--------------------

Este ano temos disponível um estande, onde podemos nos encontrar, trocar
idéias, programar, traduzir, tirar dúvidas, etc. Haverá sempre alguém
disponível durante os quatro dias do evento.

[]{#Slides_dos_Palestrantes} Slides dos Palestrantes
----------------------------------------------------

-   Para ter acesso aos slides de algumas das palestras apresentadas no
    [FISL](FISL.html) 10, acesse a nossa página de
    [apresentações](Apresentacoes.html).

[]{#Fotos_do_Evento} Fotos do Evento
------------------------------------

![3664581339\_7fecfc26b5.jpg](http://farm3.static.flickr.com/2562/3664581339_7fecfc26b5.jpg)

Palestras sobre GNOME

![3664582521\_2264487e2e.jpg](http://farm4.static.flickr.com/3601/3664582521_2264487e2e.jpg)

Palestra GNOME 3 : Vinicius + Gustavo Noronha

![3665387798\_2789f4ef36.jpg](http://farm4.static.flickr.com/3320/3665387798_2789f4ef36.jpg)

Palestra Como contribuir para o GNOME : John Wendell

![3664583693\_a0d8cf8101.jpg](http://farm4.static.flickr.com/3561/3664583693_a0d8cf8101.jpg)

Palestra Utilizando o GNOME : Julio Cesar\
