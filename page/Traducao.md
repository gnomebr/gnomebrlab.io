%Equipe brasileira de tradução do GNOME!

Essa é a página da **Equipe Brasileira de Tradução do GNOME**. Somos
parte da [comunidade GNOME Brasil](http://br.gnome.org/), e trabalhamos
com o [GNOME Translation
Project](http://wiki.gnome.org/TranslationProject/) e o
[LDP-BR](http://trac.watter.net/ldp-br/wiki). Nossa missão é adaptar e
traduzir o GNOME para os brasileiros, com a melhor qualidade e da forma
mais completa possível. Para entrar em contato conosco, acesse a página
[Comunidade](Comunidade.html). 

Se você tiver encontrado um erro de tradução, por favor relate-o
seguindo o link disponibilizado na [nossa página no servidor de
traduções do GNOME](http://l10n.gnome.org/languages/pt_BR). Essa página
também disponibiliza estatísticas de tradução, e a lista completa dos
membros da equipe, agrupados por hierarquia.

Se você quiser ajudar a *localizar*, ou seja, traduzir e adaptar o GNOME
para o português brasileiro, leia a documentação abaixo e apresente-se
na [nossa lista de discussão](Comunidade.html).

## Saiba mais {#Saiba_mais}

-   [Revisores -\> tradutores](MentoringTradutores.html) --- Lista
    de Revisores e os tradutores acompanhados.
-   [Guia do tradutor](GuiaDoTradutor.html) --- uma introdução ao
    trabalho da nossa equipe.
-   [Erros frequentes](ErrosFrequentes.html) --- quem não conhece a
    história, está fadado a repeti-la ;-)
-   [Guia de estilo](GuiaDeEstilo.html) --- tanto para a interface
    quanto para a documentação.
-   [Ferramentas de tradução](FerramentasDeTraducao.html) --- desde
    aplicativos até scripts úteis.
-   [Como revisar uma tradução](ComoRevisar.html) --- antes de
    enviá-la ao Vertimus ou ao GIT.
-   [Trechos para reaproveitar](TrechosParaReaproveitar.html) ---
    trechos que podem ser úteis na hora de traduzir.
-   [Pauta da próxima reunião](PautaReuniao.html) --- pautas das
    reuniões que ocorrem sobre tradução.
-   [Memórias da equipe brasileira de tradução do
    GNOME](MemoriasDaEquipeDeTraducao.html).

## Veja também {#Veja_tambem}

Outros projetos de tradução:

-   [Equipe brasileira do Translation Project
    (LDP-BR)](http://translationproject.org/team/pt_BR.html). ([Lista de
    discussão](https://lists.sourceforge.net/lists/listinfo/ldpbr-translation))

Projetos de tradução de distribuições:

-   [Equipe brasileira de tradução do
    Debian](http://wiki.debianbrasil.org/).
-   [Equipe brasileira de tradução do
    Fedora](http://fedoraproject.org/wiki/L10N/Teams/BrazilianPortuguese).
-   [Equipe brasileira de tradução do
    Gentoo](http://www.gentoo.org/doc/pt_br/overview.xml).
-   [Equipe brasileira de tradução do
    Ubuntu](http://wiki.ubuntubrasil.org/TimeDeTraducao).
