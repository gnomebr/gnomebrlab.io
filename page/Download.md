[]{#Como_instalar_o_GNOME} Como instalar o GNOME
================================================

Para instalar ou atualizar sua máquina para o GNOME 2.32, nós
recomendamos que você instale os pacotes oficiais da sua distribuição.
[Distribuições populares](SuporteComunitario.html) irão
disponibilizar o GNOME 2.32 muito em breve, e algumas já possuem versões
em desenvolvimento com o GNOME 2.32. Você pode obter uma lista delas e
descobrir a última versão oferecida em nossa página *[Get
Footware](http://people.gnome.org/~daniellem/footware.shtml)*.

------------------------------------------------------------------------

[]{#Compilando} Compilando\...
------------------------------

Se você é corajoso, paciente e gosta de compilar o GNOME a partir do
código-fonte, nós recomendamos que você use o
[JHBuild](http://library.gnome.org/devel/jhbuild/), que está preparado
para compilar a última versão do GNOME do Git. Você pode usar o JHBuild
para compilar o GNOME 2.32.x usando a definição de módulo gnome-2.32.

Embora seja possível compilar o GNOME diretamente dos tarballs da
versão, nós recomendamos fortemente que você use o JHBuild.

::: {style="margin-left:-9999px;"}
:::
