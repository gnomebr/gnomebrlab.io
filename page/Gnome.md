%Sobre o GNOME

O [GNOME](http://www.gnome.org/) é um projeto Internacional de [software
livre](http://www.gnu.org/philosophy/free-sw.html) que provê basicamente
duas coisas: o *ambiente desktop GNOME*, intuitivo e atraente para
usuários finais; e a *plataforma de desenvolvimento GNOME*, um framework
extenso para construção de aplicações que se integrem com todo o
desktop. Descubra então mais abaixo o porquê de escolher o GNOME.

------------------------------------------------------------------------

## O GNOME é\...

### Livre {#livre}

O GNOME é [Software Livre](http://pt.wikipedia.org/wiki/Software_livre)
e parte do [Projeto GNU](http://www.gnu.org/), que se dedica a dar a
usuários e desenvolvedores controle sobre seus desktops, software e
dados. Isso consiste, basicamente, em garantir a todos o direito a
quatro liberdades básicas: a de usar o software para qualquer fim,
estudar o seu código-fonte, modificá-lo para qualquer necessidade que
você possa ter e redistribui-lo, modificado ou não.  

### Simples e Intuitivo {#simpleseintuitivo}

O GNOME entende que usabilidade é criar programas que sejam de fácil
utilização para todos e não simplesmente abarrotá-los com recursos. A
comunidade GNOME de profissionais e voluntários especialistas em
usabilidade criou as primeiras as Diretrizes para Interface Humana em
software livre. E todas os principais aplicativos do GNOME seguem estes
princípios. Descubra mais a respeito na [página das Diretrizes para
Interface Humana do
GNOME](https://developer.gnome.org/hig/stable/index.html.pt_BR)

### Acessível {#acessivel}

Parte da filosofia por trás do Software Livre é garantir essa liberdade
para todos, sem exceção, o que inclui usuários e desenvolvedores com
deficiências. Graças a vários anos de esforço contínuo, o GNOME é, hoje,
o ambiente desktop mais acessível em qualquer plataforma Unix. Descubra
mais a respeito na [página da Equipe de Acessibilidade do
GNOME](https://wiki.gnome.org/Accessibility).

### Internacional {#internacional}

Existem muitas línguas fundamentalmente diferentes ao redor do mundo,
que impõem formas de pensar diferentes. O GNOME acredita que os usuários
têm o direito a usar seus computadores na sua língua mãe, que é aquela
com a qual eles se sentem mais confortáveis. Por isso, nos esforçamos
muito para garantir que todo software que é parte do GNOME possa ser
traduzido para todas as línguas e temos, como resultado, um ambiente
desktop funcionando em mais de [100 (cem) línguas e
dialetos](http://l10n.gnome.org/languages/) que, em número de falantes,
correspondem a aproximadamente 70% da população mundial. E, como não
poderia deixar de ser, temos o GNOME completamente traduzido para
português do Brasil, graças ao nosso [Projeto de
Tradução.](Traducao.html)

### Uma comunidade {#umacomunidade}

Antes de mais nada, o GNOME é uma comunidade internacional de
profissionais e voluntários que projetam, programam, traduzem,
documentam, garantem qualidade e geralmente se divertem juntos. Mais
interessante ainda é que essa comunidade é completamente aberta, e muito
receptiva a novos colaboradores. [Saiba como fazer
parte.](Comunidade.html)

### Organizado {#organizado}

O GNOME se esforça para ser uma comunidade bem organizada, com uma
[Fundação](Fundacao.html) com centenas de membros e uma diretoria
eleita anualmente, equipes de acessibilidade, usabilidade, garantia de
qualidade, otimização, entre outas. Os lançamentos do GNOME são
controlados por uma equipe de lançamento e realizados, rigidamente, a
cada seis meses.

### Independente {#independente}

GNOME é liderada pela organização sem fins lucrativos Fundação GNOME.
Nosso conselho é eleito democraticamente, e as decisões técnicas são
feitas pelos engenheiros que fazem o trabalho. Contamos com o apoio de
muitas organizações; funcionários de mais de cem empresas têm
contribuído desde o início do projeto.

### Pessoas com foco {#pessoascomfoco}

Nosso software é traduzido em muitas línguas e vem com construído em
recursos de acessibilidade. Isto significa que ele pode ser usado por
qualquer pessoa, independentemente da língua que falam ou suas
capacidades físicas.

### Apoiado {#apoiado}

Além da comunidade internacional do GNOME, o projeto tem o apoio de
empresas líderes em Unix e Linux, incluindo
[Canonical](http://www.canonical.com), [Debian](http://www.debian.org),
[Intel](http://www.intel.com.br), [Red Hat](http://www.redhat.com) e
[SUSE](http://www.suse.com).

Veja a lista completa de patrocinadores em
[https://www.gnome.org/foundation/](https://www.gnome.org/foundation/)
