%Pauta da Reunião de 16/10

Horário: 20h (Horário de Brasília, GMT -0300), local \#gnome-br na Rede
GimpNet.

### []{#Pauta} Pauta

-   Apresentação
-   Manter padrões
-   Planos para a nova ortografia
-   Regras de autoria
-   String freeze breaks
-   Mudança nas regras de promoção
-   Tradução via web
