[]{#Fotos_do_III_ENSL_em_Salvador_Ma} Fotos do III [ENSL](ENSL.html) em Salvador. Maio 2009
===============================================================================================

::: {#igp1 .igp .jqSlimbox .{itemSelector:'.igpThumbNail', .singleMode:true}}
[![Carlão na sua aula de
GTK+](/pub/images/GNOMEBR/IIIENSLFotos/1/thumb_3582113393_7633992ddb_b.jpg)](/pub/images/GNOMEBR/IIIENSLFotos/1/3582113393_7633992ddb_b.jpg "Carlão na sua aula de GTK+"){.igpThumbNail
.{origUrl:"/IIIENSLFotos/3582113393_7633992ddb_b.jpg"}}
[![Marcelo Santana na sua
palestra](/pub/images/GNOMEBR/IIIENSLFotos/1/thumb_3582182015_b169673e96_b.jpg)](/pub/images/GNOMEBR/IIIENSLFotos/1/3582182015_b169673e96_b.jpg "Marcelo Santana na sua palestra"){.igpThumbNail
.{origUrl:"/IIIENSLFotos/3582182015_b169673e96_b.jpg"}}
[![Mesa redonda: como contribuir com software
livre](/pub/images/GNOMEBR/IIIENSLFotos/1/thumb_3582249805_ce4a0771c0_b.jpg)](/pub/images/GNOMEBR/IIIENSLFotos/1/3582249805_ce4a0771c0_b.jpg "Mesa redonda: como contribuir com software livre"){.igpThumbNail
.{origUrl:"/IIIENSLFotos/3582249805_ce4a0771c0_b.jpg"}}
[![Wendell, Alex e
Fábio](/pub/images/GNOMEBR/IIIENSLFotos/1/thumb_dsc03587.jpg)](/pub/images/GNOMEBR/IIIENSLFotos/1/dsc03587.jpg "Wendell, Alex e Fábio"){.igpThumbNail
.{origUrl:"/IIIENSLFotos/dsc03587.jpg"}} [![Wendell e Fábio
na palestra de
acessibilidade](/pub/images/GNOMEBR/IIIENSLFotos/1/thumb_dsc03588.jpg)](/pub/images/GNOMEBR/IIIENSLFotos/1/dsc03588.jpg "Wendell e Fábio na palestra de acessibilidade"){.igpThumbNail
.{origUrl:"/IIIENSLFotos/dsc03588.jpg"}} [![Lazer depois do
evento](/pub/images/GNOMEBR/IIIENSLFotos/1/thumb_dsc03594.jpg)](/pub/images/GNOMEBR/IIIENSLFotos/1/dsc03594.jpg "Lazer depois do evento"){.igpThumbNail
.{origUrl:"/IIIENSLFotos/dsc03594.jpg"}} [![Acessibilidade
no GNOME com
John](/pub/images/GNOMEBR/IIIENSLFotos/1/thumb_dscf9598.jpg)](/pub/images/GNOMEBR/IIIENSLFotos/1/dscf9598.jpg "Acessibilidade no GNOME com John"){.igpThumbNail
.{origUrl:"/IIIENSLFotos/dscf9598.jpg"}}[]{.foswikiClear}
:::
