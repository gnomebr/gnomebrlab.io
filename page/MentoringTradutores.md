[]{#Mentoring_entre_Revisores_e_Trad} Mentoring entre Revisores e Tradutores
============================================================================

Como decidido pela equipe em Novembro de 2010. Cada tradutor poderá ser
acompanhado por um único revisor, desde que o queira.

Esta página visa relacionar os nomes das pessoas que estão trabalhando
juntas. O acordo de quem trabalha com quem e também as conversas pode se
dar através de e-mail pessoal, *jabber* ou por outro meio que for
preferido para ambos.

*Nota:* Cada revisor poderá acompanhar mais de um tradutor, desde que
tenha tempo livre e seja avisado antes.

Tradutores e revisores, relacionem aqui com quem vocês estão trabalhando
junto, seguindo o esquema abaixo:

[]{#Rela_o_de_nomes} Relação de nomes
-------------------------------------

    [Revisor](/MentoringTradutoressortcol=0;table=1;up=0#sorted_table "Sort by this column") [Tradutor](/MentoringTradutoressortcol=1;table=1;up=0#sorted_table "Sort by this column")
  ---------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------
                                                                                                           Antonio Fernandes - \- Gabriel Villar
                                                                                                             Djavan Fagundes - \- Mateus Zenaide
                                                                                                             Flamarion Jorge - \- *Livre*
