%Como Revisar a Tradução de Documentação

Esta é uma página da [equipe brasileira de tradução do
GNOME](Traducao.html). Quando você traduzir a documentação de um
programa, deve revisar o catálogo de mensagens antes de colocá-lo no
Bugzilla do GNOME. Se o catálogo contiver mensagens previamente
traduzidas, considere a possibilidade de revisá-las também. Sua tradução
será considerada de qualidade na medida em que o revisor tenha pouco o
que corrigir.

Antes de começar a traduzir a documentação, instale o programa
correspondente. A versão tem de ser exatamente a mesma! Só assim você
poderá ter certeza de que a tradução da documentação esteja de acordo
com a da interface. Durante a tradução propriamente dita, o catálogo de
mensagens da interface pode até quebrar o galho, mas durante a revisão
você precisará abrir o programa.

Observe que, no caso de você ter que instalar exatamente a mesma versão
do programa referenciado na documentação, a revisão pode ser um processo
muito trabalhoso e exigir bastante tempo, especialmente se a
documentação em questão se referir a um programa de uma versão ainda em
desenvolvimento do GNOME, pois é bem provável que você terá que compilar
você mesmo a aplicação. Mas como essas aplicações carregam muitas
dependências de outros componentes do GNOME, talvez você tenha que
compilar e instalar uma versão nova do GNOME inteiro! E se você não
dispõe de uma boa máquina ou de tempo e espaço em disco para isso,
talvez seja melhor se concentrar em outras tarefas mais simples. Para
compilar versões em desenvolvimento do GNOME, uma boa opção é o
[JHBuild](https://wiki.gnome.org/Projects/Jhbuild/).

Se a sua ferramenta de tradução tiver verificador ortográfico,
habilite-o antes de começar a tradução. No caso do poEdit, para que o
verificador ortográfico funcione é necessário configurar o idioma e o
país do catálogo de mensagem no menu *Catálogo \| Configurações*. No
caso do Vim, vale a pena atualizar o [destaque de sintaxe
PO](http://www.vim.org/scripts/script.php?script_id=913), pois a versão
que vem com o Vim não restringe a verificação às mensagens traduzidas.

Durante a tradução do catálogo, releia cada mensagem à medida em que for
terminando de traduzi-las. A documentação costuma gerar mensagens
longas, e é comum que o tradutor as traduza parte por parte. Revisando
os parágrafos logo após traduzi-los, você garante ao usuário uma leitura
mais agradável e compreensível.

Quando tiver concluído a tradução do catálogo (parabéns!), verifique-o
com o `msgfmt` e o `pofilter` (mais instruções a seguir). Esse passo não
é necessário para quem usa o Pootle ou o Pootling (que verificam o
catálogo com o próprio `pofilter`), ou para quem usa o KBabel (cujo
verificador é muito bom, ainda que menos abrangente).

Por fim, crie um arquivo DocBook traduzido (mais informações a seguir) e
abra-o com o navegador da ajuda Yelp com o comando
`yelp "$PWD/help.xml" &`. Leia a documentação do começo ao fim,
verificando se os termos usados correspondem mesmo à interface de
usuário, e procurando eventuais erros ou trechos passíveis de melhora.

Nota: a variável `$PWD` e as aspas são necessárias devido a um erro
conhecido do Yelp mas que em breve deixarão de ser necessárias.

Se você não tiver verificado ainda a ortografia, pode fazê-lo agora.
Execute o comando:

    aspell --mode=sgml check help.xml

no DocBook traduzido, mas lembre-se de que as correções devem ser feitas
no catálogo de mensagens.

Muito bem, revisão concluída! Quando anexar o catálogo de mensagens a um
relatório de erro no Bugzilla do GNOME, aproveite para listar os
cuidados que teve ao revisar a tradução. Com certeza isso motivará o
*committer* a dar prioridade a esse catálogo.

------------------------------------------------------------------------

## Verificando um catálogo com o msgfmt e o pofilter {#Verificando_um_catologo}

O utilitário `msgfmt` faz parte do pacote `gettext`. Apesar de ele
verificar poucos erros, alguns destes não são apontados por outras
ferramentas. A sintaxe é

    msgfmt -cvo /dev/null help.VERSAO.pt_BR.po

O `pofilter` faz parte do pacote
[translate-toolkit](http://translate.sourceforge.net/). Mantendo o
catálogo aberto em sua ferramenta de tradução, execute o seguinte
comando no terminal:

    pofilter --gnome arquivo.po | less

Os avisos do tipo `xmltags` são os mais importantes, pois apontam erros
capazes de quebrar a estrutura da documentação. Os outros avisos são
menos importantes, e às vezes nem apontam erros de fato, mas vale a pena
conferi-los.

------------------------------------------------------------------------

## Criando um arquivo DocBook traduzido {#Criando um arquivo DocBook}

A tradução do GNOME é um conjunto de arquivos XML, mais especificamente
de arquivos no formato DocBook. Quando o código-fonte do programa é
compilado, a documentação em inglês é cruzada com os catálogos de
mensagem para gerar arquivos DocBook para cada idioma. Isso é feito com
a ferramenta `xml2po`, parte do pacote `gnome-doc-utils`. Os passos a
seguir podem assustar a princípio, mas depois de segui-los uma ou duas
vezes você ficará à vontade.

Para começar, abra o navegador em
<http://l10n.gnome.org/languages/pt_BR>, siga o link para o *release
set* adequado (por exemplo, GNOME 2.20), e siga o link do nome do
programa (não clique no ícone de download ao lado!).

O cabeçalho da página conterá uma linha semelhante a *Branches: HEAD ·
gnome-2-20 · gnome-2-18 · gnome-2-16 · gnome-2-14*. O nome de arquivo do
catálogo de mensagens já diz qual é o ramo (*branch*) adequado,
lembrando que *HEAD* é sempre referente a versão em desenvolvimento.
Siga o link ou role a página até chegar na parte correspondente, e siga
o link *Browse SVN* que está ao lado do subtítulo com o nome da versão.

Você chegará então a uma página com vários arquivos e diretórios. O que
você precisa saber é que a documentação fica no diretório `help`; a
título de curiosidade, a tradução da interface de usuário fica no
diretório `po`. Seguindo o link para a pasta `help`, você encontrará o
diretório `C`, com a documentação em inglês, e ainda um diretório para
cada tradução.

Siga o link para pasta `C`. Você encontrará então um arquivo chamado
`MODULO.xml`, onde novamente `MODULO` é o apelido do programa. Exemplos:
`gnome-power-manager` para o Gerenciador de Energia, `eog` para o
Visualizador de Imagens, `bug-buddy` para a Ferramenta de Relatório de
Erros. Além do arquivo `MODULO.XML`, você encontrará ainda o arquivo
`legal.xml` e o diretório `figures`, que contém as ilustrações. Copie os
arquivos `MODULO.xml` e `legal.xml` para uma pasta temporária em seu
disco rígido.

Muito bem, agora você tem uma pasta com o catálogo de mensagens e dois
arquivos DocBook em inglês. Para gerar a documentação em inglês, execute
o seguinte comando:

    xml2po -p help.VERSAO.pt_BR.po CAMINHO/PARA/MODULO.xml > help.xml
    xml2po -p help.VERSAO.pt_BR.po CAMINHO/PARA/legal.xml > legal.xml

Pronto! Agora você pode abri-lo com o Yelp.

