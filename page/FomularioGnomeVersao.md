[]{#Escolha_a_Vers_o_do_Gnome} Escolha a Versão do Gnome
========================================================

[]{#Instru_es} Instruções
-------------------------

Inclua este tópico para ter uma caixa *dropdown* com uma lista de
versões do gnome a serem escolhidas.

Exemplo:

    <form ...>
    ...
    %INCLUDE{FomularioGnomeVersao}%
    ...
    </form>

***Atenção:*** o nome do campo gerado é `VersodoGnome`.

[]{#C_digo} Código
------------------

Escolha\...\<=2.102.142.162.18

\-- [LeandroSantos](/Main/LeandroSantos) - 19 May 2007
