%Anual Report 2007

[Relatorio Anual 2007 Versão em
Português](RelatorioAnual2007VersaoEmPortugues.html)

We organized a GNOME community event inside the 9th Fórum Internacional
Software Livre ([FISL](FISL.html)), the largest FLOSS conference in
Brazil. There were talks about the project and how to contribute to the
brazilian translation team. Also, we had an important public meeting
with the aim to set our goals for 2007. We also participated with talks
on Latinoware, another important FLOSS conference in Brazil.

The 4th Fórum GNOME, the yearly event of the brazilian GNOME users and
developers community, took place as a community event of the 2nd
Northeastern Free Software Conference, the biggest free software event
in northeastern Brazil, at Aracaju/SE, from September 28th to 30th. The
Fórum was a very good event, where we could bring new contributors,
specially translators.

In 2007, we got to unify all GNOME brazilian websites into a brand-new
website. The new website is based on a wiki system what will make it
easier to keep it always updated and to get more contributors.

On the contributions side, the brazilian translation team got to
translate 100% of the GNOME 2.18 and 2.20 desktop. Unfortunately, one of
our most valuable translators, Raphael Higino, passed away on October 7.
Jonh Wendell became co-maintainer of Vino, the GNOME remote desktop
(VNC) server, and he is now working on a GNOME remote desktop client
called Vinagre which has been already proposed for GNOME 2.22 desktop.

Our plans for 2008 are to have a much nicer Fórum GNOME. For the 5th
edition of the Fórum, we\'re planning to have the participation of
foreigns speakers and some workshops with in-place bug fixes/coding.
