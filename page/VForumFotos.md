#Fotos do 5º Fórum GNOME / Latinoware 2008

::: {#igp1 .igp .jqSlimbox .{itemSelector:'.igpThumbNail', .singleMode:true}}
[![Sandino hackeando até de microfone na
mão!](/pub/images/GNOMEBR/VForumFotos/1/thumb_00001.jpg)](/pub/images/GNOMEBR/VForumFotos/1/00001.jpg "Sandino hackeando até de microfone na mão!"){.igpThumbNail
.{origUrl:"/VForumFotos/00001.jpg"}} [![Pessoal do Ubuntu-br
prestigiando o estande do
GNOME.](/pub/images/GNOMEBR/VForumFotos/1/thumb_00006.jpg)](/pub/images/GNOMEBR/VForumFotos/1/00006.jpg "Pessoal do Ubuntu-br prestigiando o estande do GNOME."){.igpThumbNail
.{origUrl:"/VForumFotos/00006.jpg"}} [![German e a \"quinta
liberdade\" do software
livre!](/pub/images/GNOMEBR/VForumFotos/1/thumb_00016.jpg)](/pub/images/GNOMEBR/VForumFotos/1/00016.jpg "German e a "quinta liberdade" do software livre!"){.igpThumbNail
.{origUrl:"/VForumFotos/00016.jpg"}} [![Plateia assistindo
como exterminar bugs no
GNOME!](/pub/images/GNOMEBR/VForumFotos/1/thumb_00027.jpg)](/pub/images/GNOMEBR/VForumFotos/1/00027.jpg "Plateia assistindo como exterminar bugs no GNOME!"){.igpThumbNail
.{origUrl:"/VForumFotos/00027.jpg"}} [![Gnomers mostrando
como se extermina um
\"bug\"\...](/pub/images/GNOMEBR/VForumFotos/1/thumb_00196.jpg)](/pub/images/GNOMEBR/VForumFotos/1/00196.jpg "Gnomers mostrando como se extermina um "bug"..."){.igpThumbNail
.{origUrl:"/VForumFotos/00196.jpg"}} [![GNOME
\"Whisky](/pub/images/GNOMEBR/VForumFotos/1/thumb_00224.jpg)](/pub/images/GNOMEBR/VForumFotos/1/00224.jpg "GNOME "Whisky"){.igpThumbNail
.{origUrl:"/VForumFotos/00224.jpg"}} [![Cataratas Conf:
\"apresentação do Projeto Macuco\".
:-)](/pub/images/GNOMEBR/VForumFotos/1/thumb_1010186.jpg)](/pub/images/GNOMEBR/VForumFotos/1/1010186.jpg "Cataratas Conf: "apresentação do Projeto Macuco". :-)"){.igpThumbNail
.{origUrl:"/VForumFotos/1010186.jpg"}} [![Diego e Wendell,
Turquia na
cabeça!](/pub/images/GNOMEBR/VForumFotos/1/thumb_DSC03019.JPG)](/pub/images/GNOMEBR/VForumFotos/1/DSC03019.JPG "Diego e Wendell, Turquia na cabeça!"){.igpThumbNail
.{origUrl:"/VForumFotos/DSC03019.JPG"}} [![Izabel, Wendell,
Eline e Fábio, aguardando o ônibus para o jantar de
abertura](/pub/images/GNOMEBR/VForumFotos/1/thumb_DSC03020.JPG)](/pub/images/GNOMEBR/VForumFotos/1/DSC03020.JPG "Izabel, Wendell, Eline e Fábio, aguardando o ônibus para o jantar de abertura"){.igpThumbNail
.{origUrl:"/VForumFotos/DSC03020.JPG"}} [![Fábio na sua
palestra sobre tradução do
GNOME](/pub/images/GNOMEBR/VForumFotos/1/thumb_DSC03041.JPG)](/pub/images/GNOMEBR/VForumFotos/1/DSC03041.JPG "Fábio na sua palestra sobre tradução do GNOME"){.igpThumbNail
.{origUrl:"/VForumFotos/DSC03041.JPG"}} [![Brincadeira no
estande do KDE, que estava todo enfeitado de
GNOME](/pub/images/GNOMEBR/VForumFotos/1/thumb_DSC03043.JPG)](/pub/images/GNOMEBR/VForumFotos/1/DSC03043.JPG "Brincadeira no estande do KDE, que estava todo enfeitado de GNOME"){.igpThumbNail
.{origUrl:"/VForumFotos/DSC03043.JPG"}} [![Wendell
preparando a
palestra\...](/pub/images/GNOMEBR/VForumFotos/1/thumb_DSC03044.JPG)](/pub/images/GNOMEBR/VForumFotos/1/DSC03044.JPG "Wendell preparando a palestra..."){.igpThumbNail
.{origUrl:"/VForumFotos/DSC03044.JPG"}} [![Palestra do
Wendell, sobre
acessibilidade.](/pub/images/GNOMEBR/VForumFotos/1/thumb_DSC03046.JPG)](/pub/images/GNOMEBR/VForumFotos/1/DSC03046.JPG "Palestra do Wendell, sobre acessibilidade."){.igpThumbNail
.{origUrl:"/VForumFotos/DSC03046.JPG"}} [![Equipe GNOME
América Latina no encerramento do evento!
Uhuuu!](/pub/images/GNOMEBR/VForumFotos/1/thumb_DSC03050.JPG)](/pub/images/GNOMEBR/VForumFotos/1/DSC03050.JPG "Equipe GNOME América Latina no encerramento do evento! Uhuuu!"){.igpThumbNail
.{origUrl:"/VForumFotos/DSC03050.JPG"}} [![Pizza de
encerramento!
:-)](/pub/images/GNOMEBR/VForumFotos/1/thumb_DSC03052.JPG)](/pub/images/GNOMEBR/VForumFotos/1/DSC03052.JPG "Pizza de encerramento! :-)"){.igpThumbNail
.{origUrl:"/VForumFotos/DSC03052.JPG"}} [![Depois da pizza
de
encerramento\...](/pub/images/GNOMEBR/VForumFotos/1/thumb_DSC03057.JPG)](/pub/images/GNOMEBR/VForumFotos/1/DSC03057.JPG "Depois da pizza de encerramento..."){.igpThumbNail
.{origUrl:"/VForumFotos/DSC03057.JPG"}} [![Vicente
apresentando a cidade para o
\"GIMP\"\...](/pub/images/GNOMEBR/VForumFotos/1/thumb_dsc03931.jpg)](/pub/images/GNOMEBR/VForumFotos/1/dsc03931.jpg "Vicente apresentando a cidade para o "GIMP"..."){.igpThumbNail
.{origUrl:"/VForumFotos/dsc03931.jpg"}} [![GNOME
Bus!](/pub/images/GNOMEBR/VForumFotos/1/thumb_dsc03937.jpg)](/pub/images/GNOMEBR/VForumFotos/1/dsc03937.jpg "GNOME Bus!"){.igpThumbNail
.{origUrl:"/VForumFotos/dsc03937.jpg"}} [![Equipe GNOME se
apresentando na
\"conferência\"!](/pub/images/GNOMEBR/VForumFotos/1/thumb_dsc04078.jpg)](/pub/images/GNOMEBR/VForumFotos/1/dsc04078.jpg "Equipe GNOME se apresentando na "conferência"!"){.igpThumbNail
.{origUrl:"/VForumFotos/dsc04078.jpg"}} [![Estande do
GNOME.](/pub/images/GNOMEBR/VForumFotos/1/thumb_HPIM5763.JPG)](/pub/images/GNOMEBR/VForumFotos/1/HPIM5763.JPG "Estande do GNOME."){.igpThumbNail
.{origUrl:"/VForumFotos/HPIM5763.JPG"}} [![Vinicius, Wendell
e Diego no estande do
GNOME.](/pub/images/GNOMEBR/VForumFotos/1/thumb_HPIM5765.JPG)](/pub/images/GNOMEBR/VForumFotos/1/HPIM5765.JPG "Vinicius, Wendell e Diego no estande do GNOME."){.igpThumbNail
.{origUrl:"/VForumFotos/HPIM5765.JPG"}} [![Pedro, Bruno,
Diego e German se
divertindo\...](/pub/images/GNOMEBR/VForumFotos/1/thumb_HPIM5793.JPG)](/pub/images/GNOMEBR/VForumFotos/1/HPIM5793.JPG "Pedro, Bruno, Diego e German se divertindo..."){.igpThumbNail
.{origUrl:"/VForumFotos/HPIM5793.JPG"}} [![Diego em sua
palestra sobre o desenvolvimento de aplicações no Projeto
GNOME.](/pub/images/GNOMEBR/VForumFotos/1/thumb_HPIM5915.JPG)](/pub/images/GNOMEBR/VForumFotos/1/HPIM5915.JPG "Diego em sua palestra sobre o desenvolvimento de aplicações no Projeto GNOME."){.igpThumbNail
.{origUrl:"/VForumFotos/HPIM5915.JPG"}} [![Bruno em sua
palestra, sobre \"o massacre dos
bugs\"!](/pub/images/GNOMEBR/VForumFotos/1/thumb_HPIM5928.JPG)](/pub/images/GNOMEBR/VForumFotos/1/HPIM5928.JPG "Bruno em sua palestra, sobre "o massacre dos bugs"!"){.igpThumbNail
.{origUrl:"/VForumFotos/HPIM5928.JPG"}} [![Pedro, durante a
palestra dele e do Bruno.
WTH??](/pub/images/GNOMEBR/VForumFotos/1/thumb_HPIM5929.JPG)](/pub/images/GNOMEBR/VForumFotos/1/HPIM5929.JPG "Pedro, durante a palestra dele e do Bruno. WTH??"){.igpThumbNail
.{origUrl:"/VForumFotos/HPIM5929.JPG"}} [![Pedro, durante a
palestra dele e do
Bruno.](/pub/images/GNOMEBR/VForumFotos/1/thumb_HPIM5930.JPG)](/pub/images/GNOMEBR/VForumFotos/1/HPIM5930.JPG "Pedro, durante a palestra dele e do Bruno."){.igpThumbNail
.{origUrl:"/VForumFotos/HPIM5930.JPG"}} [![German e suas
peripécias!](/pub/images/GNOMEBR/VForumFotos/1/thumb_HPIM5940.JPG)](/pub/images/GNOMEBR/VForumFotos/1/HPIM5940.JPG "German e suas peripécias!"){.igpThumbNail
.{origUrl:"/VForumFotos/HPIM5940.JPG"}} [![Mesa KDE e GNOME
(com o kov): namoro ou
amizade?](/pub/images/GNOMEBR/VForumFotos/1/thumb_HPIM5950.JPG)](/pub/images/GNOMEBR/VForumFotos/1/HPIM5950.JPG "Mesa KDE e GNOME (com o kov): namoro ou amizade?"){.igpThumbNail
.{origUrl:"/VForumFotos/HPIM5950.JPG"}} [![Vinicius em sua
palestra sobre usabiliade no GNOME e o Projeto
Tango!](/pub/images/GNOMEBR/VForumFotos/1/thumb_HPIM5952.JPG)](/pub/images/GNOMEBR/VForumFotos/1/HPIM5952.JPG "Vinicius em sua palestra sobre usabiliade no GNOME e o Projeto Tango!"){.igpThumbNail
.{origUrl:"/VForumFotos/HPIM5952.JPG"}} [![Nicolas
contribuindo com o \"painel\"
GNOME.](/pub/images/GNOMEBR/VForumFotos/1/thumb_p1000892.jpg)](/pub/images/GNOMEBR/VForumFotos/1/p1000892.jpg "Nicolas contribuindo com o "painel" GNOME."){.igpThumbNail
.{origUrl:"/VForumFotos/p1000892.jpg"}} [![Izabel passando
as
instruções.](/pub/images/GNOMEBR/VForumFotos/1/thumb_P1000926.JPG)](/pub/images/GNOMEBR/VForumFotos/1/P1000926.JPG "Izabel passando as instruções."){.igpThumbNail
.{origUrl:"/VForumFotos/P1000926.JPG"}} [![Wendell e Fábio
discutindo o futuro da humanidade\...
:)](/pub/images/GNOMEBR/VForumFotos/1/thumb_P1010156.JPG)](/pub/images/GNOMEBR/VForumFotos/1/P1010156.JPG "Wendell e Fábio discutindo o futuro da humanidade... :)"){.igpThumbNail
.{origUrl:"/VForumFotos/P1010156.JPG"}} [![Diego, o
fotógrafo que mais faz caretas\...
:-P](/pub/images/GNOMEBR/VForumFotos/1/thumb_P1010170.JPG)](/pub/images/GNOMEBR/VForumFotos/1/P1010170.JPG "Diego, o fotógrafo que mais faz caretas... :-P"){.igpThumbNail
.{origUrl:"/VForumFotos/P1010170.JPG"}}
[![Pedro!](/pub/images/GNOMEBR/VForumFotos/1/thumb_P1010171.JPG)](/pub/images/GNOMEBR/VForumFotos/1/P1010171.JPG "Pedro!"){.igpThumbNail
.{origUrl:"/VForumFotos/P1010171.JPG"}} [![Vicente em sua
palestra sobre o \"bazar\" do Projeto
GNOME.](/pub/images/GNOMEBR/VForumFotos/1/thumb_P1010172.JPG)](/pub/images/GNOMEBR/VForumFotos/1/P1010172.JPG "Vicente em sua palestra sobre o "bazar" do Projeto GNOME."){.igpThumbNail
.{origUrl:"/VForumFotos/P1010172.JPG"}} [![German mostrando
como se contribui com o
Nautilus.](/pub/images/GNOMEBR/VForumFotos/1/thumb_p1010185.jpg)](/pub/images/GNOMEBR/VForumFotos/1/p1010185.jpg "German mostrando como se contribui com o Nautilus."){.igpThumbNail
.{origUrl:"/VForumFotos/p1010185.jpg"}}[]{.foswikiClear}
:::
