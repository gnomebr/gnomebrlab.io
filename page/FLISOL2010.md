::: {align="center"}
[]{#FLISOL_2010} FLISOL 2010
============================

[![Logo
FLISOL](http://djavan.comum.org/flisol.png)](http://flisol.net/FLISOL2010/)\
**24 de Abrill de 2010**
:::

O [FLISOL](http://flisol.net/FLISOL2010/), (Festival Latinoamericano de
Instalação de Software Livre) é o maior evento de divulgação de Software
Livre da América Latina. Ele acontece desde 2005 e seu principal
objetivo é promover o uso de software livre, apresentando sua filosofia,
seu alcance, avanços e desenvolvimento ao público em geral.

O GNOME estará presente em diversos locais, através de membros, usuários
ou de distribuições que o utilizam como ambiente desktop. Abaixo estão
listadas algumas das apresentações que falarão sobre o nosso ambiente
desktop predileto.

------------------------------------------------------------------------

[]{#foswikiTOC}

::: {.foswikiToc}
-   [Apresentações](#Apresenta_es)
-   [Slides dos
    Palestrantes](#Slides_dos_Palestrantes)
-   [Fotos do Evento](#Fotos_do_Evento)
:::

------------------------------------------------------------------------

[]{#Apresenta_es} Apresentações
-------------------------------

  --------------------------- ------------------------------------------------------------- ---------------------------
  AL - Maceió - **11:30h**    O projeto GNOME                                               Jonh Wendell
  BA - Sobradinho - **11h**   Traduzindo o GNOME                                            Antonio Fernandes C. Neto
  DF - Brasília - **15h**     Contribuindo com Traduções para o GNOME                       Flamarion Jorge
  MG - Belo Horizonte         Como funciona o GNOME/Experiências de contribuição upstream   Gustavo Noronha (Kov)
  --------------------------- ------------------------------------------------------------- ---------------------------

[]{#Slides_dos_Palestrantes} Slides dos Palestrantes
----------------------------------------------------

Logo após o FLISOL você poderá acessar os slides dos palestrantes na
nossa página de [apresentações](Apresentacoes.html).

[]{#Fotos_do_Evento} Fotos do Evento
------------------------------------

Aguardem!

\
