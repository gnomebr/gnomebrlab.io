[]{#Guia_de_Estilo_de_Tradu_o_da_Doc} Guia de Estilo de Tradução da Documentação do GNOME
=========================================================================================

Esta é uma página da [equipe brasileira de tradução do
GNOME](Traducao.html).

**Veja também:**

-   [Guia de Estilo da
    Conectiva](http://br.tldp.org/ferramentas/guia_estilo/guia_estilo.html);
-   [Guia de Estilo do
    GNOME](http://library.gnome.org/devel/gdp-style-guide); e
-   [Manual de Documentação do
    GNOME](http://library.gnome.org/devel/gdp-handbook).

------------------------------------------------------------------------

[]{#Tradu_o_dos_termos_da_licen_a} Tradução dos termos da licença
-----------------------------------------------------------------

Apesar dos termos da licença traduzidos não terem validade legal no país
(apenas no original a licença é considerada legítima), isso não quer
dizer que eles não devam ser traduzidos, pois o usuário pode querer se
familiarizar com esses termos e conhecer um pouco mais das licenças de
software que regem o mundo do *software* livre.

Para facilitar o trabalho de tradução e revisão, é ideal que o tradutor
não tenha que perder tempo *reinventando a roda* toda vez que se deparar
com os velhos e surrados termos da licença para traduzir. E não sendo o
tipo de coisa que se precisa fazer sempre, apenas quando se começa uma
nova tradução de documentação, pode-se até ignorar totalmente esta seção
quando o tradutor for apenas atualizar uma documentação já traduzida
(desde que alguém tenha seguido esses passos quando criou a
documentação, é claro ;)). Então, segue a tradução dos termos da
licença, bastando apenas copiar e colar no PO em questão:

> msgid \"\"\
> \"Permission is granted to copy, distribute and/or modify this
> document under the terms of the GNU Free Documentation License (GFDL),
> Version 1.1 or any later version published by the Free Software
> Foundation with no Invariant Sections, no Front-Cover Texts, and no
> Back-Cover Texts. You can find a copy of the GFDL at this link or in
> the file COPYING-DOCS distributed with this manual.\"\
> msgstr\"\"\
> \"Permissão concedida para copiar, distribuir e/ou modificar este
> documento sob os termos da Licença de Documentação Livre GNU (GNU Free
> Documentation License), Versão 1.1 ou qualquer versão mais recente
> publicada pela Free Software Foundation; sem Seções Invariantes,
> Textos de Capa Frontal, e sem Textos de Contracapa. Você pode
> encontrar uma cópia da licença GFDL no link ou no arquivo COPYING-DOCS
> distribuído com este manual.\"
>
> msgid \"\"\
> \"This manual is part of a collection of GNOME manuals distributed
> under the GFDL. If you want to distribute this manual separately from
> the collection, you can do so by adding a copy of the license to the
> manual, as described in section 6 of the license.\"\
> msgstr\"\"\
> \"Este manual é parte da coleção de manuais do GNOME distribuídos sob
> a GFDL. Se você quiser distribuí-lo separadamente da coleção, você
> pode fazê-lo adicionando ao manual uma cópia da licença, como descrito
> na seção 6 da licença.\"
>
> msgid \"\"\
> \"Many of the names used by companies to distinguish their products
> and services are claimed as trademarks. Where those names appear in
> any GNOME documentation, and the members of the GNOME Documentation
> Project are made aware of those trademarks, then the names are in
> capital letters or initial capital letters.\"\
> msgstr\"\"\
> \"Muitos dos nomes usados por empresas para distinguir seus produtos e
> serviços são reivindicados como marcas registradas. Onde esses nomes
> aparecem em qualquer documentação do GNOME e os membros do Projeto de
> Documentação do GNOME estiverem cientes dessas marcas registradas, os
> nomes aparecerão impressos em letras maiúsculas ou com iniciais em
> maiúsculas.\"
>
> msgid \"\"\
> \"DOCUMENT IS PROVIDED ON AN \\\"AS IS\\\" BASIS, WITHOUT WARRANTY OF
> ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION,
> WARRANTIES THAT THE DOCUMENT OR MODIFIED VERSION OF THE DOCUMENT IS
> FREE OF DEFECTS MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE OR
> NON-INFRINGING. THE ENTIRE RISK AS TO THE QUALITY, ACCURACY, AND
> PERFORMANCE OF THE DOCUMENT OR MODIFIED VERSION OF THE DOCUMENT IS
> WITH YOU. SHOULD ANY DOCUMENT OR MODIFIED VERSION PROVE DEFECTIVE IN
> ANY RESPECT, YOU (NOT THE INITIAL WRITER, AUTHOR OR ANY CONTRIBUTOR)
> ASSUME THE COST OF ANY NECESSARY SERVICING, REPAIR OR CORRECTION. THIS
> DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS LICENSE.
> NO USE OF ANY DOCUMENT OR MODIFIED VERSION OF THE DOCUMENT IS
> AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER; AND\"\
> msgstr\"\"\
> \"O DOCUMENTO É FORNECIDO NA BASE \\\"COMO ESTÁ\\\", SEM GARANTIAS DE
> QUALQUER TIPO, TANTO EXPRESSA OU IMPLÍCITA, INCLUINDO, MAS NÃO
> LIMITADO A, GARANTIAS DE QUE O DOCUMENTO OU VERSÃO MODIFICADA DO
> DOCUMENTO SEJA COMERCIALIZÁVEL, LIVRE DE DEFEITOS, PRÓPRIO PARA UM
> PROPÓSITO ESPECÍFICO OU SEM INFRAÇÕES. TODO O RISCO A RESPEITO DA
> QUALIDADE, EXATIDÃO, E DESEMPENHO DO DOCUMENTO OU VERSÕES MODIFICADAS
> DO DOCUMENTO É DE SUA RESPONSABILIDADE. SE ALGUM DOCUMENTO OU VERSÃO
> MODIFICADA SE PROVAR DEFEITUOSO EM QUALQUER ASPECTO, VOCÊ (NÃO O
> ESCRITOR INICIAL, AUTOR OU QUALQUER CONTRIBUIDOR) ASSUME O CUSTO DE
> QUALQUER SERVIÇO NECESSÁRIO, REPARO OU CORREÇÃO. ESSA RENÚNCIA DE
> GARANTIAS CONSTITUI UMA PARTE ESSENCIAL DESTA LICENÇA. NENHUM USO
> DESTE DOCUMENTO OU VERSÃO MODIFICADA DESTE DOCUMENTO É AUTORIZADO SE
> NÃO FOR SOB ESSA RENÚNCIA; E\"
>
> msgid \"\"\
> \"UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL THEORY, WHETHER IN TORT
> (INCLUDING NEGLIGENCE), CONTRACT, OR OTHERWISE, SHALL THE AUTHOR,
> INITIAL WRITER, ANY CONTRIBUTOR, OR ANY DISTRIBUTOR OF THE DOCUMENT OR
> MODIFIED VERSION OF THE DOCUMENT, OR ANY SUPPLIER OF ANY OF SUCH
> PARTIES, BE LIABLE TO ANY PERSON FOR ANY DIRECT, INDIRECT, SPECIAL,
> INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY CHARACTER INCLUDING,
> WITHOUT LIMITATION, DAMAGES FOR LOSS OF GOODWILL, WORK STOPPAGE,
> COMPUTER FAILURE OR MALFUNCTION, OR ANY AND ALL OTHER DAMAGES OR
> LOSSES ARISING OUT OF OR RELATING TO USE OF THE DOCUMENT AND MODIFIED
> VERSIONS OF THE DOCUMENT, EVEN IF SUCH PARTY SHALL HAVE BEEN INFORMED
> OF THE POSSIBILITY OF SUCH DAMAGES.\"\
> msgstr\"\"\
> \"SOB NENHUMA CIRCUNSTÂNCIA E SOB NENHUMA TEORIA LEGAL, TANTO EM DANO
> (INCLUINDO NEGLIGÊNCIA), CONTRATO, OU OUTROS, DEVEM O AUTOR, ESCRITOR
> INICIAL, QUALQUER CONTRIBUIDOR, OU QUALQUER DISTRIBUIDOR DO DOCUMENTO
> OU VERSÃO MODIFICADA DO DOCUMENTO, OU QUALQUER FORNECEDOR DE ALGUMA
> DESSAS PARTES, SER CONSIDERADOS RESPONSÁVEIS A QUALQUER PESSOA POR
> QUALQUER DANO, SEJA DIRETO, INDIRETO, ESPECIAL, ACIDENTAL OU
> CONSEQÜENCIAL DE QUALQUER INDIVÍDUO, INCLUINDO, MAS NÃO LIMITADO A,
> DANOS POR PERDA DE BOA VONTADE, TRABALHO PARADO, FALHA OU MAU
> FUNCIONAMENTO DO COMPUTADOR, OU QUALQUER E TODOS OS OUTROS DANOS OU
> PERDAS RESULTANTES OU RELACIONADOS AO USO DO DOCUMENTO E VERSÕES
> MODIFICADAS, MESMO QUE TAL PARTE TENHA SIDO INFORMADA DA POSSIBILIDADE
> DE TAIS DANOS.\"
>
> msgid \"\"\
> \"DOCUMENT AND MODIFIED VERSIONS OF THE DOCUMENT ARE PROVIDED UNDER
> THE TERMS OF THE GNU FREE DOCUMENTATION LICENSE WITH THE FURTHER
> UNDERSTANDING THAT: \<placeholder-1/\>\"\
> msgstr\"\"\
> \"O DOCUMENTO E VERSÕES MODIFICADAS DO DOCUMENTO SÃO FORNECIDOS SOB OS
> TERMOS DA GNU FREE DOCUMENTATION LICENSE COM O ENTENDIMENTO ADICIONAL
> DE QUE: \<placeholder-1/\>\"
