%CeSOL

[Congresso Estadual de Software Livre - Ceará
(CESoL-CE)](http://www.cesol.ufc.br/)

O CeSOL-CE é o maior evento do segmento no Ceará e um dos maiores do
nordeste. Este ano, o GNOME Brasil estará presente, através de uma
palestra, um workshop e um festival de desenvolvimento.

------------------------------------------------------------------------

## Programação {#programacao}


Dias            20/08 - Quarta-Feira
------------    --------------------------------
14-15h          O Projeto GNOME
15:30-19:30h    Workshop: Desenvolvendo no GNOME

Dias            21 até 23/08 - Quinta a Sábado
-----           ---------------------------------
                Festival de Desenvolvimento Livre

## Slides dos Palestrantes {#slides_dos_palestrantes}

Para ter acesso aos slides de algumas das palestras apresentadas no
CeSOL-CE, acesse a nossa página de
[apresentações](Apresentacoes.html).

Fotos do Evento {#Fotos_do_Evento}
------------------------------------

-   Em breve.

## Mais informações {#mais_informacoes}


\* [Blog do
Wendell](http://www.bani.com.br/lang/pt-br/2008/07/31/gnome-and-ubuntu-in-ceara-brazil-gnome-e-ubuntu-no-ceara/)

\
