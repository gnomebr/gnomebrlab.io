[]{#Latinoware_2009} Latinoware 2009
====================================

### []{#Palestras} Palestras

  [Palestra](/Latinoware2009sortcol=0;table=1;up=0#sorted_table "Sort by this column")   [Palestrante](/Latinoware2009sortcol=1;table=1;up=0#sorted_table "Sort by this column")
  ------------------------------------------------------------------------------------------------------------------------ ---------------------------------------------------------------------------------------------------------------------------
  Gnome Love - Você no seu desktop                                                                                         Lício Fonseca
  Projeto de Tradução do GNOME para o Português do Brasil                                                                  Rodrigo Flores
  Projeto Mono: O retorno dos Jedi                                                                                         Alessandro Binhara
  Gnome Women                                                                                                              Izabel Valverde e Luciana Menezes
  Reorganizando a Experiência do Usuário no GNOME                                                                          Vinicius Depizzol
  Trocando a roupa do gnomo - Personalização do ambiente GNOME                                                             Tiago Menezes

### []{#Blog_posts_sobre_o_evento} Blog posts sobre o evento

-   [Alexandro](http://blog.alexos.com.br/2009/10/29/forum-gnome-no-latinoware-2009/)
-   [Lício
    Fonseca](http://weblog.licio.eti.br/2009/10/28/latinoware-2009-and-gnome-forum/)
-   [Rodrigo Flores](http://www.blog.rodrigoflores.org/?p=280)

### []{#Fotos} Fotos

-   [Por
    Alex](http://picasaweb.google.com/alexoslabs/LatinowareFozDoIguacuOut09)
-   [Por
    Licio](http://www.flickr.com/photos/licio/sets/72157622556844605/)
