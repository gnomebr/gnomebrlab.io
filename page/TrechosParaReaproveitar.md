%Trechos para reaproveitar

Essa é a página da [Equipe Brasileira de Tradução do GNOME](Traducao.html).
Aqui você encontra a tradução de textos recorrentes; mantê-los com a
mesma tradução é importante para promover a intuitividade do GNOME.

## Notas legais dos pacotes

Aqui segue o texto da GPL, que normalmente aparece nos catálogos. Os
novos pacotes normalmente estão sem tradução, então reaproveitem.

> **msgid** \"\"\
> \"This program is free software; you can redistribute it and/or modify it
> under the terms of the GNU General Public License as published by the Free
> Software Foundation; either version 2 of the License, or (at your option)
> any later version. \"\
>
> **msgstr** \"\"\
> \"Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo
> sob os termos da Licença Pública Geral GNU publicada pela Free Software
> Foundation; qualquer versão 2 da Licença, ou (a seu critério) outra versão
> posterior. \"

> **msgid** \"\"\
> \"This program is distributed in the hope that it will be useful, but WITHOUT
> ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
> FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
> more details. You should have received a copy of the GNU General Public
> License along with this program; if not, write to the Free Software
> Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.\"\
>
> **msgstr** \"\"\
> \"Este programa é distribuído na expectativa de que seja útil, mas SEM NENHUMA
> GARANTIA; sem mesmo implicar garantias de COMERCIALIZAÇÃO ou ADEQUAÇÃO
> A UM FIM ESPECÍFICO. Veja a Licença Pública Geral GNU (GPL) para mais
> detalhes. Você deve ter recebido uma cópia da Licença Pública Geral GNU
> junto com este programa; caso contrário, escreva para Free Software
> Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
> USA.\"

##Notas legais da documentação

Este costuma ser usado na tradução da ajuda.

> **msgid** \"\"\
> \"Permission is granted to copy, distribute and/or modify this
> document under the terms of the GNU Free Documentation License (GFDL),
> Version 1.1 or any later version published by the Free Software
> Foundation with no Invariant Sections, no Front-Cover Texts, and no
> Back-Cover Texts. You can find a copy of the GFDL at this \<ulink
> type=\\\"help\\\" url=\\\"ghelp:fdl\\\"\>link\</ulink\> or in the file
> COPYING-DOCS distributed with this manual.\"\
>
> **msgstr** \"\"\
> \"Permissão concedida para copiar, distribuir e/ou modificar este
> documento sob os termos da Licença de Documentação Livre GNU (GNU Free
> Documentation License), Versão 1.1 ou qualquer versão mais recente
> publicada pela Free Software Foundation; sem Seções Invariantes,
> Textos de Capa Frontal, e sem Textos de Contracapa. Você pode
> encontrar uma cópia da licença GFDL neste \<ulink type=\\\"help\\\"
> url=\\\"ghelp:fdl\\\"\>link\</ulink\> ou no arquivo COPYING-DOCS
> distribuído com este manual.\"

> **msgid** \"\"\
> \"This manual is part of a collection of GNOME manuals distributed
> under the GFDL. If you want to distribute this manual separately from
> the collection, you can do so by adding a copy of the license to the
> manual, as described in section 6 of the license.\"\
>
> **msgstr** \"\"\
> \"Este manual é parte da coleção de manuais do GNOME distribuídos sob
> a GFDL. Se você quiser distribuí-lo separadamente da coleção, você
> pode fazê-lo adicionando ao manual uma cópia da licença, como descrito
> na seção 6 da licença.\"

> **msgid** \"\"\
> \"Many of the names used by companies to distinguish their products
> and services are claimed as trademarks. Where those names appear in
> any GNOME documentation, and the members of the GNOME Documentation
> Project are made aware of those trademarks, then the names are in
> capital letters or initial capital letters.\"\
>
> **msgstr** \"\"\
> \"Muitos dos nomes usados por empresas para distinguir seus produtos e
> serviços são reivindicados como marcas registradas. Onde esses nomes
> aparecem em qualquer documentação do GNOME e os membros do Projeto de
> Documentação do GNOME estiverem cientes dessas marcas registradas, os
> nomes aparecerão impressos em letras maiúsculas ou com iniciais em
> maiúsculas.\"

> **msgid** \"\"\
> \"DOCUMENT IS PROVIDED ON AN \\\"AS IS\\\" BASIS, WITHOUT WARRANTY OF
> ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION,
> WARRANTIES THAT THE DOCUMENT OR MODIFIED VERSION OF THE DOCUMENT IS
> FREE OF DEFECTS MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE OR
> NON-INFRINGING. THE ENTIRE RISK AS TO THE QUALITY, ACCURACY, AND
> PERFORMANCE OF THE DOCUMENT OR MODIFIED VERSION OF THE DOCUMENT IS
> WITH YOU. SHOULD ANY DOCUMENT OR MODIFIED VERSION PROVE DEFECTIVE IN
> ANY RESPECT, YOU (NOT THE INITIAL WRITER, AUTHOR OR ANY CONTRIBUTOR)
> ASSUME THE COST OF ANY NECESSARY SERVICING, REPAIR OR CORRECTION. THIS
> DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS LICENSE.
> NO USE OF ANY DOCUMENT OR MODIFIED VERSION OF THE DOCUMENT IS
> AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER; AND\"\
>
> **msgstr** \"\"\
> \"O DOCUMENTO É FORNECIDO NA BASE \\\"COMO ESTÁ\\\", SEM GARANTIAS DE
> QUALQUER TIPO, TANTO EXPRESSA OU IMPLÍCITA, INCLUINDO, MAS NÃO
> LIMITADO A, GARANTIAS DE QUE O DOCUMENTO OU VERSÃO MODIFICADA DO
> DOCUMENTO SEJA COMERCIALIZÁVEL, LIVRE DE DEFEITOS, PRÓPRIO PARA UM
> PROPÓSITO ESPECÍFICO OU SEM INFRAÇÕES. TODO O RISCO A RESPEITO DA
> QUALIDADE, EXATIDÃO, E DESEMPENHO DO DOCUMENTO OU VERSÕES MODIFICADAS
> DO DOCUMENTO É DE SUA RESPONSABILIDADE. SE ALGUM DOCUMENTO OU VERSÃO
> MODIFICADA SE PROVAR DEFEITUOSO EM QUALQUER ASPECTO, VOCÊ (NÃO O
> ESCRITOR INICIAL, AUTOR OU QUALQUER CONTRIBUIDOR) ASSUME O CUSTO DE
> QUALQUER SERVIÇO NECESSÁRIO, REPARO OU CORREÇÃO. ESSA RENÚNCIA DE
> GARANTIAS CONSTITUI UMA PARTE ESSENCIAL DESTA LICENÇA. NENHUM USO
> DESTE DOCUMENTO OU VERSÃO MODIFICADA DESTE DOCUMENTO É AUTORIZADO SE
> NÃO FOR SOB ESSA RENÚNCIA; E\"

> **msgid** \"\"\
> \"UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL THEORY, WHETHER IN TORT
> (INCLUDING NEGLIGENCE), CONTRACT, OR OTHERWISE, SHALL THE AUTHOR,
> INITIAL WRITER, ANY CONTRIBUTOR, OR ANY DISTRIBUTOR OF THE DOCUMENT OR
> MODIFIED VERSION OF THE DOCUMENT, OR ANY SUPPLIER OF ANY OF SUCH
> PARTIES, BE LIABLE TO ANY PERSON FOR ANY DIRECT, INDIRECT, SPECIAL,
> INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY CHARACTER INCLUDING,
> WITHOUT LIMITATION, DAMAGES FOR LOSS OF GOODWILL, WORK STOPPAGE,
> COMPUTER FAILURE OR MALFUNCTION, OR ANY AND ALL OTHER DAMAGES OR
> LOSSES ARISING OUT OF OR RELATING TO USE OF THE DOCUMENT AND MODIFIED
> VERSIONS OF THE DOCUMENT, EVEN IF SUCH PARTY SHALL HAVE BEEN INFORMED
> OF THE POSSIBILITY OF SUCH DAMAGES.\"\
>
> **msgstr** \"\"\
> \"SOB NENHUMA CIRCUNSTÂNCIA E SOB NENHUMA TEORIA LEGAL, TANTO EM DANO
> (INCLUINDO NEGLIGÊNCIA), CONTRATO, OU OUTROS, DEVEM O AUTOR, ESCRITOR
> INICIAL, QUALQUER CONTRIBUIDOR, OU QUALQUER DISTRIBUIDOR DO DOCUMENTO
> OU VERSÃO MODIFICADA DO DOCUMENTO, OU QUALQUER FORNECEDOR DE ALGUMA
> DESSAS PARTES, SEREM CONSIDERADOS RESPONSÁVEIS A QUALQUER PESSOA POR
> QUALQUER DANO, SEJA DIRETO, INDIRETO, ESPECIAL, ACIDENTAL OU DANOS
> DECORRENTES DE QUALQUER NATUREZA, INCLUINDO, MAS NÃO LIMITADO A, DANOS
> POR PERDA DE BOA VONTADE, TRABALHO PARADO, FALHA OU MAU FUNCIONAMENTO
> DO COMPUTADOR, OU QUALQUER E TODOS OS OUTROS DANOS OU PERDAS
> RESULTANTES OU RELACIONADOS AO USO DO DOCUMENTO E VERSÕES MODIFICADAS,
> MESMO QUE TAL PARTE TENHA SIDO INFORMADA DA POSSIBILIDADE DE TAIS
> DANOS.\"

> **msgid** \"\"\
> \"DOCUMENT AND MODIFIED VERSIONS OF THE DOCUMENT ARE PROVIDED UNDER
> THE TERMS OF THE GNU FREE DOCUMENTATION LICENSE WITH THE FURTHER
> UNDERSTANDING THAT: \<\_:orderedlist-1/\>\"\
>
> **msgstr** \"\"\
> \"O DOCUMENTO E VERSÕES MODIFICADAS DO DOCUMENTO SÃO FORNECIDOS SOB OS
> TERMOS DA GNU FREE DOCUMENTATION LICENSE COM O ENTENDIMENTO ADICIONAL
> DE QUE: \<\_:orderedlist-1/\>\"

## Informação sobre relatório de erro no Gitlab

Desde a migração para o Gitlab, instruções de relatório de erro têm
sido mudadas nas documentações do GNOME. Visando uma padronização,
aproveito os trechos abaixo.

> **msgid** \"\"\
> \"If you notice a problem you can file a \<em>bug report\</em>. To file a bug,
> go to \<link href=\\\"https://gitlab.gnome.org/GNOME/NOMEPROJETO/issues/\\\"/>.\"\
>
> **msgstr** \"\"\
> \"Se você notar um problema, você pode apresentar um \<em>relatório de erro\</em>.
> Para relatar um erro, acesse \<link
> href=\\\"https://gitlab.gnome.org/GNOME/NOMEPROJETO/issues/\\\"/>.\"

> **msgid** \"\"\
> \"This is a bug tracking system where users and developers can file details
> about bugs, crashes and request enhancements.\"\
>
> **msgstr** \"\"\
> \"Este é um sistema de rastreamento de erros onde usuários e desenvolvedores
> podem relatar detalhes sobre erros, falhas e solicitações de melhorias.\"

> **msgid** \"\"\
> \"To participate you need an account which will give you the ability to gain
> access, file bugs, and make comments. Also, you need to register so you can
> receive updates by e-mail about the status of your bug. If you don't already
> have an account, just click on the \<gui>Sign in / Register\</gui> link to
> create one.\"\
>
> **msgstr** \"\"\
> \"Para participar, você precisa de uma conta que lhe concederá a habilidade de
> relatar erros e fazer comentários. Também, você precisa se registrar para
> receber atualizações por e-mail sobre o status de seu relatório de erro. Se
> você não tiver uma conta, basta clicar no link \<gui>Sign in / Register\</gui>
> para criar uma.\"

> **msgid** \"\"\
> \"Once you have an account, log in, and click on \<gui>New issue\</gui>. Before
> reporting a bug, please read the \<link href=\\\"https://bugzilla.gnome.org/
> page.cgi?id=bug-writing.html\\\">bug writing guidelines\</link>, and please
> \<link href=\\\"https://gitlab.gnome.org/GNOME/NOMEPROJETO/issues\\\">browse
> \</link> for the bug to see if it already exists.\"\
>
> **msgstr** \"\"\
> \"Assim que você tiver uma conta, autentique-se, clique em \<gui>New issue\</gui>.
> Antes de relatar um erro, por favor leia as \<link href=\\\"
> https://bugzilla.gnome.org/page.cgi?id=bug-writing.html\\\">diretrizes de
> relatório de erro\</link>, e por favor \<link href=\\\"https://bugzilla.gnome.
> org/browse.cgi?product=NOMEPROJETO\\\">pesquise\</link> por erros relatados
> para ver se já foi relatado."

> **msgid** \"\"\
> \"If you are requesting a new feature, choose \<gui>1. Feature\</gui> in the
> \<gui>Labels\</gui> menu. Fill in the Title and Description sections and click
> \<gui>Submit Issue\</gui>.\"\
>
> **msgstr** \"\"\
> \"Se você está requisitando um novo recurso, escolha \<gui>1. Feature\</gui> no
> menu \<gui>Labels\</gui>. Preencha as seções Title e Description e clique em
> \<gui>Submit Issue\</gui>.\"
