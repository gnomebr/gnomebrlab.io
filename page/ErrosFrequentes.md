%Área de Tradutores - Erros Frequentes

Esta é uma página da [equipe brasileira de tradução do
GNOME](Traducao.html). Este tópico ajudará na busca constante por
melhor qualidade de localização de software. Não é só questão da
tradução estar \"certa\" ou \"errada\", mas, acima de tudo, é necessário
respeitar a padronização para manter a coerência das traduções.

Para tornar essa página mais interessante, vamos acrescentar
recomendações novas no começo, ou seja, antes das antigas, como se fosse
um blog.

------------------------------------------------------------------------

Sobre o uso correto dos **pronomes demonstrativos**:

-   Quando o pronome se referir a algo que já foi citado no texto
    (referência anafórica, para quem gosta dos termos bonitos), serão
    usados ***esse, essa ou isso***. Se disser respeito a algo que ainda
    vai ser mencionado (referência catafórica), devem-se empregar
    ***este, esta ou isto***.
-   Para remeter a elementos citados anteriormente, substitua o primeiro
    por ***aquele, aquela, aquilo*** e o último por ***este, esta,
    isto***. Exemplo:

> Temos duas listas de discussão no GNOME Brasil, a gnome-br-list e a
> gnome-l10n-br. ***Esta*** \[referência a gnome-\|10n-br\] é usada para
> os trabalhos de tradução, enquanto ***aquela*** \[referência a
> gnome-br-list\] serve a propósitos genéricos.

------------------------------------------------------------------------

As seguintes palavras em inglês não tem forma plural, pois são
incontáveis:

-   ***information***;
-   ***music***;

Mas elas possuem plural no português, então sempre verifique se o
contexto pede que elas sejam traduzidas no plural ou no singular. Por
exemplo: ***some music*** seria traduzido como ***algumas músicas*** e
***for more information*** como ***para mais informações***.

------------------------------------------------------------------------

A palavra ***case*** e ***capitalization*** devem ser traduzidas como
***maiusculização*** e não capitalização. Embora este termo exista no
léxico da língua portuguesa, não corresponde ao sentido desejado.

------------------------------------------------------------------------

Para evitar ambiguidades, ***locale*** (que define a língua em que o
software será executado) deve ser traduzido como ***localidade***,
enquanto ***location*** (caminho, diretório, endereço de internet, etc.)
como ***localização*** ou ***local***.

------------------------------------------------------------------------

***This feature isn\'t supported*** deve ser traduzido como ***Não há
suporte a esse recurso***, e não ***Esse recurso não é suportado***. Da
mesma forma, ***Epiphany doesn\'t support Git commands*** deve ser
traduzido como ***O Epiphany não tem suporte a comandos Git***, e não
***O Epiphany não suporta comandos Git***.

------------------------------------------------------------------------

Convencionou-se traduzir os verbos ***display*** e ***show***,
respectivamente, como ***exibir*** e ***mostrar***.

------------------------------------------------------------------------

Cuidado com a tradução de ***this***. Muitas vezes o termo é usado para
algo que já foi referenciado anteriormente. Quando isso acontece, a
melhor tradução é ***isso*** ou ***esse*** em vez de *isto* ou *este*.

------------------------------------------------------------------------

A tradução de ***delete*** é ***excluir***, não *apagar* ou *remover*.

------------------------------------------------------------------------

O substantivo ***binary***, quando se referir ao código objeto do
programa, deve ser traduzido para ***executável***, não *binário*. E
programas não ***rodam*** (tradução informal de ***run***), mas
***executam***.

------------------------------------------------------------------------

O trema não deve mais ser usado a partir do Novo Acordo Ortográfico
(informe-se mais sobre isso!). Portanto, escreva ***frequente*** e
***consequência***, em vez de *freqüente* e *conseqüência*.

------------------------------------------------------------------------

Outras convenções de interface:

-   ***popup window*** como ***janela instantânea***;
-   ***dropdown list*** como ***lista suspensa***;
-   ***spin box*** como ***seletor numérico***;

------------------------------------------------------------------------

Convencionou-se traduzir o termo ***charset*** para ***codificação de
caractere***, não *codificação de caracteres*.

------------------------------------------------------------------------

***Enable*** deve ser traduzido como ***habilitar***, ao invés de
*ativar*. Da mesma forma, a tradução correta para ***disable*** é
***desabilitar***.

------------------------------------------------------------------------

***Parse*** deve ser traduzido como ***analisar***.

------------------------------------------------------------------------

***Find*** deve ser traduzido preferencialmente como ***localizar***, e
não *encontrar*, como ocorre muitas vezes.

------------------------------------------------------------------------

***Search*** deve ser traduzido como ***pesquisar***, e não como
*buscar* ou *procurar*.

------------------------------------------------------------------------

Quando aparece o termo ***plugin***, normalmente é mantido
equivocadamente sem alteração. Porém, o correto é alterar para
***plug-in***. O mesmo acontece com ***online***, que deve ser alterado
para ***on-line***.

------------------------------------------------------------------------

Quando aparecem na mensagem ***dois espaços (toque duplo)*** após um
ponto, muitas vezes o tradutor conserva o espaçamento entre as frases.
Porém, o correto é eliminar um deles e deixar apenas ***um espaço após o
ponto (toque simples)***, antes de continuar a tradução do restante da
mensagem.

------------------------------------------------------------------------

A tradução de ***indentation*** é preferencialmente ***recuo***, em vez
de *indentação*, *identação*, *endentação* *etc.*

------------------------------------------------------------------------

Algumas pessoas traduzem ***tunneling*** como *tunelamento*, mas o
correto é ***encapsulamento***.

------------------------------------------------------------------------

A tradução de ***applet*** é ***miniaplicativo***, e não
*mini-aplicativo*.

------------------------------------------------------------------------

***Encrypted*** pode induzir o tradutor a usar a palavra *encriptado*,
mas a tradução correta para esse termo é ***Criptografado***.

------------------------------------------------------------------------

As expressões ***cannot be/could not be*** são às vezes traduzidas com
distinção, como ***não é possível/não foi possível*** , respectivamente.
Porém, entre os programadores não há discriminação entre as duas formas,
sendo adotada de praxe a expressão ***não foi possível***.

------------------------------------------------------------------------

***View*** é às vezes traduzido como *Visualizar* e até essa alternativa
aparece no Vocabulário Padrão LDP-BR como uma tradução possível em
certas ocasiões. Contudo, deve-se preferir ***Ver*** a *Visualizar*,
principalmente quando se trata de um item de menu e/ou quando há o termo
*Preview* no mesmo catálogo. ***Preview*** é às vezes traduzido como
*Pré-Visualizar*, mas o próprio Vocabulário Padrão LDP-BR sugere a
tradução ***Visualizar***.

------------------------------------------------------------------------

A melhor tradução para ***consistent*** é ***coerente***. No português
padrão, *consistência* refere-se mais a qualidade de um material (se tem
firmeza, se é encorpado, espesso *etc.*) ou argumento, não se ele é
compatível, se tem conformidade ou congruência com outros elementos
(*coerente*). Trata-se de um [falso
amigo](http://en.wikipedia.org/wiki/False_friend) muito difundido.

------------------------------------------------------------------------

A tradução de ***mute*** é ***sem áudio***, não *mudo*.

------------------------------------------------------------------------

A tradução de ***\_Contents*** (item do menu **Aj\_uda**) agora é
***S\_umário***.

------------------------------------------------------------------------

A palavra avatar e seu plural existem em português e são traduzidas como
avatar e avatares.

------------------------------------------------------------------------

A tradução de ***enter*** deve ser ***digitar***.

------------------------------------------------------------------------

A tradução de ***Gradient*** deve ser ***Degradê***, quando refere-se a
cores. A **exceção** é quando ***Gradient*** refere-se a operação de
campos vetoriais em matemática, devendo neste caso ser traduzida como
***Gradiente***.

------------------------------------------------------------------------

A tradução de termos envoltos em \\\"\\\", como \\\"palavra\\\", na
maioria das vezes não devem ser traduzidas, pois são chaves do gconf. Se
no comentário da string tiver algo como \"schemas.in\", provavelmente
trata-se de chaves do [gconf](http://www.gnome.org/projects/gconf/).

No entanto, cada caso deve ser analisado separadamente. Há casos em que
a string com aspas aparece e deve ser traduzida normalmente.

Na dúvida, a orientação é marcar a string como tradução incerta (fuzzy)
ou perguntar na lista ou no irc.

------------------------------------------------------------------------

Use corretamente os sinais de pontuação ***hífen*** (-) (\"hyphen\"),
***meia-risca*** ou meio-traço (–) (\"en dash\") e ***travessão***
(—) (\"dash\"), conforme o contexto adequado. Segue algumas
referências com informações detalhadas: Wikipédia
([hífen](https://pt.wikipedia.org/wiki/H%C3%ADfen),
[meia-risca](https://pt.wikipedia.org/wiki/Meia-risca),
[travessão](https://pt.wikipedia.org/wiki/Travess%C3%A3o)), Norma Culta
([hífen](https://www.normaculta.com.br/hifen/),
[meia-risca](https://www.normaculta.com.br/meia-risca-ou-meio-traco/) e
[travessão](https://www.normaculta.com.br/travessao/)), Manual de
comunicação da SECOM do Senado Federal
([hífen](https://www12.senado.leg.br/manualdecomunicacao/redacao-e-estilo/estilo/hifen),
[travessão](https://www12.senado.leg.br/manualdecomunicacao/redacao-e-estilo/estilo/travessao)),
Techtudo ([Como incluir no Mac e no
PC](http://www.techtudo.com.br/dicas-e-tutoriais/noticia/2016/07/travessao-meia-risca-e-hifen-como-incluir-no-texto-no-mac-e-no-pc.html)).
