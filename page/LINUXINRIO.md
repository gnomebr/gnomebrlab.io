::: {align="center"}
[]{#LINUX_IN_RIO_2010} LINUX IN RIO 2010
========================================

[![](http://www.linuxinrio.com.br/2010/imagens/banners/linrioheaderbanner.png "LINUXINRIO")](http://www.linuxinrio.com.br)\

[LINUXINRIO](LINUXINRIO.html) 2010 -
Encontro de Software Livre do Rio de Janeiro.

Local: Hotel Mont Blanc Rua Passos da Pátria, 105, bairro 25 de Agosto\
Duque de Caxias - RJ

  -------------- ---------------------------------------- ----------------
  `Dia / Hora`   `Palestra`                               `Palestrante`
  03/09 09h -    GNOMEShell e as Novidades do GNOME 3 -   Rodrigo Padula
  -------------- ---------------------------------------- ----------------

Site do evento: <http://linuxinrio.com.br>\
Programação:
<http://linuxinrio.com.br/2010/imagens/materias/grade14082010.pdf>
:::
