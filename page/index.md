# GNOME 3: facilidade, conforto e controle

:: {align="center"}
  ![window-selection-3.20-420x236.png](WebHome/window-selection-3.20-420x236.png){width="420" height="236"}   GNOME 3 é uma forma fácil e elegante de usar seu computador. Ele é projetado para colocar você no controle e trazer liberdade para todos. GNOME 3 é desenvolvido pela comunidade do GNOME, um grupo internacional diverso de contribuidores que conta com apoio de uma fundação sem fins lucrativos e independente.

  -------------------------------------------------- -----------------------------------------------------------
   [Conheça o GNOME](https://www.gnome.org/gnome-3)    [Obtenha o GNOME 3](https://www.gnome.org/getting-gnome)
  -------------------------------------------------- -----------------------------------------------------------
::

  ------------------------------------------------------------------------   -----    --------------------------------------------------------------------------
  Latinoware 2011                                                                     FISL12
  ------------------------------------------------------------------------   -----    --------------------------------------------------------------------------
  Em outubro, Foz do Iguaçu será, mais uma vez, a sede da Conferência                 Como nos últimos anos, o GNOME Brasil esteve presente no [FISL](http://softwarelivre.org/fisl12).
  Latino-Americana de Software Livre -- Latinoware 2011. Cerca de 2.500               Participamos através de um evento comunitário, palestras e exposição.
  pessoas, entre estudantes, profissionais e especialistas da áreas, são              O FISL foi realizado na PUC-RS em Porto Algre-RS nos dias 29 de Junho
  esperadas para a sétima edição do evento, que será realizada entre os               a 02 de Julho. Clique [aqui](/FISL12.html )e veja o que rolou.
  dias 19 e 21 de outubro, no Parque Tecnológico Itaipu (PTI), localizado
  na Usina Hidrelétrica de Itaipu.
  ------------------------------------------------------------------------  ------    --------------------------------------------------------------------------
