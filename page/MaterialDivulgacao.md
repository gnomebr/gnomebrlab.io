%Material de Divulgação

O Projeto GNOME tem um [Engagement
Team](https://wiki.gnome.org/action/show/Engagement) que, entre
outras atividades, organiza a produção de materiais de divulgação do
GNOME para serem utilizados em eventos diversos. Em geral, este
materiais são produzidos em inglês, o que não é muito adequado para o
contexto brasileiro. É importante que o GNOME Brasil tenha um conjunto
de materiais básicos que possam ser (re)utilizados em stands do grupo
e/ou na produção de eventos.

Portanto, se você quer colaborar com o GNOME Brasil na produção de
materiais de divulgação, aqui vão algumas ideias:

-   Traduzir algum banner/folheto/poster já existente (Veja a lista ou
    repositório da [Engagement
    Team](https://wiki.gnome.org/action/show/Engagement))
-   Produzir um cartilha básica sobre o que é o GNOME e o GNOME Brasil
-   Produzir um cartilha básica sobre como contribuir com o GNOME e o
    GNOME Brasil
-   Produzir materiais para o [Engagement
    Team](https://wiki.gnome.org/action/show/Engagement) do GNOME
-   Produzir imagens para a produção de camisas
-   Produzir banners web do GNOME e GNOME Brasil para serem colocados em
    outros sites, blogs e páginas pessoais

Se quiser começar a trabalhar em algumas dessas idéias ou se tiver
outras, utilize a nossa [lista de discussão](Comunidade.html) para
anunciar e discutir as suas atividades para evitar trabalho duplicado e
possivelmente conseguir mais colaboradores.

------------------------------------------------------------------------

## Material Gráfico Disponível

-   [Banner](#Banner)
-   [Banner Web Fórum](#bannerwebforum)

------------------------------------------------------------------------

## Banner {#Banner}

![MaterialDivulgacao/BannerA4_A1-thumb.png](MaterialDivulgacao/BannerA4_A1-thumb.png "Banner do GNOME")\

-   [Versão PNG](MaterialDivulgacao/BannerA4_A1.png)
-   [Versão SVG](MaterialDivulgacao/BannerA4_A1.svg)
-   [Versão XCF](MaterialDivulgacao/BannerA4_A1.xcf)

------------------------------------------------------------------------

## Banner Web Fórum {#bannerwebforum}

IV Fórum do GNOME

![MaterialDivulgacao/bannerFixo_468x60.png](MaterialDivulgacao/bannerFixo_468x60.png "Banner IV Fórum do GNOME")\

-   [Versão PNG](MaterialDivulgacao/bannerFixo_468x60.png)
-   [Versão SVG](MaterialDivulgacao/BannerWeb_468x60.svg)
