%Como relatar erros de tradução

> Essa é uma página da [equipe brasileira de tradução do GNOME](Traducao.html).

Errar é humano! Caso você encontre um erro de tradução no GNOME, KDE,
XFCE etc, por favor, nos ajude a melhorar a qualidade do nosso trabalho!
Relate o erro! No caso do erro ser em um dos aplicativos do GNOME, abra
um relatório de erro no [bugzilla do
GNOME](http://bugzilla.gnome.org/enter_bug.cgi?product=l10n). Alguns
tradutores brasileiros monitoram esses relatórios, e assim que possível
o erro será corrigido. Caso você tenha dificuldades com o inglês, pode
entrar em contato conosco através da lista de discussão (confira a
página [Comunidade](Comunidade.html)).

Caso o erro seja em um aplicativo do ambiente KDE, [veja como
relatar](http://br.kde.org/index.php?title=Tradu%C3%A7%C3%A3o_errada).

TODO: XFCE, Openbox, Ubuntu

## Como abrir um relatório de erro {#Como_abrir_um_relatorio_de_erro}

Acesse o site do bugzilla, crie uma conta e um novo relatório clicando
no *link* ***New Bug***. Na parte ***Bug Type***, escolha a opção
***Translations***, na próxima opção ***Application selection***,
selecione a categoria e o programa que foi traduzido. Em ***Language
selection***, selecione ***Brazilian Portuguese \[pt\_BR\]***. Na
próxima página selecione a versão do ***HEAD*** do GNOME. Na página
***Enter bug information***, é interessante realizar a descrição com
informações suficientes para a identificação do programa traduzido.
