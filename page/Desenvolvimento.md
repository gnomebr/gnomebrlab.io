# Desenvolvimento {#Desenvolvimento}

O Projeto GNOME desenvolve um sistema computacional composto por mais de
[100 softwares livres entre bibliotecas e aplicativos]
(http://bugzilla.gnome.org/describecomponents.cgi) tanto do desktop como
da plataforma. Em outras palavras, isto corresponde a mais de dois
milhões de linhas de código - que podem se ampliar ainda mais a cada nova
versão.

Assim, por se tratar de um projeto bastante amplo, que envolve muitos
módulos e desenvolvedores de diferentes partes do mundo, a comunidade
GNOME conta com o time do **[GNOME Love]
(https://www.gnome.org/get-involved/)** que busca facilitar a
entrada de novas(os) colaboradoras(es) que queiram contribuir com o
desenvolvimento dos softwares do projeto.

Para tanto, além de uma [lista de discussão aberta]
(https://mail.gnome.org/mailman/listinfo/newcomers-list) (em inglês),
esse time prepara e atualiza alguns [guias (tutoriais)]
(https://wiki.gnome.org/action/show/Newcomers) de \"como\" e 
\"onde\" uma pessoa interessada em contribuir com o desenvolvimento do
GNOME pode começar a colaborar.

Se preferir tirar suas dúvidas iniciais em português, sinta-se a vontade
para tirá-las na [lista geral](Comunidade.html) do GNOME Brasil. No
entanto, tenha sempre em mente que saber se comunicar em inglês é um
forte requisito para contribuições técnicas no desenvolvimento de
softwares do GNOME.
