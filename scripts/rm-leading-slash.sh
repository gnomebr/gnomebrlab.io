#!/bin/bash
# Remove leading / from links so that links works locally
# [text](/somelink)  ->  [text](somelink)

if [ -n "$1" ]; then
  rootdir=$1
  rootdir=${rootdir%%/}  # remove trailing /
else
  rootdir=$(pwd)
fi

pagedir=$rootdir/page
extradir=$rootdir/extra

if [ -d $pagedir ]; then
  echo "Wrong rootdir obtained: $rootdir"
  exit 1
fi
 
# collect filename of .md files from 'page' dir 
page="$(find $pagedir -name '*.md' | sed -e "s|$pagedir/||" -e 's|.md||')";

# collect filepath of files from 'extra' dir, which contains subdirectory
extra="$(find $extradir -type f | sed -e "s|$extradir/||");"

# It might take a 1 minute or more to process all pages, please hold
for x in $page; do

  for y in $extra; do
    sed -i "s|\](/$y)|]($y)|" $pagedir/$x.md
  done

  for y in $page; do
    y=$y.html
    sed -i "s|\](/$y)|]($y)|" $pagedir/$x.md
  done

done
