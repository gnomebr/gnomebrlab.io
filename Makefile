extra = $(patsubst extra/%, public/%, $(wildcard extra/*))
page = $(patsubst page/%.md, public/%.html, $(wildcard page/*.md))

PANDOC = pandoc \
	--standalone \
	--from markdown \
	--to html \
	--css=stylesheet.css \
	--title-prefix="GNOME Brasil" \
	--include-before-body=page/header.html

all: $(extra) $(page) public/stylesheet.css

$(extra): public/%: extra/% public
	cp -R $< $@

$(page): public/%.html: page/%.md public
	$(PANDOC) --output $@ $<

public:
	mkdir $@

public/stylesheet.css: page/stylesheet.css
	cp $< $@

clean:
	$(RM) -r public
