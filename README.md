GNOMEBR
===============================================================================

This repository stores public files and information of the Brazilian Portuguese GNOME community, as well as its website.

This is migration from wiki.softwarelivre.org to Gitlab Pages.

Descriptions
-------------------------------------------------------------------------------
**Directory description:**
* extra  - stores images, videos and other files used in the pages.

* page   - stores site content to be automatically compiled and published in the website. Used by `make-dist`. See Makefile for more info.

* public - receives HTML converted from .md files and other files (e.g. artwork) and publishes in the website
 
Procedures
-------------------------------------------------------------------------------

**Publish changes in webpages:**
* Make desired edits in the .md file
* Run `make-dist` to make it avalaible
* Run git-add, git-commit and git-push to publish
